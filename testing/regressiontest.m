%regession testing for calculator

%set up arrrays for everything (results from this round, results from
%before)
scalinglistraw=['sa'; 'sf'; 'st'; 'lm'; 'li'; 'du'; 'de'];
scalinglist=cellstr(scalinglistraw);
nscalings=size(scalinglist,1);
load testsamplesall.mat

nsamples10=size(nominal10,1);
nsamples26=size(nominal26,1);
nsamples14=size(nominal14,1);
nsamples3=size(nominal3,1);
nsamples36=size(nominal36,1);

%results10old=zeros(12,nsamples10,nscalings);
%results26old=zeros(12,nsamples26,nscalings);
%results14old=zeros(12,nsamples14,nscalings);
%results3old=zeros(8,nsamples3,nscalings);
%results36old=zeros(28,nsamples36,nscalings);

results10=zeros(12,nsamples10,nscalings);
results26=zeros(12,nsamples26,nscalings);
results14=zeros(12,nsamples14,nscalings);
results3=zeros(8,nsamples3,nscalings);
results36=zeros(28,nsamples36,nscalings);
fail=0;
pass=0;

addpath(genpath(fullfile('..','common')));
for i=1:nscalings;
    %run each scaling scheme
    scaling_model=scalinglist{i};
    addpath(genpath(fullfile('..',scaling_model)));
    %run Be
    for j=1:nsamples10;
        out10=be10age(nominal10(j,:),uncerts10(j,:),scaling_model);
        results10(:,j,i)=out10;
        %Check against previous results
        if out10==results10old(:,j,i)
            pass=pass+1;
            fprintf(1,'Be sample %d passed \n',full([j]));
        else
            fail=fail+1;
            fprintf(1,'Be sample %d failed \n',full([j]));
            %calculate percent difference in ages, uncerts
            agediff=(out10(1)-results10old(1,j,i))/results10old(1,j,i)*100;
            uncdiff=(out10(2)-results10old(2,j,i))/results10old(2,j,i)*100;
            fprintf(1,'Be sample %d: %f percent age diff and %f percent unc diff \n',full([j; agediff; uncdiff]));
        end
    end
    %run Al
    for k=1:nsamples26;
        out26=al26age(nominal26(k,:),uncerts26(k,:),scaling_model);
        results26(:,k,i)=out26;
        %Check against previous results
        if out26==results26old(:,k,i)
            pass=pass+1;
            fprintf(1,'Al sample %d passed \n',full([k]));
        else
            fail=fail+1;
            fprintf(1,'Al sample %d failed \n',full([k]));
            %calculate percent difference in ages, uncerts
            agediff=(out26(1)-results26old(1,k,i))/results26old(1,k,i)*100;
            uncdiff=(out26(2)-results26old(2,k,i))/results26old(2,k,i)*100;
            fprintf(1,'Al sample %d: %f percent age diff and %f percent unc diff \n',full([k; agediff; uncdiff]));
        end
    end
    %run cl
    for m=1:nsamples36;
        out36=cl36age(nominal36(m,:),uncerts36(m,:),cov36(m),scaling_model);
        results36(:,m,i)=out36;
                %Check against previous results
        if out36==results36old(:,m,i)
            pass=pass+1;
            fprintf(1,'Cl sample %d passed \n',full([m]));
        else
            fail=fail+1;
            fprintf(1,'Cl sample %d failed \n',full([m]));
            %calculate percent difference in ages, uncerts
            agediff=(out36(1)-results36old(1,m,i))/results36old(1,m,i)*100;
            uncdiff=(out36(2)-results36old(2,m,i))/results36old(2,m,i)*100;
            fprintf(1,'Cl sample %d: %f percent age diff and %f percent unc diff \n',full([m; agediff; uncdiff]));
        end
    end
    %run he
    for n=1:nsamples3;
        out3=he3age(nominal3(n,:),uncerts3(n,:),scaling_model);
        results3(:,n,i)=out3;
           %Check against previous results
        if out3==results3old(:,n,i)
            pass=pass+1;
            fprintf(1,'He sample %d passed \n',full([n]));
        else
            fail=fail+1;
            fprintf(1,'He sample %d failed \n',full([n]));
            %calculate percent difference in ages, uncerts
            agediff=(out3(1)-results3old(1,n,i))/results3old(1,n,i)*100;
            uncdiff=(out3(2)-results3old(2,n,i))/results3old(2,n,i)*100;
            fprintf(1,'He sample %d: %f percent age diff and %f percent unc diff \n',full([n; agediff; uncdiff]));
        end
    end
    %run C
    for p=1:nsamples14;
        out14=c14age(nominal14(p,:),uncerts14(p,:),scaling_model);
        results14(:,p,i)=out14;
           %Check against previous results
        if out14==results14old(:,p,i)
            pass=pass+1;
            fprintf(1,'C sample %d passed \n',full([p]));
        else
            fail=fail+1;
            fprintf(1,'C sample %d failed \n',full([p]));
            %calculate percent difference in ages, uncerts
            agediff=(out14(1)-results14old(1,p,i))/results14old(1,p,i)*100;
            uncdiff=(out14(2)-results14old(2,p,i))/results14old(2,p,i)*100;
            fprintf(1,'C sample %d: %f percent age diff and %f percent unc diff \n',full([p; agediff; uncdiff]));
        end
    end
    rmpath(genpath(fullfile('..',scaling_model)));
end    
rmpath(genpath(fullfile('..','common')));
%summarize pass/fail for all
fprintf(1,'Samples passed: %d, Samples failed: %d \n',full([pass; fail]));
save regressiontestresults