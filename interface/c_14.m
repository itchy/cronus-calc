function [sample, outputdata, plotdata, labeldata] = c_14(inputs, RawInputs, sampleInputs, OutputData, sampleCount, calcConfig)
	% Wrapper for c_14 calculator
	lprint(calcConfig.logFile,'Starting c_14\n');
	totalSampleData = [];
	totalSampleErrData = [];
	
	for i=1:sampleCount;
		sampleInput = sampleInputs(i);
		% construct the input vector (order is important here).
		% I (hakan) don't recommend getting the input as such. We already have
		% all input in distinct variables so we can easily use them.
		sampleData = [ 	sampleInput.lat
						sampleInput.long
						sampleInput.elev
						sampleInput.ATM
						sampleInput.sthck
						sampleInput.bd
						sampleInput.shieldingF
						sampleInput.erosionrate
						sampleInput.cconc
						sampleInput.inheritance
						sampleInput.lambdafe
						sampleInput.depthtosample
						sampleInput.yearsampled
					]; 
		% input vector for uncertainities (order is important here)
		sampleErrData = [sampleInput.lat_uncert
						sampleInput.long_uncert
						sampleInput.elev_uncert
						sampleInput.ATM_uncert
						sampleInput.sthck_uncert
						sampleInput.bd_uncert
						sampleInput.shieldingF_uncert
						sampleInput.erosionrate_uncert
						sampleInput.cconc_uncert
						sampleInput.inheritance_uncert
						sampleInput.lambdafe_uncert
						sampleInput.depthtosample_uncert
						sampleInput.yearsampled_uncert
					];
		totalSampleData{end+1} = [sampleData];
		totalSampleErrData{end+1} = [sampleErrData];
	end
	
	%Load running_times and overwrite running_times if it exists
	%Create running_times for c
	running_times = struct();
	
	running_times_default.c.depth.average = 0;
	running_times_default.c.depth.count = 0;
	running_times_default.c.surface.DE.average = 0; running_times_default.c.surface.DE.count = 0;
	running_times_default.c.surface.DU.average = 0; running_times_default.c.surface.DU.count = 0;
	running_times_default.c.surface.LI.average = 0; running_times_default.c.surface.LI.count = 0;
	running_times_default.c.surface.LM.average = 0; running_times_default.c.surface.LM.count = 0;
	running_times_default.c.surface.SA.average = 0; running_times_default.c.surface.SA.count = 0;
	running_times_default.c.surface.ST.average = 0; running_times_default.c.surface.ST.count = 0;
	running_times_default.c.surface.SF.average = 0; running_times_default.c.surface.SF.count = 0;
	if (exist('running_times.mat') == 2)
		load('running_times','running_times');
	end
	%Merge the loaded variable with the defaults above in case they don't exist
	warning off;
	running_times = catstruct(running_times_default,running_times);
	warning on;
	
	%Call surface code, then add sample name and plotfiles to output
	[sample, outputdata, plotdata, plotfiles, labeldata,running_times] = c_14_surface(inputs, RawInputs, totalSampleData, totalSampleErrData, sampleInputs, sampleCount, OutputData, running_times, calcConfig);
	for i=1:sampleCount;
		sample(i).name = sampleInputs(i).name;
		sample(i).plot = plotfiles{i};
	end
	
	%Write out running_times
	save('running_times','running_times');
end

function [samples, outputdata, plotdata, plotfiles, labeldata, running_times] = c_14_surface(inputs, RawInputs, sampleData, sampleErrData, sampleInputs, sampleCount, OutputData, running_times, calcConfig)
	samples = repmat(struct([]),sampleCount,1);
	outputdata = [];
	plotdata = [];
	labeldata = [];
	plotfiles = {};
	for i=1:sampleCount;
		ElementID = tic;
		lprint(calcConfig.logFile,'************************************************\n');
		lprint(calcConfig.logFile,'C Sample %d/%d (%s) Starting (average execution time for %s is %s)...\n',i,sampleCount,sampleInputs(i).name,RawInputs(i).scaling,sec2hms(running_times.c.surface.(RawInputs(i).scaling).average));
		lprint(calcConfig.logFile,['Using scaling ' lower(RawInputs(i).scaling) '\n']);
		if (running_times.c.surface.(RawInputs(i).scaling).count > 0)
			lprint(calcConfig.userLogFile,'C Sample %d/%d (%s) (average execution time for %s is %s)...',i,sampleCount,sampleInputs(i).name,RawInputs(i).scaling,sec2hms(running_times.c.surface.(RawInputs(i).scaling).average));
		else
			lprint(calcConfig.userLogFile,'C Sample %d/%d (%s) (%s)...',i,sampleCount,sampleInputs(i).name,RawInputs(i).scaling);
		end
		lastwarn('');
	
		if (calcConfig.debug)
			lprint(calcConfig.logFile,'Generating debug data...\n');
			outvector = 1:100;derivs = 1:100;times = 1:100;plotprod = 1:100;
		else
			path = genpath(fullfile(pwd,'..',lower(RawInputs(1).scaling)));
			lprint(calcConfig.logFile,'Adding scaling scheme to matlab path...\n');
				addpath( path );
			lprint(calcConfig.logFile,'Calling c14age\n');
				[outvector,times,plotprod] = c14age(sampleData{i}, sampleErrData{i}, RawInputs(i).scaling);
			lprint(calcConfig.logFile,'Done Calling c14age\n');
			%Clean up the path
			lprint(calcConfig.logFile,'Removing scaling scheme from matlab path...\n');
				rmpath( path );
		end

		%outputs
		% 1. age (kyr)
		% 2. age uncertainty 
		% 3. Contemporary Elevation/latitude scaling factor for neutrons for C (unitless)
		% 4. Fast muon scaling factor (not going to user)
		% 5. Slow muon scaling factor (not going to user)
		% 6. Contemporary depth avg prod rate, neutron spallation (atoms/g/yr)
		% 7. Contemporary depth avg prod rate, muons (atoms/g/yr)
		% 8. Qs (unitless)
		% 9. Qmu (unitless)
		% 10. Inherited 14-C (atoms/g of target)
		% 11. Measured 14-C (atoms/g of target)
		% 12. Analytical (internal) uncertainty (kyr)
		
		lprint(calcConfig.logFile,'Generating labels and output data...\n');
		labeldata = {'Sample Name'};
		
		% Construct output structure for this sample
		[sample_age, sample_uncert] = age2str(outvector(1),outvector(2),2);
		
		samples(i).age = sample_age;
		labeldata{end+1} = 'Age';

		samples(i).uncertainty = sample_uncert;
		labeldata{end+1} = 'Age Uncertainty';

		samples(i).erosionrate = num2str(sampleInputs(i).erosionrate); 
		labeldata{end+1} = 'Erosion Rate';

		samples(i).elsfneutron = num2str(outvector(3)); 
		labeldata{end+1} = 'Scaling C Spallation';

		samples(i).elsfmuonfast = num2str(outvector(4)); 
		labeldata{end+1} = 'Scaling Fast Muons';

		samples(i).elsfmuonslow = num2str(outvector(5)); 
		labeldata{end+1} = 'Scaling Slow Muons';

		samples(i).prC = num2str(outvector(6)); 
		labeldata{end+1} = 'Production C Spallation';

		samples(i).prMu = num2str(outvector(7)); 
		labeldata{end+1} = 'Production Muon Spallation';

		samples(i).qs = num2str(outvector(8));
		labeldata{end+1} = 'Qs';

		samples(i).qmu = num2str(outvector(9));
		labeldata{end+1} = 'Qmu';
		
		samples(i).internalUncertainty=num2str(outvector(12));	
		labeldata{end+1} = 'Analytical (Internal) Uncertainty';

		if(length(lastwarn)>0); samples(i).warning = lastwarn; end;

		% Generate Plots
		figure;
		hold all;
		plot(times,plotprod);
		xlabel('Time Before 2010 (ka)');
		ylabel('Production Rate (atoms/g sample/yr)');
		hold off;
		plotfile = [char(inputs.prefix) '_' num2str( i ) '_' sampleInputs(i).name  '_c.png'];
		set(gcf,'PaperPositionMode','auto');
		lprint(calcConfig.logFile,'Saving plots...\n'); 
		saveas(gcf,[OutputData.tempDir '/' plotfile]);
		plotfiles{end+1} = plotfile;

		% Generate Output Data
		lprint(calcConfig.logFile,'Generating Output Data...\n');
		cellstruct = struct2cell(samples(i));
		outputdata = [sampleInputs(i).name ',' sprintf('%s,',cellstruct{:})];
		labeldata = [sprintf('%s,',labeldata{:}) ];
		plotdata = [plotdata '\n' sampleInputs(i).name '\n' ...
					 'Times Before 2010,' sprintf('%d,',times) '\n' ...
					 'ProdC,' sprintf('%d,',plotprod) '\n' ];
					 
		element_time = toc(ElementID);
		lprint(calcConfig.logFile,['Element completed in ' sec2hms(element_time) '\n']); 
		lprint(calcConfig.logFile,'************************************************\n');
		lcprint(calcConfig.userLogFile,' %s +/- %s ka completed in %s\n',samples(i).age, samples(i).uncertainty, sec2hms(element_time));
		
		if (~calcConfig.debug)
			running_times.c.surface.(RawInputs(i).scaling).average = (running_times.c.surface.(RawInputs(i).scaling).average*running_times.c.surface.(RawInputs(i).scaling).count + element_time)/(running_times.c.surface.(RawInputs(i).scaling).count + 1);
			if (running_times.c.surface.(RawInputs(i).scaling).count < 100000) running_times.c.surface.(RawInputs(i).scaling).count = running_times.c.surface.(RawInputs(i).scaling).count + 1; end
		end
	end
end 
