function [output,cache]=topo_calc(inputs,cache)
% Topographic Shielding Calculator

%set up command logging (no data is stored)
user_log_file = [char(inputs.tempDir) '/' inputs.prefix '_topo.txt'];
log_file = [char(inputs.logDir) '/' inputs.prefix];
log_file = [log_file '_topo.log'];

lprint(log_file,'Starting topo_calc\n');
lprint(user_log_file,'Sample Preprocessing Starting...');

ticID = tic;
OutputData.temp_dir = relativepath(char(inputs.tempDir));
OutputData.date = datestr(now);

tic;
lprint(log_file,'Loading cache...');
if(isempty(cache))
   % Load transformer and validator objects into the cache
   cache.transformer = cronusCalculators.xml.XMLTransformer(inputs.stylesheetFile);
   cache.validator = cronusCalculators.xml.XMLValidator(inputs.schemaFile);
end
lcprint(log_file,['    Cache loaded in ' num2str(toc) '\n']); 

% Parse input HTTP parameter sampleData as a Java XML DOM object
tic;
lprint(log_file,'Parsing inputs...');
try
   inputXMLDOM = cronusCalculators.xml.XMLModule.parseXMLstring(inputs.sampleData);
catch
   error('Input is not a well-formed XML!');
end
lcprint(log_file,['    Inputs parsed in ' num2str(toc) '\n']); 

% Validate the input against the schema file. If validation fails, a Java
% exception is thrown here and caught by the servlet.
tic;
lprint(log_file,'Validating inputs against schema...');
cache.validator.updateSchemaIfNewer;
try
   cache.validator.validate(inputXMLDOM);
catch exception
   error(strcat('Input is a well-formed XML but not a valid input for this calculator. ',getReport(exception)));
end
lcprint(log_file,['    Inputs validated in ' num2str(toc) '\n']); 

output = [];

% Loop through each sample element in the input XML
root = inputXMLDOM.getFirstChild;
children = root.getChildNodes;

lprint(log_file,'Looping through elements...\n');
lcprint(user_log_file,'    Completed in %s seconds\n',num2str(toc(ticID)));

for i=0:children.getLength-1;
   ElementID = tic;

   lprint(log_file,'************************************************\n');
   lprint(log_file,'Element %d\n',i);

   sampleNode = children.item(i);
   [sampleStruct,sampleAttribs] = node2struct(sampleNode);

   lprint(user_log_file,'Sample %d (%s) Starting...',i,sampleStruct.input.name);

   % doublefields function tries to parse every field of the input struct
   % into double. Second argument (true) tells it to ignore non-numeric
   % and non-parseable fields.
   sampleInput = doublefields(sampleStruct.input,true);

   % now sampleInput contains all input data in double type
   sampleInput.theta = str2num(sampleStruct.input.theta);
   sampleInput.horizon = str2num(sampleStruct.input.horizon);

   inputcellstruct = struct2cell(sampleStruct.input);

   tic;
   lprint(log_file,'Calling topocalc code...');
	output = topofactor([sampleInput.theta; sampleInput.horizon]',...
				sampleInput.dip_direction, sampleInput.dip_angle,...
				'Ssnow',sampleInput.snow_shielding);
   lcprint(log_file,['    Topocalc code finished in ' num2str(toc) '\n']); 

   output.name = sampleStruct.input.name;
   outChild = struct2node(inputXMLDOM,output,'output');
   sampleNode.appendChild(outChild);


   element_time = toc(ElementID);
   lprint(log_file,['Element completed in ' num2str(element_time) '\n']); 
   lprint(log_file,'************************************************\n');
   if (element_time > 3600)
      lcprint(user_log_file,'    Completed in %s hours\n',num2str(element_time/3600));
   elseif (element_time > 60)
      lcprint(user_log_file,'    Completed in %s minutes\n',num2str(element_time/60));
   else
      lcprint(user_log_file,'    Completed in %s seconds\n',num2str(element_time));
   end;
end;

tic;
lprint(log_file,'Processing results...\n');
OutputData.time_elapsed = toc(ticID);
outputDataChild = struct2node(inputXMLDOM,OutputData,'output_data');
inputXMLDOM.getFirstChild.appendChild(outputDataChild);


% Calculation finished.

% Transform the XML to a HTML page using xslt we loaded into the cache
cache.transformer.updateStylesheet;
%cache.transformer.updateStylesheetIfNewer;
resHTML = cache.transformer.transform(inputXMLDOM);

out = java.io.BufferedWriter(java.io.FileWriter([char(inputs.tempDir) '/' char(inputs.prefix) '_topo.html']));
out.write(resHTML);
out.close();

% Remove 'ROOT' from path (since ROOT is hidden in the urls)
temp_html = regexprep(OutputData.temp_dir,'ROOT\/','');
output.write = ['<html><head><meta http-equiv=''refresh'' content=''0; url=' temp_html char(inputs.prefix) '_topo.html''></head></html>'];
output.contentType = 'html';
lcprint(user_log_file,'\n\nProcessing Complete - output can be viewed <a href="%s">here</a>\n',[temp_html char(inputs.prefix) '.html']);

%End logging
diary off
end 
