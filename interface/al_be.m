function [sample, outputdata, plotdata, labeldata] = al_be(inputs, RawInputs, sampleInputs, OutputData, sampleCount, calcConfig)
% Wrapper for al-be calculator
    lprint(calcConfig.logFile,'Starting al_be\n');
	totalSampleData = [];
	totalSampleErrData = [];
   
    be_stds.('KNSTD07') = 1.0000;
    be_stds.('KNSTD') = 0.9042;
    be_stds.('NIST_Certified') = 1.0425;
    be_stds.('LLNL31000') = 0.8761;
    be_stds.('LLNL10000') = 0.9042;
    be_stds.('LLNL3000') = 0.8644;
    be_stds.('LLNL1000') = 0.9313;
    be_stds.('LLNL300') = 0.8562;
    be_stds.('NIST_30000') = 0.9313;
    be_stds.('NIST_30200') = 0.9251;
    be_stds.('NIST_30300') = 0.9221;
    be_stds.('NIST_30600') = 0.9130;
    be_stds.('NIST_27900') = 1.000;
    be_stds.('S555') = 0.9124;
    be_stds.('S2007') = 0.9124;
    be_stds.('BEST433') = 0.9124;
    be_stds.('BEST433N') = 1.000;
    be_stds.('S555N') = 1.000;
    be_stds.('S2007N') = 1.000;
    be_stds.('None') = 1.000;
    
    al_stds.('KNSTD') = 1.0000;
    al_stds.('ZAL94') = 0.9134;
    al_stds.('AL09') = 0.9134;
    al_stds.('ZAL94N') = 1.0000;
    al_stds.('SMAL11') = 1.021;
    al_stds.('Z92_0222') = 1.000;
    al_stds.('None') = 1.000;

	for i=1:sampleCount;
		sampleInput = sampleInputs(i);
		
		sampleInput.beinheritance=0;
		sampleInput.beinheritance_uncert=0;
		sampleInput.alinheritance=0;
		sampleInput.alinheritance_uncert=0;
	   
		%check for NaN and replace with 0
		if sampleInput.beconc==NaN;
			sampleInput.beconc=0;
		end
		if sampleInput.alconc==NaN;
			sampleInput.alconc=0;   
		end
		
		%Make sure the be standard doesn't start with a number
		if strcmp(RawInputs(i).be_std,'07KNSTD')
			RawInputs(i).be_std = 'KNSTD07';
		end

		% construct the input vector (order is important here).
		% I (hakan) don't recommend getting the input as such. We already have
		% all input in distinct variables so we can easily use them.
		sampleData = [ sampleInput.lat
					sampleInput.long
					sampleInput.elev
					sampleInput.ATM
					sampleInput.sthck
					sampleInput.bd
					sampleInput.shieldingF
					sampleInput.erosionrate
					sampleInput.beconc*be_stds.(RawInputs(i).be_std)
					sampleInput.alconc*al_stds.(RawInputs(i).al_std)
					sampleInput.beinheritance
					sampleInput.alinheritance
					sampleInput.lambdafe
					sampleInput.depthtosample
					sampleInput.yearsampled
					]; 
		% input vector for uncertainities (order is important here)
		sampleErrData = [sampleInput.lat_uncert
					sampleInput.long_uncert
					sampleInput.elev_uncert
					sampleInput.ATM_uncert
					sampleInput.sthck_uncert
					sampleInput.bd_uncert
					sampleInput.shieldingF_uncert
					sampleInput.erosionrate_uncert
					sampleInput.beconc_uncert
					sampleInput.alconc_uncert
					sampleInput.beinheritance_uncert
					sampleInput.alinheritance_uncert
					sampleInput.lambdafe_uncert
					sampleInput.depthtosample_uncert
					sampleInput.yearsampled_uncert
					];
		totalSampleData{end+1} = [sampleData];
		totalSampleErrData{end+1} = [sampleErrData];
	end
	
	%Load running_times and overwrite running_times if it exists
	%Create running_times for al/be
	running_times = struct();
	
	running_times_default.al_be.depth.average = 0;
	running_times_default.al_be.depth.count = 0;
	
	running_times_default.al.surface.DE.average = 0; running_times_default.al.surface.DE.count = 0;
	running_times_default.al.surface.DU.average = 0; running_times_default.al.surface.DU.count = 0;
	running_times_default.al.surface.LI.average = 0; running_times_default.al.surface.LI.count = 0;
	running_times_default.al.surface.LM.average = 0; running_times_default.al.surface.LM.count = 0;
	running_times_default.al.surface.SA.average = 0; running_times_default.al.surface.SA.count = 0;
	running_times_default.al.surface.ST.average = 0; running_times_default.al.surface.ST.count = 0;
	running_times_default.al.surface.SF.average = 0; running_times_default.al.surface.SF.count = 0;
	
	running_times_default.be.surface.DE.average = 0; running_times_default.be.surface.DE.count = 0;
	running_times_default.be.surface.DU.average = 0; running_times_default.be.surface.DU.count = 0;
	running_times_default.be.surface.LI.average = 0; running_times_default.be.surface.LI.count = 0;
	running_times_default.be.surface.LM.average = 0; running_times_default.be.surface.LM.count = 0;
	running_times_default.be.surface.SA.average = 0; running_times_default.be.surface.SA.count = 0;
	running_times_default.be.surface.ST.average = 0; running_times_default.be.surface.ST.count = 0;
	running_times_default.be.surface.SF.average = 0; running_times_default.be.surface.SF.count = 0;
	if (exist('running_times.mat') == 2)
		load('running_times','running_times');
	end
	%Merge the loaded variable with the defaults above in case they don't exist
	warning off;
	running_times = catstruct(running_times_default,running_times);
	warning on;
	
	if (calcConfig.depth)
		%Sort by depth, call depth code, and append plotfiles to output
		totalSampleDataMat = cell2mat(totalSampleData);
		[tmp ind] = sort(totalSampleDataMat(14,:));
		sortedSampleData = totalSampleData(ind);
		sortedSampleErrData = totalSampleErrData(ind);
		[sample, outputdata, plotdata, plotfiles, labeldata,running_times] = al_be_depth(inputs, RawInputs, sortedSampleData, sortedSampleErrData, sampleInputs, sampleCount, OutputData, running_times, calcConfig);
		sample(1).plot = plotfiles;
	else
		%Call surface code, then add sample name and plotfiles to output
		[sample, outputdata, plotdata, plotfiles, labeldata,running_times] = al_be_surface(inputs, RawInputs, totalSampleData, totalSampleErrData, sampleInputs, sampleCount, OutputData, running_times, calcConfig);
		for i=1:sampleCount;
			sample(i).name = sampleInputs(i).name;
			sample(i).plot = plotfiles{i};
		end
	end
	%Write out running_times
	save('running_times','running_times');
end

function [samples, outputdata, plotdata, plotfiles, labeldata, running_times] = al_be_depth(inputs, RawInputs, sampleData, sampleErrData, sampleInputs, sampleCount, OutputData, running_times, calcConfig)
	samples = repmat(struct([]),1,1);
	outputdata = [];
	plotdata = [];
	labeldata = [];
	
	%Display start message (with average time if it exists and not debug)
	nuclide_time = tic;
	if (~calcConfig.debug && running_times.al_be.depth.count > 0)
		lprint(calcConfig.logFile,'Started Al/Be Depth Profile (average execution time is %s)...\n', sec2hms(running_times.al_be.depth.average));
		lprint(calcConfig.userLogFile,'Started Al/Be Depth Profile (average execution time is %s)...', sec2hms(running_times.al_be.depth.average));
	else
		lprint(calcConfig.logFile,'Started Al/Be Depth Profile...\n');
		lprint(calcConfig.userLogFile,'Started Al/Be Depth Profile...');
	end

	%Convert erosion units if necessary
	if(strcmp(inputs.erosion_units,'cm'))
		inputs.erosion_total = inputs.erosion_total * inputs.erosion_density * 1000.0;
	elseif (strcmp(inputs.erosion_units,'mm'))
		inputs.erosion_total = inputs.erosion_total * inputs.erosion_density * 0.1;
	end
	
	% Define the parameter space
	lprint(calcConfig.logFile,'Defining parameter space...\n');
	erates=linspace(inputs.erosion_min,inputs.erosion_max,inputs.erosion_points)';
	ages=linspace(inputs.age_min,inputs.age_max,inputs.age_points)';
	inhers=linspace(inputs.inheritance_min,inputs.inheritance_max,inputs.inheritance_points)';

	%Determine which scaling scheme to use
	lprint(calcConfig.logFile,'Adding scaling scheme to matlab path...\n');
	path = genpath(fullfile(pwd,'..',lower(RawInputs(1).scaling)));
	addpath( path );

	%Call createage1026 to get some extra data
	lprint(calcConfig.logFile,'Calling createage1026...\n');
	[nominal10, uncerts10, nominal26, uncerts26] = createage1026([cell2mat(sampleData)' cell2mat(sampleErrData)']);

	% Calculate depth-to-middle of the samples
	lprint(calcConfig.logFile,'Calculating depths...\n');
	depths = nominal10(:,14) + 0.5*nominal10(:,5).*nominal10(:,6);
	
	% Define the prior distribution
	lprint(calcConfig.logFile,'Defining prior distribution...\n');
	pr=prior(erates,ages,inhers,'uniform','uniform','uniform');

	lastwarn('');
	if (calcConfig.debug)
		lprint(calcConfig.logFile,'Generating debug data...\n');
		MAP = [2 1 3];
		
		figure(4);
		plot(1:100,1:1:100);
		xlabel('Test Figure');
		ylabel('Test Figure');
		figure(5);
		plot(1:100,100:-1:1);
		xlabel('Test Figure');
		ylabel('Test Figure');
		figure(6);
		plot(1:100,1:1:100);
		xlabel('Test Figure');
		ylabel('Test Figure');
		figure(7);
		plot(1:100,100:-1:1);
		xlabel('Test Figure');
		ylabel('Test Figure');

	else
		% Run ageprofilecalc1026
		lprint(calcConfig.logFile,'Calling ageprofilecalc1026...\n');
		[posterior_er,posterior_age,posterior_inher,MAP,mu_bayes,chi2grid,lhsurf,...
			jposterior]=ageprofilecalc1026(nominal10,uncerts10(:,9:10),depths(:,1),...
			erates,ages,inhers,pr,inputs.erosion_total,lower(RawInputs(1).scaling));
			
		% Create plots
		lprint(calcConfig.logFile,'Generating plots...\n');
		makeplots1026(erates,ages,inhers,posterior_er,posterior_age,...
			posterior_inher,nominal10,uncerts10,depths,MAP,jposterior,lower(RawInputs(1).scaling));
	end
	% Display the MAP solution and Bayesian credible intervals
	%erCI=bayesianCI(posterior_er,erates,0.05);
	%ageCI=bayesianCI(posterior_age,ages,0.05);
	%inherCI=bayesianCI(posterior_inher,inhers,0.05);

	% Saving output data
	samples(1).MAP_age   = MAP(2);
	samples(1).MAP_erate = MAP(1);
	samples(1).MAP_inher = MAP(3);
	if(length(lastwarn)>0); samples(1).warning = lastwarn; end
	
	cellstruct = struct2cell(samples(1));
	outputdata = [sprintf('%f,',cellstruct{:})];
	
	labeldata{end+1} = 'Profile Age (ka)';
	labeldata{end+1} = 'Erosion Rate (g/cm^2/kyr)';
	labeldata{end+1} = 'Inheritance (yrs of exposure)';
	labeldata = [sprintf('%s',labeldata{:})];
	
	% Save plots
	lprint(calcConfig.logFile,'Saving plots...\n');
	plotfiles = {	[char(inputs.prefix) '_al_be_bestfit.png'],
					[char(inputs.prefix) '_al_be_erate_vs_age.png'],
					[char(inputs.prefix) '_al_be_age_vs_inher.png'],
					[char(inputs.prefix) '_al_be_erate_vs_inher.png'],
	};
	figure(4);
	set(gcf,'PaperPositionMode','auto');   
	saveas(gcf,[OutputData.tempDir '/' plotfiles{1}]);
	figure(5);
	set(gcf,'PaperPositionMode','auto');   
	saveas(gcf,[OutputData.tempDir '/' plotfiles{2}]);
	figure(6);
	set(gcf,'PaperPositionMode','auto');   
	saveas(gcf,[OutputData.tempDir '/' plotfiles{3}]);
	figure(7);
	set(gcf,'PaperPositionMode','auto');   
	saveas(gcf,[OutputData.tempDir '/' plotfiles{4}]);
	
		
	%Clean up the path
	lprint(calcConfig.logFile,'Removing scaling scheme from matlab path...\n');
	rmpath( path );
	
	%Print total time and update running_times if not debug
	profile_time = toc(nuclide_time);
	lprint(calcConfig.logFile,'Finished in %s',sec2hms(profile_time));
	lcprint(calcConfig.userLogFile,'		Completed in %s',sec2hms(profile_time));
	if (~calcConfig.debug)
		running_times.al_be.depth.average = (running_times.al_be.depth.average*running_times.al_be.depth.count + profile_time)/(running_times.al_be.depth.count + 1);
		if (running_times.al_be.depth.count < 100000) running_times.al_be.depth.count = running_times.al_be.depth.count + 1; end
	end
end


function [samples, outputdata, plotdata, plotfiles, labeldata, running_times] = al_be_surface(inputs, RawInputs, sampleData, sampleErrData, sampleInputs, sampleCount, OutputData, running_times, calcConfig)
	samples = repmat(struct([]),sampleCount,1);
	outputdata = [];
	plotdata = [];
	labeldata = []; labeldatabe = []; labeldataal = [];
	plotfiles = {};
	for i=1:sampleCount;
		ElementID = tic;
		
		%figure out if it has al or be or both
		hasbe=1;
		hasal=1;
		if sampleInputs(i).beconc==0
			hasbe=0;
		end
		if sampleInputs(i).alconc==0
			hasal=0;
		end
		
		lprint(calcConfig.logFile,'************************************************\n');
		if (hasbe == 1 && hasal == 1)
			lprint(calcConfig.logFile,'Al/Be Sample %d/%d (%s) Starting...\n',i,sampleCount,sampleInputs(i).name);
			lprint(calcConfig.userLogFile,'Al/Be Sample %d/%d (%s) Starting...\n',i,sampleCount,sampleInputs(i).name);
		elseif (hasbe == 1)
			lprint(calcConfig.logFile,'Be Sample %d/%d (%s) Starting...\n',i,sampleCount,sampleInputs(i).name);
			lprint(calcConfig.userLogFile,'Be Sample %d/%d (%s) Starting...\n',i,sampleCount,sampleInputs(i).name);
		elseif (hasal == 1)
			lprint(calcConfig.logFile,'Al Sample %d/%d (%s) Starting...\n',i,sampleCount,sampleInputs(i).name);
			lprint(calcConfig.userLogFile,'Be Sample %d/%d (%s) Starting...\n',i,sampleCount,sampleInputs(i).name);
		end
		lprint(calcConfig.logFile,['Using scaling ' lower(RawInputs(i).scaling) '\n']);
		
		lastwarn('');


		%Compute Be
		if hasbe == 1
			BeID = tic;
			if (running_times.be.surface.(RawInputs(i).scaling).count > 0)
				lcprint(calcConfig.userLogFile,'\t\t\tBe Aging (average execution time for %s is %s)...',RawInputs(i).scaling,sec2hms(running_times.be.surface.(RawInputs(i).scaling).average));
			else
				lcprint(calcConfig.userLogFile,'\t\t\tBe Aging (%s)...',RawInputs(i).scaling);
			end
			if (calcConfig.debug)
				lprint(calcConfig.logFile,'Generating debug data...\n');
				outvector = 1:100;times = 1:100;plotprodbe = 100:-1:1;
			else
				path = genpath(fullfile(pwd,'..',lower(RawInputs(1).scaling)));
				lprint(calcConfig.logFile,'Adding scaling scheme to matlab path...\n');
					addpath( path );
				lprint(calcConfig.logFile,'Calling be10age\n');
					[outvector,times,plotprodbe] = be10age(sampleData{i}, sampleErrData{i}, RawInputs(i).scaling);
				lprint(calcConfig.logFile,'Done Calling be10age\n');
				%Clean up the path
				lprint(calcConfig.logFile,'Removing scaling scheme from matlab path...\n');
					rmpath( path );
			end

			%outputs
			% 1. age (kyr)
			% 2. age uncertainty 
			% 3. Contemporary Elevation/latitude scaling factor for neutrons for Be (unitless)
			% 4. Contemporary Elevation/latitude scaling factor for fast muons (unitless)
			% 5. Contemporary Elevation/lat scaling factor for slow muons (unitless)
			% 6. Contemporary depth avg prod rate, neutron spallation (atoms/g/yr)
			% 7. Contemporary depth avg prod rate, muons (atoms/g/yr)
			% 8. Qs (unitless)
			% 9. Qmu (unitless)
			% 10. Inherited 10-Be (atoms/g of target)
			% 11. Measured 10-Be (atoms/g of target)
			% 12. Analytical (internal) uncertainty (kyr)
			samplebe = [];
			labeldatabe = {};
			
			% Construct output structure for this sample
			[sample_age, sample_uncert] = age2str(outvector(1),outvector(2),2);
			
			samplebe.age = sample_age;
			labeldatabe{end+1} = 'Age (Be)';

			samplebe.uncertainty = sample_uncert;
			labeldatabe{end+1} = 'Total Uncertainty (Be)';

			samplebe.erosionrate = num2str(sampleInputs(i).erosionrate);
			labeldatabe{end+1} = 'Erosion Rate';

			samplebe.elsfneutron = num2str(outvector(3));
			labeldatabe{end+1} = 'Scaling Be Spallation';

			samplebe.elsfmuonfast = num2str(outvector(4));
			labeldatabe{end+1} = 'Scaling Fast Muons (Be)';

			samplebe.elsfmuonslow = num2str(outvector(5));
			labeldatabe{end+1} = 'Scaling Slow Muons (Be)';

			samplebe.prBe = num2str(outvector(6));
			labeldatabe{end+1} = 'Production Be Spallation';

			samplebe.prMu = num2str(outvector(7));
			labeldatabe{end+1} = 'Production Muon Spallation (Be)';

			samplebe.qs = num2str(outvector(8));
			labeldatabe{end+1} = 'Qs (Be)';

			samplebe.qmu = num2str(outvector(9));
			labeldatabe{end+1} = 'Qmu (Be)';
			
			samplebe.internalUncertainty = num2str(outvector(12));
			labeldatabe{end+1} = 'Analytical (Internal) Uncertainty (Be)';

			if(length(lastwarn)>0); samplebe.warning = lastwarn; end;
			
			% Generate Output Data
			lprint(calcConfig.logFile,'Generating Output Data...\n');
			cellstruct = struct2cell(samplebe);
			outputdata = [outputdata '\n' sampleInputs(i).name ',' sprintf('%s,',cellstruct{:}) ];
			labeldatabe = [sprintf('%s,',labeldatabe{:}) ];
			samples(i).be = samplebe;
			
			be_time = toc(BeID);
			lcprint(calcConfig.userLogFile,' %s +/- %s ka completed in %s\n',samplebe.age, samplebe.uncertainty, sec2hms(be_time));
			if (~calcConfig.debug)
				running_times.be.surface.(RawInputs(i).scaling).average = (running_times.be.surface.(RawInputs(i).scaling).average*running_times.be.surface.(RawInputs(i).scaling).count + be_time)/(running_times.be.surface.(RawInputs(i).scaling).count + 1);
				if (running_times.be.surface.(RawInputs(i).scaling).count < 100000) running_times.be.surface.(RawInputs(i).scaling).count = running_times.be.surface.(RawInputs(i).scaling).count + 1; end
			end
		end
		
		%Compute Al
		if hasal == 1
			AlID = tic;
			if (running_times.al.surface.(RawInputs(i).scaling).count > 0)
				lcprint(calcConfig.userLogFile,'\t\t\tAl Aging (average execution time for %s is %s)...',RawInputs(i).scaling,sec2hms(running_times.al.surface.(RawInputs(i).scaling).average));
			else
				lcprint(calcConfig.userLogFile,'\t\t\tAl Aging (%s)...',RawInputs(i).scaling);
			end
			if (calcConfig.debug)
				lprint(calcConfig.logFile,'Generating debug data...\n');
				outvector = 1:100;times = 1:100;plotprodal = 1:1:100;
			else
				path = genpath(fullfile(pwd,'..',lower(RawInputs(1).scaling)));
				lprint(calcConfig.logFile,'Adding scaling scheme to matlab path...\n');
					addpath( path );
				lprint(calcConfig.logFile,'Calling al26age\n');
					[outvector,times,plotprodal] = al26age(sampleData{i}, sampleErrData{i}, RawInputs(i).scaling);
				lprint(calcConfig.logFile,'Done Calling al26age\n');
				%Clean up the path
				lprint(calcConfig.logFile,'Removing scaling scheme from matlab path...\n');
					rmpath( path );
			end

			%outputs
			% 1. age (kyr)
			% 2. age uncertainty 
			% 3. Contemporary Elevation/latitude scaling factor for neutrons for Be (unitless)
			% 4. Contemporary Elevation/latitude scaling factor for fast muons (unitless)
			% 5. Contemporary Elevation/lat scaling factor for slow muons (unitless)
			% 6. Contemporary depth avg prod rate, neutron spallation (atoms/g/yr)
			% 7. Contemporary depth avg prod rate, muons (atoms/g/yr)
			% 8. Qs (unitless)
			% 9. Qmu (unitless)
			% 10. Inherited 10-Be (atoms/g of target)
			% 11. Measured 10-Be (atoms/g of target)
			% 12. Analytical (internal) uncertainty (kyr)
			
			% Construct output structure for this sample
			sampleal = [];
			labeldataal = {};
			
			% Construct output structure for this sample
			[sample_age, sample_uncert] = age2str(outvector(1),outvector(2),2);
			
			sampleal.age = sample_age;
			labeldataal{end+1} = 'Age (Al)';

			sampleal.uncertainty = sample_uncert;
			labeldataal{end+1} = 'Total Uncertainty (Al)';

			sampleal.erosionrate = num2str(sampleInputs(i).erosionrate);
			labeldataal{end+1} = 'Erosion Rate';

			sampleal.elsfneutron = num2str(outvector(3)); 
			labeldataal{end+1} = 'Scaling Al Spallation';

			sampleal.elsfmuonfast = num2str(outvector(4)); 
			labeldataal{end+1} = 'Scaling Fast Muons (Al)';

			sampleal.elsfmuonslow = num2str(outvector(5)); 
			labeldataal{end+1} = 'Scaling Slow Muons (Al)';

			sampleal.prAl = num2str(outvector(6)); 
			labeldataal{end+1} = 'Production Al Spallation';

			sampleal.prMu = num2str(outvector(7)); 
			labeldataal{end+1} = 'Production Muon Spallation (Al)';

			sampleal.qs = num2str(outvector(8));
			labeldataal{end+1} = 'Qs (Al)';

			sampleal.qmu = num2str(outvector(9));
			labeldataal{end+1} = 'Qmu (Al)';
			
			sampleal.internalUncertainty = num2str(outvector(12));
			labeldataal{end+1} = 'Analytical (Internal) Uncertainty (Al)';

			if(length(lastwarn)>0); sampleal.warning = lastwarn; end;
		
			% Generate Output Data
			lprint(calcConfig.logFile,'Generating Output Data...\n');
			cellstruct = struct2cell(sampleal);
			if (hasbe == 0)
				outputdata = [outputdata '\n' sampleInputs(i).name ','];
			end
			outputdata = [outputdata sprintf('%s,',cellstruct{:}) ];
			labeldataal = [sprintf('%s,',labeldataal{:}) ];
			samples(i).al = sampleal;
			
			al_time = toc(AlID);
			lcprint(calcConfig.userLogFile,' %s +/- %s ka completed in %s\n',sampleal.age, sampleal.uncertainty, sec2hms(al_time));
			if (~calcConfig.debug)
				running_times.al.surface.(RawInputs(i).scaling).average = (running_times.al.surface.(RawInputs(i).scaling).average*running_times.al.surface.(RawInputs(i).scaling).count + al_time)/(running_times.al.surface.(RawInputs(i).scaling).count + 1);
				if (running_times.al.surface.(RawInputs(i).scaling).count < 100000) running_times.al.surface.(RawInputs(i).scaling).count = running_times.al.surface.(RawInputs(i).scaling).count + 1; end
			end
		end
		%Generate plotdata for sample	
		lprint(calcConfig.logFile,'Generating Sample Plot & Data...\n');
		figure;
		hold all;
		plotdata = [plotdata '\n' sampleInputs(i).name '\n' ...
				'Times Before 2010 (Be),' sprintf('%d,',times) '\n'];
		legends = {};
		if hasbe == 1; plotdata = [plotdata 'ProdBe,' sprintf('%d,',plotprodbe) '\n']; plot(times,plotprodbe); legends{end+1} = 'Be'; end;
		if hasal == 1; plotdata = [plotdata 'ProdAl,' sprintf('%d,',plotprodal) '\n']; plot(times,plotprodal); legends{end+1} = 'Al'; end;
		legend(legends);
		xlabel('Time Before 2010 (ka)');
		ylabel('Production Rate (atoms/g sample/yr)');
		hold off;
		plotfile = [char(inputs.prefix) '_' num2str( i ) '_' sampleInputs(i).name '_al_be.png'];
		set(gcf,'PaperPositionMode','auto');
		lprint(calcConfig.logFile,'Saving Plot...\n');
		saveas(gcf,[OutputData.tempDir '/' plotfile]);
		plotfiles{i} = plotfile;
		
		if hasal==1 && hasbe==1
			%put banana plot code here later
		end
	
		element_time = toc(ElementID);
		lprint(calcConfig.logFile,['Element completed in ' sec2hms(element_time) '\n']); 
		lprint(calcConfig.logFile,'************************************************\n');
		lcprint(calcConfig.userLogFile,'\t\t\tElement completed in %s\n',sec2hms(element_time));
	end
	%labeldata is comprised of be then al, if one or the other was never run it will be an empty array leading to no labels for that nuclide
	labeldata = ['Sample Name,' labeldatabe labeldataal];
end