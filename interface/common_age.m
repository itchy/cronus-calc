function [output,cache]=common_age(inputs,nuclide,cache)

	calcConfig = loadConfiguration( inputs );
	diary( calcConfig.logFile );
	
	try
		cache = prepareCache( cache, inputs, calcConfig );
		[output,cache] = runSamples(inputs, nuclide, calcConfig, cache);
	catch err
		output = processError( err, calcConfig );
	end
	
	diary off;

end

function calcConfig = loadConfiguration( inputs )

	if (~exist('config.ini','file'))
		error('Config.ini not found in same dir as common_age.m');
	end
	calcConfig = ini2struct('config.ini');
	calcConfig.webmasterEmail = 'CRONUSEarth@gmail.com';
	calcConfig.prefix = inputs.prefix;
	calcConfig.tempDir = inputs.tempDir;
	calcConfig.profile_dir = [char(calcConfig.tempDir) '/profile_' calcConfig.prefix];
	calcConfig.userLogFile = [char(calcConfig.tempDir) '/log_' calcConfig.prefix '.txt'];
	calcConfig.logFile = [char(calcConfig.tempDir) '/adv_log_' calcConfig.prefix '.txt'];
	
	if (isfield(calcConfig,'profileEnabled') && calcConfig.profileEnabled)
		calcConfig.profileEnabled = true;
		lprint(calcConfig.logFile,'Enabling profiler...\n');
		profile on -history;
	else
		calcConfig.profileEnabled = false;
	end
	
	calcConfig.userEmailPresent = false;
	calcConfig.userEmailSubject = '';
	if (isfield(inputs,'email') && ~isequal(inputs.email,'missing'))
		validateEmailSettings( calcConfig );
		calcConfig.userEmail = inputs.email;
		calcConfig.userEmailPresent = true;
		if (isfield(inputs,'subject') && ~isequal(inputs.subject,'missing'))
			calcConfig.userEmailSubject = inputs.subject;
		end
	end
	
	%Set debug
	if (strcmp(inputs.debug,'debug'))
		calcConfig.debug = true;
	else
		calcConfig.debug = false;
	end
	
	%Set depth
	if (isfield(inputs,'depth') && strcmp(inputs.depth,'on'))
		calcConfig.depth = true;
		if (strcmp(inputs.age_min,'missing') || strcmp(inputs.age_max,'missing') || strcmp(inputs.age_points,'missing') || ...
			strcmp(inputs.inheritance_min,'missing') || strcmp(inputs.inheritance_max,'missing') || strcmp(inputs.inheritance_points,'missing') || ...
			strcmp(inputs.erosion_min,'missing') || strcmp(inputs.erosion_max,'missing') || strcmp(inputs.erosion_points,'missing') || ...
			strcmp(inputs.erosion_total,'missing') || strcmp(inputs.erosion_units,'missing'))
				error(['**************************' 10 'Depth Profile Data: Each item in the Depth Profile Data fieldset is required' 10 '**************************']);
		else
			%Fields exist, convert to double and check values
			inputs.age_min = str2double(inputs.age_min); inputs.age_max = str2double(inputs.age_max); inputs.age_points = str2double(inputs.age_points);
			inputs.inheritance_min = str2double(inputs.inheritance_min); inputs.inheritance_max = str2double(inputs.inheritance_max); inputs.inheritance_points = str2double(inputs.inheritance_points); 
			inputs.erosion_min = str2double(inputs.erosion_min); inputs.erosion_max = str2double(inputs.erosion_max); inputs.erosion_points = str2double(inputs.erosion_points); 
			inputs.erosion_total = str2double(inputs.erosion_total); if (isfield(inputs,'erosion_density'))inputs.erosion_density = str2double(inputs.erosion_density); end 
			if (inputs.age_min >= 0 && inputs.age_max >= 0 && inputs.age_points > 0 && ...
				inputs.inheritance_min >= 0 && inputs.inheritance_max >= 0 && inputs.inheritance_points > 0 && ...
				inputs.erosion_points > 0)
				%Ok, depth looks good
			else
				 error(['**************************' 10 'Depth Profile Data: Age Min/Max/Points and Inheritance Min/Max/Points and Erosion Points cannot be negative' 10 '**************************']);
			end
		end
	else
		calcConfig.depth = false;
	end
	
end

function outputData = setupOutputData( inputs, calcConfig )
	tic;
	lprint(calcConfig.logFile,'Generating output data struct...');
	
	outputData.wrapperVersion = '0.2';
	outputData.calcVersion = '1.0';
	outputData.date = datestr(now);
	
	
	outputData.tempDir = relativepath(char(calcConfig.tempDir));
	outputData.inputDataFile = [char(calcConfig.prefix) '_inputs.csv'];
	outputData.outputDataFile = [char(calcConfig.prefix) '_results.csv'];
	outputData.plotDataFile = [char(calcConfig.prefix) '_plotData.csv'];
	outputData.localmap = [char(calcConfig.prefix) '_map.png'];
	outputData.zip_file = [char(calcConfig.prefix) '.zip'];
	outputData.inputData = '';
	outputData.outputData = '';
	outputData.plotData = '';
	
	lcprint(calcConfig.logFile,['    Structs created in ' num2str(toc) '\n']); 
end

function inputXMLDOM = parseInputsToXML( inputs, calcConfig )
	% Parse input HTTP parameter sampleData as a Java XML DOM object
	tic;
	lprint(calcConfig.logFile,'Parsing inputs...');
	try
		inputXMLDOM = cronusCalculators.xml.XMLModule.parseXMLstring(inputs.sampleData);
	catch err
		error( 'Input is not a well-formed XML! ' + getReport( err ) );
	end
	lcprint(calcConfig.logFile,['    Inputs parsed in ' num2str(toc) '\n']);
end

function validateInputXML( inputXMLDOM, cache, calcConfig )
	% Validate the input against the schema file. If validation fails, a Java
	% exception is thrown here and caught by the servlet.
	tic;
	lprint(calcConfig.logFile,'Validating inputs against schema...');
	
	if (calcConfig.depth)
		cache.depthValidator.validate( inputXMLDOM );
	else
		cache.surfaceValidator.validate( inputXMLDOM );
	end

	lcprint(calcConfig.logFile,['    Inputs validated in ' num2str(toc) '\n']); 
end

function [output,cache] = runSamples(inputs, nuclide, calcConfig, cache)
	lprint(calcConfig.logFile,'Starting runSamples\n');
	lprint(calcConfig.userLogFile,'Sample Preprocessing Starting...');

	ticID = tic;
	
	outputData = setupOutputData( inputs, calcConfig );
	inputXMLDOM = parseInputsToXML( inputs, calcConfig ); 
	validateInputXML( inputXMLDOM, cache, calcConfig );
	
	% Loop through each sample element in the input XML
	inputNodes = inputXMLDOM.getFirstChild.getChildNodes;
	numSamples = inputNodes.getLength;
	
	if (calcConfig.depth && numSamples < 3)
		error(['Too few samples for depth calculation. Please include at least 3 samples ' ...
			'(the recommended minimum for reduced uncertainties is 6 samples)']);
	end
	
	sendSampleStatusEmail( inputs, calcConfig );
	
	[ sampleInputs, sampleStructs, inputData, latitudes, longitudes ] = processSampleInput( nuclide, inputNodes, calcConfig );
	
	world_map( latitudes, longitudes, nuclide, outputData.tempDir, outputData.localmap, calcConfig.logFile );
	
	lcprint(calcConfig.userLogFile,'Completed in %s seconds\n',num2str(toc(ticID)));
	
	%Call nuclide code
	lprint(calcConfig.logFile,'Calling nuclide code...\n');
	[sample resultsdata plotData labeldata] = ageSamplesForNuclide( nuclide, inputs, sampleStructs, sampleInputs, outputData, numSamples, calcConfig );
	
	tic;
	lprint(calcConfig.logFile,'Writing output data...');
	outputData.outputData = [labeldata '\n' resultsdata];
	outputData.plotData = [plotData];
	outputData.inputData = inputData;
	lcprint(calcConfig.logFile,['    Output data written in ' num2str(toc) '\n']); 
	
	% Check if this sample already had a output or geochronvalues 
	% child(s) in the input XML. If so, delete them.
	
	%hasgeochron = false;
	%sChildren = sampleNode.getChildNodes;
	%nSChildren = sChildren.getLength;
	%for(j=0:nSChildren-1)
	%	sChild = sChildren.item(j);
	%	if(sChild.getNodeName.equals('output'))
	%		% remove this output child
	%		sampleNode.removeChild(sChild);
	%	elseif(sChild.getNodeName.equals('geochronvalues'))
	%		% already had a geochronvalues child
	%		% we can read the values from sampleStruct
	%		hasgeochron = true;
	%		sampleNode.removeChild(sChild);
	%	end
	%end
	
	  
	% Now build output and geochronvalues elements and append to the sample.
	% struct2node function converts the input structure to a DOM node in the
	% context of inputXMLDOM. The last argument is the tag name of the
	% returned element.
	for i=1:length(sample)
		outChild = struct2node(inputXMLDOM,sample(i),'output');
		inputNodes.item(i-1).appendChild(outChild);
	end
	
	writeOutputDataToFiles( outputData, calcConfig );
	
	tic;
	lprint(calcConfig.logFile,'Processing results...\n');
	outputData.time_elapsed = sec2hms(toc(ticID));
	outputDataChild = struct2node(inputXMLDOM,outputData,'output_data');
	inputXMLDOM.getFirstChild.appendChild(outputDataChild);
	
	% Calculation finished.
	
	% Save the XML to the temp folder in case user wants to send it
	% to the database
	xmlFilePath = [char(calcConfig.tempDir) '/' char(calcConfig.prefix) '.xml'];
	xmlwrite(xmlFilePath,inputXMLDOM);
	    
	% Now add additional information to the xml
	% This additional info will be used to make database uploading functionality
	% work
	inputXMLDOM.getDocumentElement.setAttribute('xmlFileLocalLocation',xmlFilePath);
	    
	% Transform the XML to a HTML page using xslt we loaded into the cache
	if (calcConfig.depth)
		cache.depthTransformer.updateStylesheet;
		resHTML = cache.depthTransformer.transform(inputXMLDOM);
	else
		cache.surfaceTransformer.updateStylesheet;
		resHTML = cache.surfaceTransformer.transform(inputXMLDOM);
	end
	out = java.io.BufferedWriter(java.io.FileWriter([char(calcConfig.tempDir) '/' char(calcConfig.prefix) '.html']));
	out.write(resHTML);
	out.close();
	
	% Generate zip file (turn off the warning message that the zip file cannot zip itself)
	warning off;
	zip([outputData.tempDir '/' outputData.zip_file],[char(calcConfig.prefix) '*.*'], outputData.tempDir);
	warning on;
	
	% Remove 'ROOT' from path (since ROOT is hidden in the urls)
	[ htmlUrl, xmlUrl ] = getResultsURL( calcConfig );
	
	if(strcmpi(inputs.returnType,'html'))    
		output.write = ['<html><head><meta http-equiv=''refresh'' content=''0; url=' htmlUrl '''></head></html>'];
	    output.contentType = 'html';
		lcprint(calcConfig.userLogFile,'\n\nProcessing Complete - output can be viewed <a href="%s">here</a>\n', htmlUrl );
	else
	    output.contentType = 'text';
	    output.write = xmlwrite(inputXMLDOM);
	    lcprint(calcConfig.userLogFile,'\n\nProcessing Complete - output can be viewed <a href="%s">here</a>\n', xmlUrl );
	end
	
	sendSampleResults( inputs, sampleInputs, numSamples, outputData, calcConfig );
	
	lprint(calcConfig.logFile,['Results processed in ' num2str(toc) '. Exiting' '\n\nAppending MATLAB diary output (if any):\n']); 
	
	if (calcConfig.profileEnabled)
		profile off;
		profsave(profile('info'),calcConfig.profile_dir);
	end
	rmpath(genpath(fullfile('..','common')));


end 

function writeOutputDataToFiles( outputData, calcConfig )
	lprint(calcConfig.logFile,'Writing output files...\n');
	
	fid = fopen( [ outputData.tempDir '/' outputData.inputDataFile ], 'w' );
		fprintf( fid, outputData.inputData );
		fclose( fid );
	fid = fopen( [ outputData.tempDir '/' outputData.outputDataFile], 'w' );
		fprintf( fid, outputData.outputData );
		fclose( fid );
	if ( length( outputData.plotData ) > 0 )
		fid = fopen( [ outputData.tempDir '/' outputData.plotDataFile ], 'w' );
			fprintf( fid, outputData.plotData );
			fclose( fid );
	end
	
	lcprint( calcConfig.logFile, ['    Files written in ' num2str(toc) '\n' ] ); 
end

function [sample resultsdata plotData labeldata] = ageSamplesForNuclide( nuclide, inputs, sampleStructs, sampleInputs, outputData, numSamples, calcConfig )
	
	switch nuclide
		case 'cl'
			nuclideFxn = @cl_36;
		case 'albe'
			nuclideFxn = @al_be;
		case 'he'
			nuclideFxn = @he_3;
		case 'c'
			nuclideFxn = @c_14;
	end
	[sample resultsdata plotData labeldata] = nuclideFxn(inputs, sampleStructs, sampleInputs, outputData, numSamples, calcConfig);

end

function cache = prepareCache( cache, inputs, calcConfig )
	tic;
	lprint(calcConfig.logFile,'Loading cache...');
	if(~isfield(cache,'surfaceTransformer') || ~isfield(cache,'surfaceValidator'))
		% Load transformer and validator objects into the cache
		cache.surfaceTransformer = cronusCalculators.xml.XMLTransformer(inputs.stylesheetFile);
		cache.surfaceValidator = cronusCalculators.xml.XMLValidator(inputs.schemaFile);
	end
	if (calcConfig.depth && (~isfield(cache,'depthValidator') || ~isfield(cache,'depthTransformer')))
		% Load transformer and validator objects into the cache
		cache.depthTransformer = cronusCalculators.xml.XMLTransformer(inputs.depthStylesheetFile);
		cache.depthValidator = cronusCalculators.xml.XMLValidator(inputs.depthSchemaFile);
	end
	if (calcConfig.depth)
		cache.depthValidator.updateSchemaIfNewer;
	else
		cache.surfaceValidator.updateSchemaIfNewer;
	end
	
	lcprint(calcConfig.logFile,['    Cache loaded in ' num2str(toc) '\n']); 
end

function [ htmlUrl, xmlUrl ] = getResultsURL( calcConfig )
	url_prefix = [ calcConfig.serverName 'temp/' char( calcConfig.prefix ) ]
	htmlUrl = [ url_prefix '.html' ]
	xmlUrl = [ url_prefix '.xml' ]
end

function statusUrl = getSampleStatusURL( calcConfig )
	statusUrl = [ calcConfig.serverName 'html/sample_status.xhtml?sample=' char( calcConfig.prefix ) ];
end

function validateEmailSettings( calcConfig )
	if ( ~hasSMTPServer( calcConfig ) )
		setpref( 'Internet', 'SMTP_Server', calcConfig.smtpServer );
	end
	if ( ~hasNoReplyEmail( calcConfig ) )
		setpref( 'Internet', 'E_mail', calcConfig.noReplyEmail );
	end

	function result = hasSMTPServer( calcConfig )
		result = ispref( 'Internet' ) && ...
				 ispref( 'Internet', 'SMTP_Server' ) && ...
				 strcmp( getpref( 'Internet', 'SMTP_Server' ), calcConfig.smtpServer );
	end
	function result = hasNoReplyEmail( calcConfig )
		result = ispref( 'Internet' ) && ...
				 ispref( 'Internet', 'E_mail' ) && ...
				 strcmp( getpref( 'Internet', 'E_mail'), calcConfig.noReplyEmail );
	end
end

function sendSampleStatusEmail( inputs, calcConfig )
	if (calcConfig.userEmailPresent)
		statusUrl = getSampleStatusURL( calcConfig );
		
		message = [ ...
			'Your samples are currently being processed by the CRONUS Earth Web Calculators. ' ...
			'If you would like to view the progress of your samples, please visit the following page: ' 10 ...
			10 statusUrl 10 ...
			10 'Thanks for using the CRONUS Earth Web Calculator!' 10 ...
	    ];
		SubjectMsg = '[CRONUS Calculators] ';
		if (calcConfig.debug)
			SubjectMsg = [SubjectMsg 'Debug '];
		end
		SubjectMsg = [SubjectMsg calcConfig.userEmailSubject ' Calculation Started'];
		
		sendmail(calcConfig.userEmail,SubjectMsg,message);
		lprint(calcConfig.logFile,'"Calculation Started" email sent to user\n');
	end
end

function sendSampleResults( inputs, sampleInputs, numSamples, outputData, calcConfig )
	if (calcConfig.userEmailPresent)
		htmlUrl = getResultsURL( calcConfig );
		
		message = ['Here are your CRONUS Calculator results for ' sampleInputs(1).name];
		if (numSamples-1 > 1)
			message = [message ' to ' sampleInputs(numSamples-1).name];
		end
		message = [ ...
			message '. Further information can be found at ' calcConfig.serverName 10 ...
			char(calcConfig.prefix) '.zip contains the following files:' 10 ...
			9 char(calcConfig.prefix) '.xml: XML output data' 10 ...
			9 char(calcConfig.prefix) '.html: HTML output file.' 10 ...
			9 9 'Open this file in a browser to view results in a web format.' 10 ...
			9 char(calcConfig.prefix) '_inputs.csv: Inputs (one sample per line).' 10 ...
			9 9 'These are your original inputs plus any other default parameters' 10 ...
			9 9 'that were calculated for your sample.' 10 ...
		];
		if (calcConfig.depth)
			message = [ ...
				message ...
				9 char(calcConfig.prefix) '_results.csv: Results' 10 ...
			];
		else
			message = [ ...
				message ...
				9 char(calcConfig.prefix) '_results.csv: Results (one sample per line).' 10 ... 
				9 9 'The derivatives for each input parameter are included in order ' 10 ...
				9 9 'to help you assess how the uncertainties on different parameters' 10 ... 
				9 9 'affect the uncertainty on the sample age.  Larger' 10 ...
				9 9 'values generally indicate a larger sensitivity to that parameter.' 10 ...
				9 char(calcConfig.prefix) '_plotData.csv: Age data used to create plots' 10 ...
			];
		end
		message = [ ...
			message ...
			9 char(calcConfig.prefix) '_''sample_name''.png: Images generated from plots' 10 ...
			'* Please note that your data is not saved on the server. The server deletes results automatically.' 10 ...
			'Your results can be viewed on the server at ' htmlUrl ' until ' 10 ...
			'the server deletes them in which case they can still be accessed in the attachment to this email.' 10 ...
		];
		SubjectMsg = '[CRONUS Calculators] ';
		if (calcConfig.debug)
		   SubjectMsg = [SubjectMsg 'Debug '];
		end
		
		SubjectMsg = [SubjectMsg calcConfig.userEmailSubject ' Results for ' sampleInputs(1).name];
		if (numSamples-1 > 1)
			SubjectMsg = [SubjectMsg ' to ' sampleInputs( numSamples - 1 ).name];
		end
		sendmail(calcConfig.userEmail,SubjectMsg,message,[outputData.tempDir '/' outputData.zip_file]);
	end
end

function [ sampleInputs, sampleStructs, inputData, latitudes, longitudes ] = processSampleInput( nuclide, inputNodes, calcConfig )
	numSamples = inputNodes.getLength;
	lprint(calcConfig.logFile,'Validating %d samples...\n',numSamples);
	
	updatedLambdaFe = false;
	latitudes = [];
	longitudes = [];
	sampleInputs = [];
	sampleStructs = [];
	inputData = [];
	lprint(calcConfig.logFile,'Aggregating and processing inputs...');
	addpath(genpath(fullfile('..','common')));
	for i=0:numSamples-1;
		sampleNode = inputNodes.item(i);
		[sampleStruct,sampleAttribs] = node2struct(sampleNode);
		
		% doublefields function tries to parse every field of the input struct
		% into double. Second argument (true) tells it to ignore non-numeric
		% and non-parseable fields.
		sampleInput = doublefields(sampleStruct.input,true);
		
		% store the latitudes and longitudes
		latitudes = [latitudes sampleInput.lat];
		longitudes = [longitudes sampleInput.long];
		
		% now sampleInput contains all input data in double type
		sampleInput.inheritance=0.0;
		sampleInput.inheritance_uncert=0.0;
		sampleInput.name = sampleStruct.input.name;
		sampleInput.erosionrate = sampleInput.erEpsilon*sampleInput.bd/10;
		sampleInput.erosionrate_uncert = sampleInput.erEpsilon_uncert*sampleInput.bd/10;
	
		if (strcmp(sampleStruct.input.pressure_elevOption,'Elevation'))
			sampleInput.elev = sampleInput.elevation;
			sampleInput.elev_uncert = sampleInput.elevation_uncert;
			sampleInput.ATM = ERA40atm(sampleInput.lat, sampleInput.long, sampleInput.elev);
			sampleStruct.input.pressure = num2str(sampleInput.ATM);
			sampleAttribs.input.pressure.ft = [ sampleAttribs.input.pressure.ft ' (calculated from elevation)'];
			sampleInput.ATM_uncert = 0.0;
		elseif (strcmp(sampleStruct.input.pressure_elevOption,'Pressure'))
			sampleInput.ATM = sampleInput.pressure;
			sampleInput.ATM_uncert = sampleInput.pressure_uncert;
			sampleInput.elev = Ptoelev( sampleInput.lat, sampleInput.long, sampleInput.pressure );
			sampleInput.elev_uncert = 0.0;
		else
			sampleInput.elev = sampleInput.elevation;
			sampleInput.elev_uncert = sampleInput.elevation_uncert;
			sampleInput.ATM = sampleInput.pressure;
			sampleInput.ATM_uncert = sampleInput.pressure_uncert;	
		end
		
		if (sampleInput.lambdafe == 0.0)
			lcprint(calcConfig.userLogFile,'\n\t\t\tSample %d/%d needs attenuation length. Calculating...',i+1,numSamples);
			lprint(calcConfig.logFile,'Sample %d attenuation length is 0, calculating...',i+1);
			lambdafeID = tic;
			updatedLambdaFe = true;
			addpath(genpath(fullfile('..','sa'))); % Needed for physpars call in attenuationlength
			sampleInput.lambdafe = attenuationlength(sampleInput.lat, sampleInput.long, sampleInput.elev, sampleInput.ATM);
			rmpath(genpath(fullfile('..','sa')));
			sampleStruct.input.lambdafe = num2str(sampleInput.lambdafe);
			sampleAttribs.input.lambdafe.ft = [ sampleAttribs.input.lambdafe.ft ' (calculated)' ];
			lcprint(calcConfig.logFile,[' result is ' num2str(sampleInput.lambdafe) ' completed in ' sec2hms(toc(lambdafeID)) '\n']);
			lcprint(calcConfig.userLogFile,[' result is ' num2str(sampleInput.lambdafe) ' completed in ' sec2hms(toc(lambdafeID))]);
		end
		sampleInputs = [sampleInputs; sampleInput];
		sampleStructs = [sampleStructs; sampleStruct.input];
		
		%Write inputData
		inputcellstruct = struct2cell(sampleStruct.input);
		if (strcmp(nuclide,'cl'))
			%Remove the extra inputs added onto the end of the input data by the javascript
			inputcellstruct(end-4:end) = [];
		end
		if (isempty(inputData))
			%Write labels at top of file
			fNames = fieldnames(sampleAttribs.input);
			nFields = length(fNames);
			for i=1:nFields
				field = sampleAttribs.input.(fNames{i});
				if (isfield(field,'ft'))
					inputData = [inputData sprintf('%s,',field.ft)];
				end;
			end;
		end;
		inputData = [inputData '\n' sprintf('%s,',inputcellstruct{:})];
		
	end;
	if (updatedLambdaFe), lcprint(calcConfig.userLogFile,'\n\t\t\t'); end;
	lcprint(calcConfig.logFile,['    Inputs processed in ' num2str(toc) '\n']);
end
