function output = processError( err, calcConfig )
% New user facing errors are caught here in the if/elseif

	errorType = 'system';
	errorMessage = '';
	if ( isa( err, 'matlab.exception.JavaException' ) )
		errorType = 'user';
		errorMessage = inputValidationErrorMessage();
	elseif ( strcmp( err.identifier, 'MATLAB:sendmail:AddressError' ) )
		errorType = 'user';
	    errorMessage = emailAddressErrorMessage();
	end
	
	errorReport = getReport( err, 'basic' );
	output = formatErrorOutputAndEmail( errorType, errorMessage, errorReport, err, calcConfig );
end
 
function output = formatErrorOutputAndEmail( errorType, errorMessage, errorReport, err, calcConfig )
	output.type = 'text';
	if ( strcmp( errorType, 'user' ) )
		emailSubjectPrefix = [ '[ USER-ERROR ] [' calcConfig.prefix '] ' ];
		output.write = [ errorMessage 10 ...
			'See error message below for details. For questions, please email ' 10 ...
			calcConfig.webmasterEmail '.' 10 ...
			10 ...
			errorReport
		];
	else
		emailSubjectPrefix = '[ ERROR ] ';
		output.write = [ ...
			'We apologize, but an error has occurred. '...
			'We have been notified of the error. '...
			'If we need more information, we will email the address provided from ' 10 ...
			10 calcConfig.webmasterEmail 10 ...
			'and ask for more details.'
		];
	end
	sendWebmasterErrorEmail( err, emailSubjectPrefix, calcConfig );
end

function msg = emailAddressErrorMessage()
	msg = 'The email address you entered appears to be invalid. Please double-check the email address for errors.';
end

function msg = inputValidationErrorMessage()
	msg = 'The inputs you entered are not valid for this calculator. ';
end

function validateEmailSettings( calcConfig )
	if ( ~hasSMTPServer( calcConfig ) )
		setpref( 'Internet', 'SMTP_Server', calcConfig.smtpServer );
	end
	if ( ~hasNoReplyEmail( calcConfig ) )
		setpref( 'Internet', 'E_mail', calcConfig.noReplyEmail );
	end

	function result = hasSMTPServer( calcConfig )
		result = ispref( 'Internet' ) && ...
				 ispref( 'Internet', 'SMTP_Server' ) && ...
				 strcmp( getpref( 'Internet', 'SMTP_Server' ), calcConfig.smtpServer );
	end
	function result = hasNoReplyEmail( calcConfig )
		result = ispref( 'Internet' ) && ...
				 ispref( 'Internet', 'E_mail' ) && ...
				 strcmp( getpref( 'Internet', 'E_mail'), calcConfig.noReplyEmail );
	end
end

function sendWebmasterErrorEmail( err, subjectPrefix, calcConfig )
	validateEmailSettings( calcConfig );
	subject = [ subjectPrefix err.identifier ': ' err.message ];
	subject = strrep( subject, 10, '' );
	message = [ subject 10 10 ...
				'User email: ' calcConfig.userEmail 10 ...
				'User subject: ' calcConfig.userEmailSubject 10 ...
				'Run prefix: ' calcConfig.prefix 10 ...
				getReport( err ) ];

	try
		message = [ message 10 10 'Log:' 10 fileread( calcConfig.log_file ) ];
	catch
		% Do nothing if the file read fails
	end

	sendmail( calcConfig.webmasterEmail, subject, message );
end
