function lprint(log_file,msg,varargin)
   if (isempty(log_file)) return; end;
   log_id = fopen(log_file,'a');
   if (numel(varargin) > 0)
      Message = sprintf(msg,varargin{:});
   else
      Message = msg;
   end
   fprintf(log_id,['[' datestr(now) '] ' Message]);
   fprintf(['[' datestr(now) '] ' Message]);
   fclose(log_id);   
end
