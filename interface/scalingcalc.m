%To provide scaling factors given simple inputs

function out=scalingcalc(lat, long, elevation);

load pmag_consts

tdsfsample.lat=lat;
tdsfsample.long=long;

tdsfsample.elevation=elevation;
tdsfsample.scaling='all';

%Calculate pressure
tdsfsample.pressure=ERA40atm(lat,long,elevation);

tdsf=get_tdsf(tdsfsample,pmag_consts);

%if there is a t, use that. If not, use current (0). 

t=0;

% Handle extremely old or extremely young times. 
%
if (t<-max(tdsf.tv)/1000)
  t=-max(tdsf.tv)/1000
  warning('extremely old time in getcurrentsf!');
end
%
% Look for times after 2110 AD.  This is most likely an error.
% 
if (t>0.3)
  t
  warning('extremely young time in getcurrentsf!');
  t=0;
end
%
% For all times after t=0 (2010), use the 2010 scaling factors by
% forcing t=0.  We don't let this get too far out of hand- this is
% limited to 2110 (see above.)
%
if (t > 0)
  t=0.0;
end
%
% % This version for the Desilets and Zreda scheme.
% %
% out.DeSel10=interpolate(tdsf.tv,tdsf.SF_De,-t*1000);
% out.DeSel26=out.DeSel10;
% out.DeSel14=out.DeSel10;
% out.DeSel3=out.DeSel10;
% out.DeSel36Ca=out.DeSel10;
% out.DeSel36K=out.DeSel10;
% out.DeSel36Ti=out.DeSel10;
% out.DeSel36Fe=out.DeSel10;
% out.DeSel21=out.DeSel10;
% out.DeSFth=out.DeSel10;
% out.DeSFeth=out.DeSel10;
% %
% % This version for the Dunai scheme.
% %
% out.DuSel10=interpolate(tdsf.tv,tdsf.SF_Du,-t*1000);
% out.DuSel26=out.DuSel10;
% out.DuSel14=out.DuSel10;
% out.DuSel3=out.DuSel10;
% out.DuSel36Ca=out.DuSel10;
% out.DuSel36K=out.DuSel10;
% out.DuSel36Ti=out.DuSel10;
% out.DuSel36Fe=out.DuSel10;
% out.DuSel21=out.DuSel10;
% out.DuSFth=out.DuSel10;
% out.DuSFeth=out.DuSel10;
% %
% % This version for the Lifton scheme.
% %
% out.LiSel10=interpolate(tdsf.tv,tdsf.SF_Li,-t*1000);
% out.LiSel26=out.LiSel10;
% out.LiSel14=out.LiSel10;
% out.LiSel3=out.LiSel10;
% out.LiSel36Ca=out.LiSel10;
% out.LiSel36K=out.LiSel10;
% out.LiSel36Ti=out.LiSel10;
% out.LiSel36Fe=out.LiSel10;
% out.LiSel21=out.LiSel10;
% out.LiSFth=out.LiSel10;
% out.LiSFeth=out.LiSel10;
% %
% % This version for the time dependent Lal scheme.
% %
% out.LmSel10=interpolate(tdsf.tv,tdsf.SF_Lm,-t*1000);
% out.LmSel26=out.LmSel10;
% out.LmSel14=out.LmSel10;
% out.LmSel3=out.LmSel10;
% out.LmSel36Ca=out.LmSel10;
% out.LmSel36K=out.LmSel10;
% out.LmSel36Ti=out.LmSel10;
% out.LmSel36Fe=out.LmSel10;
% out.LmSel21=out.LmSel10;
% out.LmSFth=out.LmSel10;
% out.LmSFeth=out.LmSel10;
% 
% %
% % This version for the Lal/Stone scheme.  
% %
% out.StSel10=tdsf.SF_St;
% out.StSel26=out.StSel10;
% out.StSel14=out.StSel10;
% out.StSel3=out.StSel10;
% out.StSel36Ca=out.StSel10;
% out.StSel36K=out.StSel10;
% out.StSel36Ti=out.StSel10;
% out.StSel36Fe=out.StSel10;
% out.StSel21=out.StSel10;
% out.StSFth=out.StSel10;
% out.StSFeth=out.StSel10;
% %
% % This version for the Sato nuclide-dependent scheme.  Implements nuclide-dependent scaling
% % by incorporating cross-sections.
% %
% out.SaSel10=interpolate(tdsf.tv,tdsf.SF_Sa10,-t*1000);
% out.SaSel26=interpolate(tdsf.tv,tdsf.SF_Sa26,-t*1000);
% out.SaSel14=interpolate(tdsf.tv,tdsf.SF_Sa14,-t*1000);
% out.SaSel3=interpolate(tdsf.tv,tdsf.SF_Sa3,-t*1000);
% out.SaSel21=out.SaSel10;
% out.SaSel36Ca=interpolate(tdsf.tv,tdsf.SF_Sa36Ca,-t*1000);
% out.SaSel36K=interpolate(tdsf.tv,tdsf.SF_Sa36K,-t*1000);
% out.SaSel36Ti=interpolate(tdsf.tv,tdsf.SF_Sa36Ti,-t*1000);
% out.SaSel36Fe=interpolate(tdsf.tv,tdsf.SF_Sa36Fe,-t*1000);
% out.SaSFth=interpolate(tdsf.tv,tdsf.SF_Sath,-t*1000);
% out.SaSFeth=interpolate(tdsf.tv,tdsf.SF_Saeth,-t*1000);
% %
% %uncomment and change below if we go back to time-dependent muons
% %
% %SFmufast=interpolate(sf.tdsf.tv,sf.tdsf.SFmu_Li,-t*1000);
% %SFmuslow=interpolate(sf.tdsf.tv,sf.tdsf.SFmu_Li,-t*1000);
% %
% % This version is for the Sato flux scaling scheme (NOT nuclide dependent)
% %
% out.SfSel10=interpolate(tdsf.tv,tdsf.SF_Sf,-t*1000);
% out.SfSel26=out.SfSel10;
% out.SfSel14=out.SfSel10;
% out.SfSel3=out.SfSel10;
% out.SfSel36Ca=out.SfSel10;
% out.SfSel36K=out.SfSel10;
% out.SfSel36Ti=out.SfSel10;
% out.SfSel36Fe=out.SfSel10;
% out.SfSel21=out.SfSel10;
% out.SfSFth=out.SfSel10;
% out.SfSFeth=out.SfSel10;

%Would be nice to be able to sort by nuclide or by scaling scheme!

out.DeSel10=interpolate(tdsf.tv,tdsf.SF_De,-t*1000);

%
% This version for the Dunai scheme.
%
out.DuSel10=interpolate(tdsf.tv,tdsf.SF_Du,-t*1000);

%
% This version for the Lifton scheme.
%
out.LiSel10=interpolate(tdsf.tv,tdsf.SF_Li,-t*1000);
%
% This version for the time dependent Lal scheme.
%
out.LmSel10=interpolate(tdsf.tv,tdsf.SF_Lm,-t*1000);


%
% This version for the Lal/Stone scheme.  
%
out.StSel10=tdsf.SF_St;

%
% This version for the Sato nuclide-dependent scheme.  Implements nuclide-dependent scaling
% by incorporating cross-sections.
%
out.SaSel10=interpolate(tdsf.tv,tdsf.SF_Sa10,-t*1000);
out.SaSel26=interpolate(tdsf.tv,tdsf.SF_Sa26,-t*1000);
out.SaSel14=interpolate(tdsf.tv,tdsf.SF_Sa14,-t*1000);
out.SaSel3=interpolate(tdsf.tv,tdsf.SF_Sa3,-t*1000);
out.SaSel21=out.SaSel10;
out.SaSel36Ca=interpolate(tdsf.tv,tdsf.SF_Sa36Ca,-t*1000);
out.SaSel36K=interpolate(tdsf.tv,tdsf.SF_Sa36K,-t*1000);
out.SaSel36Ti=interpolate(tdsf.tv,tdsf.SF_Sa36Ti,-t*1000);
out.SaSel36Fe=interpolate(tdsf.tv,tdsf.SF_Sa36Fe,-t*1000);
out.SaSFth=interpolate(tdsf.tv,tdsf.SF_Sath,-t*1000);
out.SaSFeth=interpolate(tdsf.tv,tdsf.SF_Saeth,-t*1000);
%
%uncomment and change below if we go back to time-dependent muons
%
%SFmufast=interpolate(sf.tdsf.tv,sf.tdsf.SFmu_Li,-t*1000);
%SFmuslow=interpolate(sf.tdsf.tv,sf.tdsf.SFmu_Li,-t*1000);
%
% This version is for the Sato flux scaling scheme (NOT nuclide dependent)
%
out.SfSel10=interpolate(tdsf.tv,tdsf.SF_Sf,-t*1000);

