function [sample, outputdata, plotdata, labeldata] = cl_36(inputs, RawInputs, sampleInputs, OutputData, sampleCount, calcConfig)
	% Wrapper for cl-36 calculator
	lprint(calcConfig.logFile,'Starting cl_36\n');
	totalSampleData = [];
	totalSampleErrData = [];
	
	for i=1:sampleCount;
		sampleInput = sampleInputs(i);
		% construct the input vector (order is important here).
		% I (hakan) don't recommend getting the input as such. We already have
		% all input in distinct variables so we can easily use them.

		%1.     Sample 36-Cl concentration (atoms of 36-Cl/g of target)
		%2.     Inheritance (atoms 36-Cl/g of target)  
		%3.     erosion-rate epsilon (g/(cm^2*kyr))
		%4.     fractional volumetric water-content (unitless) 
		%5.     bulk density (g/cm^3)
		%6.     sample thickness (cm)
		%7.     Latitude (decimal degrees, -90(S) to +90(N))
		%8.     Longitude (decimal degrees, 0-360 degrees east)
		%9.     Elevation (meters)
		%10.    Pressure (hPa)                Both 9 and 10 must be present!
		%11.    Shielding factor for terrain, snow, etc. (unitless)
		%12.    Effective attenuation length -Lambdafe (g/cm^2)
		%13.    % CO2                        Rock
		%14.    % Na2O                       Rock
		%15.    % MgO                        Rock
		%16.    % Al2O3                      Rock
		%17.    % SiO2                       Rock
		%18.    % P2O5                       Rock
		%19.    % K2O                        Rock
		%20.    % CaO                        Rock
		%21.    % TiO2                       Rock
		%22.    % MnO                        Rock
		%23.    % Fe2O3                      Rock
		%24.    Cl (ppm)                     Rock
		%25.    B (ppm)                      Rock
		%26.    Sm (ppm)                     Rock
		%27.    Gd (ppm)                     Rock
		%28.    U (ppm)                      Rock
		%29.    Th (ppm)                     Rock
		%30.    Cr (ppm)                     Rock
		%31.    Li (ppm)                     Rock
		%32.	Target element %K2O          Target
		%33.    Target element %CaO          Target
		%34.    Target element %TiO2         Target
		%35.    Target element %Fe2O3        Target
		%36.    Target element Cl (ppm)      Target
		%37.    Depth to top of sample (g/cm^2)
		%38.    Year sampled (e.g. 2010)
		sampleData = [ 
					sampleInput.lat
					sampleInput.long
					sampleInput.elev
					sampleInput.ATM
					sampleInput.sthck
					sampleInput.bd
					sampleInput.shieldingF
					sampleInput.erosionrate
					sampleInput.clconc
					sampleInput.inheritance
					sampleInput.lambdafe
					sampleInput.depthtosample
					sampleInput.yearsampled
					sampleInput.fwc
					sampleInput.SiO2
					sampleInput.TiO2
					sampleInput.Al2O3
					sampleInput.Fe2O3
					sampleInput.MnO
					sampleInput.MgO
					sampleInput.CaO
					sampleInput.Na2O
					sampleInput.K2O
					sampleInput.P2O5
					sampleInput.aw
					sampleInput.CO2
					
					sampleInput.Cl
					sampleInput.B
					sampleInput.Sm
					sampleInput.Gd
					sampleInput.U
					sampleInput.Th
					sampleInput.Cr
					sampleInput.Li
					sampleInput.targetK2O
					sampleInput.targetCaO
					sampleInput.targetTiO2
					sampleInput.targetFe2O3
					sampleInput.targetCl
                  ]; 
		
		% input vector for uncertainities (order is important here)
		sampleErrData = [
					sampleInput.lat_uncert
					sampleInput.long_uncert
					sampleInput.elev_uncert
					sampleInput.ATM_uncert
					sampleInput.sthck_uncert
					sampleInput.bd_uncert
					sampleInput.shieldingF_uncert
					sampleInput.erosionrate_uncert
					sampleInput.clconc_uncert
					sampleInput.inheritance_uncert
					sampleInput.lambdafe_uncert
					sampleInput.depthtosample_uncert
					sampleInput.yearsampled_uncert
					sampleInput.fwc_uncert
					
					sampleInput.SiO2_uncert
					sampleInput.TiO2_uncert
					sampleInput.Al2O3_uncert
					sampleInput.Fe2O3_uncert
					sampleInput.MnO_uncert
					sampleInput.MgO_uncert
					sampleInput.CaO_uncert
					sampleInput.Na2O_uncert
					sampleInput.K2O_uncert
					sampleInput.P2O5_uncert
					sampleInput.aw_uncert
					sampleInput.CO2_uncert
					
					sampleInput.Cl_uncert
					sampleInput.B_uncert
					sampleInput.Sm_uncert
					sampleInput.Gd_uncert
					sampleInput.U_uncert
					sampleInput.Th_uncert
					sampleInput.Cr_uncert
					sampleInput.Li_uncert
					sampleInput.targetK2O_uncert
					sampleInput.targetCaO_uncert
					sampleInput.targetTiO2_uncert
					sampleInput.targetFe2O3_uncert
					sampleInput.targetCl_uncert
					];

		totalSampleData{end+1} = [sampleData];
		totalSampleErrData{end+1} = [sampleErrData];
	end
	
	%Load running_times and overwrite running_times if it exists
	%Create running_times for cl
	running_times = struct();
	
	running_times_default.cl.depth.average = 0;
	running_times_default.cl.depth.count = 0;
	running_times_default.cl.surface.DE.average = 0; running_times_default.cl.surface.DE.count = 0;
	running_times_default.cl.surface.DU.average = 0; running_times_default.cl.surface.DU.count = 0;
	running_times_default.cl.surface.LI.average = 0; running_times_default.cl.surface.LI.count = 0;
	running_times_default.cl.surface.LM.average = 0; running_times_default.cl.surface.LM.count = 0;
	running_times_default.cl.surface.SA.average = 0; running_times_default.cl.surface.SA.count = 0;
	running_times_default.cl.surface.ST.average = 0; running_times_default.cl.surface.ST.count = 0;
	running_times_default.cl.surface.SF.average = 0; running_times_default.cl.surface.SF.count = 0;
	if (exist('running_times.mat') == 2)
		load('running_times','running_times');
	end
	%Merge the loaded variable with the defaults above in case they don't exist
	warning off;
	running_times = catstruct(running_times_default,running_times);
	warning on;
	
	if (calcConfig.depth)
		%Sort by depth, call depth code, and append plotfiles to output
		totalSampleDataMat = cell2mat(totalSampleData);
		[tmp ind] = sort(totalSampleDataMat(38,:));
		sortedSampleData = totalSampleData(ind);
		sortedSampleErrData = totalSampleErrData(ind);
		[sample, outputdata, plotdata, plotfiles, labeldata,running_times] = cl_36_depth(inputs, RawInputs, sortedSampleData, sortedSampleErrData, sampleInputs, sampleCount, OutputData, running_times, calcConfig);
		sample(1).plot = plotfiles;
	else
		%Call surface code, then add sample name and plotfiles to output
		[sample, outputdata, plotdata, plotfiles, labeldata,running_times] = cl_36_surface(inputs, RawInputs, totalSampleData, totalSampleErrData, sampleInputs, sampleCount, OutputData, running_times, calcConfig);
		for i=1:sampleCount;
			sample(i).name = sampleInputs(i).name;
			sample(i).plot = plotfiles{i};
		end
	end
	%Write out running_times
	save('running_times','running_times');
end

function [samples, outputdata, plotdata, plotfiles, labeldata, running_times] = cl_36_depth(inputs, RawInputs, sampleData, sampleErrData, sampleInputs, sampleCount, OutputData, running_times, calcConfig)
	samples = repmat(struct([]),1,1);
	outputdata = [];
	plotdata = [];
	labeldata = [];
	
	%Display start message (with average time if it exists and not debug)
	nuclide_time = tic;
	if (~calcConfig.debug && running_times.cl.depth.count > 0)
		lprint(calcConfig.logFile,'Started Cl Depth Profile (average execution time is %s)...\n', sec2hms(running_times.cl.depth.average));
		lprint(calcConfig.userLogFile,'Started Cl Depth Profile (average execution time is %s)...', sec2hms(running_times.cl.depth.average));
	else
		lprint(calcConfig.logFile,'Started Cl Depth Profile...\n');
		lprint(calcConfig.userLogFile,'Started Cl Depth Profile...');
	end

	%Convert erosion units if necessary
	if(strcmp(inputs.erosion_units,'cm'))
		inputs.erosion_total = inputs.erosion_total * inputs.erosion_density * 1000.0;
	elseif (strcmp(inputs.erosion_units,'mm'))
		inputs.erosion_total = inputs.erosion_total * inputs.erosion_density * 0.1;
	end
	
	% Define the parameter space
	lprint(calcConfig.logFile,'Defining parameter space...\n');
	erates=linspace(inputs.erosion_min,inputs.erosion_max,inputs.erosion_points)';
	ages=linspace(inputs.age_min,inputs.age_max,inputs.age_points)';
	inhers=linspace(inputs.inheritance_min,inputs.inheritance_max,inputs.inheritance_points)';

	%Determine which scaling scheme to use
	lprint(calcConfig.logFile,'Adding scaling scheme to matlab path...\n');
	path = genpath(fullfile(pwd,'..',lower(RawInputs(1).scaling)));
	addpath( path );

	%Call createage36 to get some extra data
	lprint(calcConfig.logFile,'Generating createage36 inputs...\n');
	createAgeInput = [];
	origLength = size(sampleData{1},1) + size(sampleErrData{1},1);
	for i=1:sampleCount
		createAgeInput(i,1:origLength) = cat(1,sampleData{i},sampleErrData{i});
		createAgeInput(i,origLength+1) = sampleInputs(i).covariance;
	end
	lprint(calcConfig.logFile,'Calling createage36...\n');
	[nominal36,uncerts36,cov36] = createage36(createAgeInput);
	% Createage recalculates erosion rate unit change, but we already did that
	nominal36(:,3) = createAgeInput(:,8);
	uncerts36(:,3) = createAgeInput(:,47);

	% Calculate depth-to-middle of the samples
	lprint(calcConfig.logFile,'Calculating depths...\n');
	depths = nominal36(:,37) + 0.5*nominal36(:,6).*nominal36(:,5);

	% Define the prior distribution
	lprint(calcConfig.logFile,'Defining prior distribution...\n');
	pr=prior(erates,ages,inhers,'uniform','uniform','uniform');

	lastwarn('');
	if (calcConfig.debug)
		lprint(calcConfig.logFile,'Generating debug data...\n');
		MAP = [2 1 3];
		
		figure(4);
		plot(1:100,1:1:100);
		xlabel('Test Figure');
		ylabel('Test Figure');
		figure(5);
		plot(1:100,100:-1:1);
		xlabel('Test Figure');
		ylabel('Test Figure');
		figure(6);
		plot(1:100,1:1:100);
		xlabel('Test Figure');
		ylabel('Test Figure');
		figure(7);
		plot(1:100,100:-1:1);
		xlabel('Test Figure');
		ylabel('Test Figure');

	else
		% Run ageprofilecalc36
		lprint(calcConfig.logFile,'Calling ageprofilecalc36...\n');
		[posterior_er,posterior_age,posterior_inher,MAP,mu_bayes,chi2grid,lhsurf,...
			jposterior]=ageprofilecalc36(nominal36,uncerts36(:,1),depths,...
			erates,ages,inhers,pr,inputs.erosion_total,lower(RawInputs(1).scaling));
			
		% Create plots
		lprint(calcConfig.logFile,'Generating plots...\n');
		makeplots36(erates,ages,inhers,posterior_er,posterior_age,...
			posterior_inher,nominal36,uncerts36,depths,MAP,jposterior,lower(RawInputs(1).scaling));
	end
	% Display the MAP solution and Bayesian credible intervals
	%erCI=bayesianCI(posterior_er,erates,0.05);
	%ageCI=bayesianCI(posterior_age,ages,0.05);
	%inherCI=bayesianCI(posterior_inher,inhers,0.05);

	% Saving output data
	samples(1).MAP_age   = MAP(2);
	samples(1).MAP_erate = MAP(1);
	samples(1).MAP_inher = MAP(3);
	if(length(lastwarn)>0); samples(1).warning = lastwarn; end
	
	cellstruct = struct2cell(samples(1));
	outputdata = [sprintf('%f,',cellstruct{:})];
	
	labeldata{end+1} = 'Profile Age (ka)';
	labeldata{end+1} = 'Erosion Rate (g/cm^2/kyr)';
	labeldata{end+1} = 'Inheritance (yrs of exposure)';
	labeldata = [sprintf('%s',labeldata{:})];
	
	% Save plots
	lprint(calcConfig.logFile,'Saving plots...\n');
	plotfiles = {	[char(calcConfig.prefix) '_cl_bestfit.png'],
					[char(calcConfig.prefix) '_cl_erate_vs_age.png'],
					[char(calcConfig.prefix) '_cl_age_vs_inher.png'],
					[char(calcConfig.prefix) '_cl_erate_vs_inher.png'],
	};
	figure(4);
	set(gcf,'PaperPositionMode','auto');   
	saveas(gcf,[OutputData.tempDir '/' plotfiles{1}]);
	figure(5);
	set(gcf,'PaperPositionMode','auto');   
	saveas(gcf,[OutputData.tempDir '/' plotfiles{2}]);
	figure(6);
	set(gcf,'PaperPositionMode','auto');   
	saveas(gcf,[OutputData.tempDir '/' plotfiles{3}]);
	figure(7);
	set(gcf,'PaperPositionMode','auto');   
	saveas(gcf,[OutputData.tempDir '/' plotfiles{4}]);
	
		
	%Clean up the path
	lprint(calcConfig.logFile,'Removing scaling scheme from matlab path...\n');
	rmpath( path );
	
	%Print total time and update running_times if not debug
	profile_time = toc(nuclide_time);
	lprint(calcConfig.logFile,'Finished in %s',sec2hms(profile_time));
	lcprint(calcConfig.userLogFile,'		Completed in %s',sec2hms(profile_time));
	if (~calcConfig.debug)
		running_times.cl.depth.average = (running_times.cl.depth.average*running_times.cl.depth.count + profile_time)/(running_times.cl.depth.count + 1);
		if (running_times.cl.depth.count < 100000) running_times.cl.depth.count = running_times.cl.depth.count + 1; end
	end
end

function [samples, outputdata, plotdata, plotfiles, labeldata, running_times] = cl_36_surface(inputs, RawInputs, sampleData, sampleErrData, sampleInputs, sampleCount, OutputData, running_times, calcConfig)
	samples = repmat(struct([]),sampleCount,1);
	outputdata = [];
	plotdata = [];
	labeldata = [];
	plotfiles = {};
	for i=1:sampleCount;
		ElementID = tic;
		lprint(calcConfig.logFile,'************************************************\n');
		lprint(calcConfig.logFile,'Cl Sample %d/%d (%s) Starting (average execution time for %s is %s)...\n',i,sampleCount,sampleInputs(i).name,RawInputs(i).scaling,sec2hms(running_times.cl.surface.(RawInputs(i).scaling).average));
		lprint(calcConfig.logFile,['Using scaling ' lower(RawInputs(i).scaling) '\n']);
		if (running_times.cl.surface.(RawInputs(i).scaling).count > 0)
			lprint(calcConfig.userLogFile,'Cl Sample %d/%d (%s) (average execution time for %s is %s)...',i,sampleCount,sampleInputs(i).name,RawInputs(i).scaling,sec2hms(running_times.cl.surface.(RawInputs(i).scaling).average));
		else
			lprint(calcConfig.userLogFile,'Cl Sample %d/%d (%s) (%s)...',i,sampleCount,sampleInputs(i).name,RawInputs(i).scaling);
		end
		lastwarn('');
		if (calcConfig.debug)
			lprint(calcConfig.logFile,'Generating debug data...\n');
			outvector = 1:100;times = 1:100;plotprodca = 1:1:100;plotprodk = 100:-1:1;plotprodcl = 1:1:100;
		else
			path = genpath(fullfile(pwd,'..',lower(RawInputs(1).scaling)));
			lprint(calcConfig.logFile,'Adding scaling scheme to matlab path...\n');
				addpath( path );
			lprint(calcConfig.logFile,'Calling createage36\n');
				createAgeInput = [sampleData{i}' sampleErrData{i}' sampleInputs(i).covariance];
				[newSampleData, newSampleErrData, newSampleCovariance] = createage36( createAgeInput );
				% Createage recalculates erosion rate unit change, but we already did that
				newSampleData(:,3) = createAgeInput(:,8);
				newSampleErrData(:,3) = createAgeInput(:,47);
			lprint(calcConfig.logFile,'Calling cl36age\n');
				[outvector,times,plotprodca,plotprodk,plotprodcl] = cl36age(newSampleData, newSampleErrData, newSampleCovariance, RawInputs(i).scaling);
			lprint(calcConfig.logFile,'Done Calling cl36 age\n');
			%Clean up the path
			lprint(calcConfig.logFile,'Removing scaling scheme from matlab path...\n');
				rmpath( path );
		end

		% 1. age (kyr)
		% 2. age uncertainty 
		% 3. Elevation/latitude scaling factor for fast neutrons for Ca(unitless)
		% 4. Elevation/latitude scaling factor for fast neutrons for K(unitless)
		% 5. Elevation/latitude scaling factor for fast neutrons for Ti(unitless)
		% 6. Elevation/latitude scaling factor for fast neutrons for Fe(unitless)
		% 7. Elevation/latitude scaling factor for epithermal neutrons (unitless)
		% 8. Elevation/latitude scaling factor for thermal neutrons (unitless)
		% 9. Avg elevation/latitude scaling factor for fast muons (unitless)
		% 10. Elevation/latitude scaling factor for slow muons (unitless)
		% 11. Contemporary depth avg prod rate, Ca, spallation (atoms/g/yr)
		% 12. Contemporary depth avg prod rate, K, spallation (atoms/g/yr)
		% 13. Contemporary depth avg prod rate, Fe, spallation (atoms/g/yr)
		% 14. Contemporary depth avg prod rate, Ti, spallation (atoms/g/yr)
		% 15. Contemporary depth avg prod rate, Ca, muons (atoms/g/yr)
		% 16. Contemporary depth avg prod rate, K, muons (atoms/g/yr)
		% 17. Contemporary depth avg prod rate, Cl, low energy (atoms/g/yr)
		% 18. Sigma_th.ss (cm^2/g)
		% 19. Sigma_eth.ss (cm^2/g)
		% 20. Sigma_sc.ss (cm^2/g)
		% 21. Qs (unitless)
		% 22. Qth (unitless)
		% 23. Qeth (unitless)
		% 24. Qmu (unitless)
		% 25. Cosmogenic 36-Cl (atoms/g of target)
		% 26. Radiogenic 36-Cl (atoms/g of target)
		% 27. Measured 36-Cl (atoms/g of target)
		% 28. Analytical (internal) uncertainty (kyr)

		lprint(calcConfig.logFile,'Generating labels and output data...\n');
		labeldata = {'Sample Name'};
		
		% Construct output structure for this sample
		[sample_age, sample_uncert] = age2str(outvector(1),outvector(2),2);
		
		samples(i).age = sample_age;	
		labeldata{end+1} = 'Age';
		
		samples(i).uncertainty = sample_uncert;
		labeldata{end+1} = 'Age Uncertainty';
		
		samples(i).erosionrate = num2str(sampleInputs(i).erosionrate);
		labeldata{end+1} = 'Erosion Rate';
		
		samples(i).elsfneutronca = num2str(outvector(3));
		labeldata{end+1} = 'Scaling Ca Spallation';
		
		samples(i).elsfneutronk = num2str(outvector(4));
		labeldata{end+1} = 'Scaling K Spallation';
		
		samples(i).elsfneutronfe = num2str(outvector(5));
		labeldata{end+1} = 'Scaling Fe Spallation';
		
		samples(i).elsfneutronti = num2str(outvector(6));
		labeldata{end+1} = 'Scaling Ti Spallation';
		
		samples(i).elsfneutroneth = num2str(outvector(7));
		labeldata{end+1} = 'Scaling Cl Epithermal';
		
		samples(i).elsfneutronth = num2str(outvector(8));
		labeldata{end+1} = 'Scaling Cl Thermal';
		
		samples(i).elsfmuonfast = num2str(outvector(9));
		labeldata{end+1} = 'Scaling Fast Muons';
		
		samples(i).elsfmuonslow = num2str(outvector(10));
		labeldata{end+1} = 'Scaling Slow Muons';   
		
		samples(i).prCas = num2str(outvector(11)); 	
		labeldata{end+1} = 'Production CA Spallation';
		
		samples(i).prKs = num2str(outvector(12));		
		labeldata{end+1} = 'Production K Spallation';
		
		samples(i).prFes = num2str(outvector(13));	
		labeldata{end+1} = 'Production Fe Spallation';
		
		samples(i).prTis = num2str(outvector(14));	
		labeldata{end+1} = 'Production Ti Spallation';
		
		samples(i).prCam = num2str(outvector(15));	
		labeldata{end+1} = 'Production CA Muons';
		
		samples(i).prKm = num2str(outvector(16));		
		labeldata{end+1} = 'Production K Muons';
		
		samples(i).prCll = num2str(outvector(17));	
		labeldata{end+1} = 'Production Cl Low Energy';
		
		samples(i).sth = num2str(outvector(18));		
		labeldata{end+1} = 'Sigma th';
		
		samples(i).seth = num2str(outvector(19));		
		labeldata{end+1} = 'Sigma eth';
		
		samples(i).ssc = num2str(outvector(20));		
		labeldata{end+1} = 'Sigma sc';
		
		samples(i).qs = num2str(outvector(21));		
		labeldata{end+1} = 'Qs';
		
		samples(i).qth = num2str(outvector(22));		
		labeldata{end+1} = 'Qth';
		
		samples(i).qeth = num2str(outvector(23));		
		labeldata{end+1} = 'Qeth';
		
		samples(i).qmu = num2str(outvector(24));		
		labeldata{end+1} = 'Qmu';
		
		%cosmogenic Cl
		samples(i).cosmoCl=num2str(outvector(25));	
		labeldata{end+1} = 'Cosmogenic Cl';
		
		samples(i).radioCl=num2str(outvector(26));	
		labeldata{end+1} = 'Radiogenic Cl';
		
		samples(i).internalUncertainty=num2str(outvector(28));	
		labeldata{end+1} = 'Analytical (Internal) Uncertainty';
		
		production_sum = outvector(11) + outvector(12) + outvector(13) + outvector(14) + outvector(15) + outvector(16) + outvector(17);

		samples(i).percent_Ca = num2str(100.0 * (outvector(11) + outvector(15))/production_sum,4); %sprintf .2f 
		labeldata{end+1} = 'Percent Ca';

		samples(i).percent_K = num2str(100.0 * (outvector(12) + outvector(16))/production_sum,4); %sprintf .2f  
		labeldata{end+1} = 'Percent K';

		samples(i).percent_Cl = num2str(100.0 * (outvector(17))/production_sum,4); %sprintf .2f  
		labeldata{end+1} = 'Percent Cl';

		if(length(lastwarn)>0); samples(i).warning = lastwarn; end

		% Generate Plots
		lprint(calcConfig.logFile,'Generating plots...\n');
		figure;
		hold all;
		plot(times,plotprodca);
		plot(times,plotprodk);
		plot(times,plotprodcl);
		xlabel('Time Before 2010 (ka)');
		ylabel('Production Rate (atoms/g sample/yr)');
		legend('Ca','K', 'Cl');
		hold off;
		plotfile = [char(calcConfig.prefix) '_' num2str( i ) '_' sampleInputs(i).name '_cl.png'];
		set(gcf,'PaperPositionMode','auto');
		lprint(calcConfig.logFile,'Saving plots...\n');   
		saveas(gcf,[OutputData.tempDir '/' plotfile]);
		plotfiles{i} = plotfile;

		% Generate Output Data
		lprint(calcConfig.logFile,'Generating Output Data...\n');   
		cellstruct = struct2cell(samples(i));
		outputdata = [outputdata '\n' sampleInputs(i).name ',' sprintf('%s,',cellstruct{:}) ];
		labeldata = [sprintf('%s,',labeldata{:}) ];
		plotdata = [plotdata '\n' sampleInputs(i).name '\n' ...
					'Time Before 2010,' sprintf('%d,',times) '\n' ...
					'ProdCA,' sprintf('%d,',plotprodca) '\n' ...
					'ProdK,' sprintf('%d,',plotprodk) '\n' ...
					'ProdCL,' sprintf('%d,',plotprodcl) '\n' ];

		element_time = toc(ElementID);
		lprint(calcConfig.logFile,['Element completed in ' sec2hms(element_time) '\n']); 
		lprint(calcConfig.logFile,'************************************************\n');
		lcprint(calcConfig.userLogFile,' %s +/- %s ka completed in %s\n',samples(i).age, samples(i).uncertainty, sec2hms(element_time));
		
		if (~calcConfig.debug)
			running_times.cl.surface.(RawInputs(i).scaling).average = (running_times.cl.surface.(RawInputs(i).scaling).average*running_times.cl.surface.(RawInputs(i).scaling).count + element_time)/(running_times.cl.surface.(RawInputs(i).scaling).count + 1);
			if (running_times.cl.surface.(RawInputs(i).scaling).count < 100000) running_times.cl.surface.(RawInputs(i).scaling).count = running_times.cl.surface.(RawInputs(i).scaling).count + 1; end
		end
	end
end



