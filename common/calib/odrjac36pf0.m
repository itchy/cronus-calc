function J=odrjac36pf0(p)
%
% Computes the Jacobian for our calibration problem.
%

%
% global variables to hold stuff that must be visible to fun and jac.
%
global INDAGES;
global SIGMAAGES;
global SAMPLES;
global UNCERTS;
global STOREDSF;           % stored precomputed scale factors.
global STOREDSP;
global N36R;
global CONC;
global SIGMACONC;
%
% Some tunable parameters.
%
npars=1;             %  We're fitting three parameters.
maxdepth=100;        %  We're only using surface samples <100g/cm^2 thick.
%
% Compute the Jacobian.
%
nsamples=length(SIGMAAGES);
v0=odrfun36pf0(p);
myI=eye(npars+nsamples);
J=zeros(nsamples*2,npars+nsamples);
for k=1:(npars+nsamples)
  h=abs(p(k))*1.0e-3;
  if (abs(h)<1.0e-3)
    h=1.0e-3;
  end;
  v=odrfun36pf0(p+h*myI(:,k));
  J(:,k)=(v-v0)/h;
end

