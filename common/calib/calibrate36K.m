% This script runs the calibration of production rates from the
% Calibration Data Set.  This version of the script fits 
%
%  PsK0
%
% nominal values are used for PsTi0 and PsFe0 and Pf(0).  
%
% Reset everything first.
%
clear
randn('state',0);
rand('state',0);
%
% load in the data.  The original dataset had some problems (0
% standard deviations.)  These have been temporarily patched up.
%
load calib36Kfinal.mat
%
% global variables to hold stuff that must be visible to fun and jac.
%
global INDAGES;
global SIGMAAGES;
global SAMPLES;
global UNCERTS;
global STOREDSF;
global STOREDSP;
global N36R;
global CONC;
global SIGMACONC;
global AGEINDEX;
%
% Store the dataset into global variables that fun and jac can access.
% Note that we convert the independent ages into the kyrs units
% that we prefer in our code.
%
UNCERTS=uncerts36;
SAMPLES=nominal36;
INDAGES=indagenew36(:,1)/1000;
SIGMAAGES=indagenew36(:,2)/1000;
AGEINDEX=ageindex36;
[nsamples,n2]=size(SAMPLES);
[nages,n2]=size(INDAGES);
%
% Get the sample concentrations and uncertainties.
%
CONC=SAMPLES(:,1);
%
% Figure out the concentration uncertainties.  We'd like to use
% cl36uncert for this, but we don't have lab intercomparison data yet.
%
%
%for k=1:nsamples
%  SIGMACONC(k)=cl36uncert(CONC(k));
%end
%
% Instead, we'll use analytical uncertainties.
%
SIGMACONC=UNCERTS(:,1);
%
% We're estimating 1 parameters.
%
npars=1;
%
% Precompute scale factors and N36R for the various samples.
%
N36R=zeros(nsamples,1);
for k=1:nsamples;
  sp=samppars36(SAMPLES(k,:));
  pp=physpars();
  sf=scalefacs36(sp);
  cp=comppars36(pp,sp,sf);
  sf.currentsf=getcurrentsf(sf,0);
  STOREDSF{k}=sf;
  STOREDSP{k}=sp;
  N36R(k)=cp.N36r;
end
%
% Setup an initial vector of parameters, psK0, delta ages.
%
pinit=[150; zeros(nages,1)];
%
% Pick a maximum number of iterations for LM.
%
maxiter=50;
%
% Call LM to do the fitting.
%
[pstar,iter]=lm('odrfun36K','odrjac36K',pinit,1.0e-6,maxiter);
%
% Check whether we got good termination or just reached maxiter.
%
if (iter >= maxiter)
  fprintf(1,'Reached maximum LM iterations of %d\n',maxiter);
end
%
% Find the covariance matrix for the fitted parameters.
%
rstar=odrfun36K(pstar);
Jstar=odrjac36K(pstar);
%
% Compute a p-value for the fit.
%
chi2=norm(rstar,2)^2
pvalue=1-chi2cdf(chi2,nsamples-npars)
%
% Compute a covariance matrix for the fit.
%
covp=inv(Jstar'*Jstar);
sigmapstar=sqrt(diag(covp));
%
% Print out the results.
%
fprintf(1,'Chi^2=%f, pvalue=%f\n',[chi2; pvalue]);
fprintf(1,'PsK0= %f +- %f \n',[pstar(1); sigmapstar(1)]);
save calib36Kfinalresults.mat

