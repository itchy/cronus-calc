%
% This script plots the computed ages for calibset10.
%
load agecalibset10.mat
%
% First, setup ranges that define which samples are from which
% sites in the 10-Be data set.
%
%  1. PPT
%  2. SCOT
%  3. HU
%  4. NZ
%
rangeppt=1:39;
%
% Note: samples 86-94 are old John Stone samples from 2002.  
%
rangescot=[40:61 86:94];
rangehu=62:78;
rangenz=79:85;
%
% Set up a mapping from sample number to site number.
%
sites=zeros(94,1);
sites(rangeppt)=1;
sites(rangescot)=2;
sites(rangehu)=3;
sites(rangenz)=4;
%
% Now, get independent ages for the sites.
%
siteages=[INDAGES(1); INDAGES(40); INDAGES(62); INDAGES(79)];
siteuncerts=[SIGMAAGES(1); SIGMAAGES(40); SIGMAAGES(62); SIGMAAGES(79)];
%
% Plot the results.
%
figure(1);
clf;
errorbar(sites-0.03*mod((1:94)',10),computedages,computeduncerts,'ko');
hold on
errorbar(1.1:4.1,siteages,siteuncerts,'ro');
axis([0 5 0 25]);
xlabel('Site (1=PPT, 2=SCOT, 3=HU, 4=NZ)');
ylabel('Age (kyr before 2010)');
print -dpng plotcalibset10ages.png
print -depsc plotcalibset10ages.eps
%
% Compute % errors for all samples and the RMSE.
%
percenterrors=zeros(94,1);
for k=1:94
  percenterrors(k)=100*(computedages(k)/INDAGES(k)-1);
end
rmse=sqrt(mean(percenterrors.^2));
fprintf(1,'RMSE=%f%% \n',rmse);
%
% Print out some results.
%
fprintf(1,['1, PPT, ind=%.2f +- %.2f, mean comp=' ...
	   '%.2f, std comp age=%.2f, err=%.2f%% \n'], ...
	[siteages(1); siteuncerts(1); mean(computedages(rangeppt)); ...
	 std(computedages(rangeppt)); ... 
	 100*(mean(computedages(rangeppt))/siteages(1)-1)]);
fprintf(1,['2, SCOT, ind=%.2f +- %.2f, mean comp=' ...
	   '%.2f, std comp age=%.2f, err=%.2f%% \n'], ...
	[siteages(2); siteuncerts(2); mean(computedages(rangescot)); ...
	 std(computedages(rangescot)); ... 
	 100*(mean(computedages(rangescot))/siteages(2)-1)]);
fprintf(1,['3, HU, ind=%.2f +- %.2f, mean comp=' ...
	   '%.2f, std comp age=%.2f, err=%.2f%% \n'], ...
	[siteages(3); siteuncerts(3); mean(computedages(rangehu)); ...
	 std(computedages(rangehu)); ... 
	 100*(mean(computedages(rangehu))/siteages(3)-1)]);
fprintf(1,['4, NZ, ind=%.2f +- %.2f, mean comp=' ...
	   '%.2f, std comp age=%.2f, err=%.2f%% \n'], ...
	[siteages(4); siteuncerts(4); mean(computedages(rangenz)); ...
	 std(computedages(rangenz)); ... 
	 100*(mean(computedages(rangenz))/siteages(4)-1)]);
