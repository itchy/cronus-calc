%
% First, load in the predicted concentrations.
%
load predcalibset3.mat
%
% Plot the ratios
%
figure(1);
clf;
%errorbar(ageindex3-0.05+0.1*rand(size(ageindex3)),ratios,uncerts,'ko');
errorbar(ageindex3,ratios,uncerts,'ko');
hold on
ylabel('N3meas/N3pred');
xlabel('site index');
plot([0 max(ageindex3)+1],[1.0 1.0],'k--');
axis([0 max(ageindex3)+1 0 2.0]);
print -depsc predcalibset3.eps
print -dpdf predcalibset3.pdf
print -dpng predcalibset3.png
