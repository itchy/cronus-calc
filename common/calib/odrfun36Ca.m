function fvec=odrfun36Ca(p)
%
% This function computes the differences between forward predicted
% ages and the independent ages, normalized by the standard
% deviations of the individual ages.
% 
%
% global variables to hold stuff that must be visible to fun and jac.
%
global INDAGES;
global SIGMAAGES;
global SAMPLES;
global UNCERTS;
global STOREDSF;           % stored precomputed scale factors.
global STOREDSP;
global N36R;
global CONC;
global SIGMACONC;
global AGEINDEX;
%
% Hard-coding scaling method since feval can't take additional inputs
scaling_model='sa';
%
% Some tunable parameters.
%
npars=1;             %  We're fitting 1 parameters.
maxdepth=100;        %  We're only using surface samples <100g/cm^2 thick.
%
% Figure out how many samples there are.
%
[nsamples,n2]=size(SAMPLES);
[nages,n2]=size(INDAGES);
%
%  If any of the parameters are negative, then the parameters are
%  meaningless.  Return a very bad fvec to tell lm to make a
%  shorter step.
%
if (min(p(1:npars))<=0)
  fvec=1.0e30*ones(nsamples+nages,1);
  return;
else
%
% Otherwise, allocate space for fvec.
%
  fvec=zeros(nsamples+nages,1);
end

%
% setup pp.
%
pp=physpars();
pp.PsCa0=p(1);
%
% Run through the samples.
%
for k=1:nsamples
  sp=STOREDSP{k};
  sf=STOREDSF{k};
  cp=comppars36(pp,sp,sf,maxdepth);
  age=INDAGES(AGEINDEX(k))+p(npars+AGEINDEX(k))*SIGMAAGES(AGEINDEX(k));
  output=predN36(pp,sp,sf,cp,age,scaling_model);
  fvec(k)=(output+N36R(k)-CONC(k))/SIGMACONC(k);
end
for k=1:nages
  fvec(k+nsamples)=p(npars+k);
end
