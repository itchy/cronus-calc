%
% First, load in the predicted concentrations.
%
load predcalibset36.mat
%
% Plot the ratios
%
figure(1);
clf;
errorbar(ageindex36,ratios,uncerts,'ko');
hold on
ylabel('N36meas/N36pred');
xlabel('site index');
plot([0 max(ageindex36)+1],[1.0 1.0],'k--');
axis([0 max(ageindex36)+1 0 2.0]);
print -depsc predcalibset36.eps
print -dpdf predcalibset36.pdf
print -dpng predcalibset36.png
