%
% This script computes ages for the Calibration samples using
% standard production rate. 
%
% Reset everything first.
%
clear
randn('state',0);
rand('state',0);
%
% load in the data.
%
load calib36secQuant.mat
%
% Get basic info about the sample ages.
%
INDAGES=indages36old(:,1)/1000;
SIGMAAGES=indages36old(:,2)/1000;
nsamples=length(INDAGES);
%
% Now, compute ages for each sample, with uncertainties.
%
computedages=zeros(nsamples,1);
for k=1:nsamples;
  output=cl36age(nominal36(k,:),uncerts36(k,:),cov36(k));
  computedages(k)=output(1);
  computeduncerts(k)=output(2);
  fprintf(1,'%4d ',k);
  fprintf(1,'%-15.15s ',samplenames{k});
  fprintf(1,' indage=%f +- %f, computed age=%f +- %f \n',full(... 
      [INDAGES(k); SIGMAAGES(k); output(1); output(2)]));
end
%
% Compute the ratios.
%
ratios=computedages./INDAGES;
%
% Compute the errors.
%
percenterrors=100*(computedages-INDAGES)./INDAGES;
RMSE=sqrt(mean(percenterrors.^2))
%
% Compute mean site ages.  
%
nsites=max(ageindex36);
meansiteages=zeros(nsites,1);
mediansiteages=zeros(nsites,1);
for k=1:nsites
  range=(ageindex36 == k);
  meansiteages(k)=mean(computedages(range));
  mediansiteages(k)=median(computedages(range));
end
meansiteerrors=100*(meansiteages*1000-indages36(:,1))./indages36(:,1);
meansiteRMSE=sqrt(mean(meansiteerrors.^2))
mediansiteerrors=100*(mediansiteages*1000-indages36(:,1))./indages36(:,1);
mediansiteRMSE=sqrt(mean(mediansiteerrors.^2))
%
% Print out a table of results.
%
fprintf(1,'\n');
for k=1:nsites
  fprintf(1,'Site- %4d ',k);
  fprintf(1,'%-8.8s ',sitenames{k});
  fprintf(1,'%5.2f +- %4.2f %4.2f%%   ',...
	  [indages36(k,1)/1000; indages36(k,2)/1000; ...
	  100*indages36(k,2)/indages36(k,1)]);
  fprintf(1,'%5.2f  %5.2f%%    ',[meansiteages(k); meansiteerrors(k)]);
  fprintf(1,'%5.2f  %5.2f%% ',[mediansiteages(k); ...
		    mediansiteerrors(k)]);
  fprintf(1','\n');
end

%
% Save the results.
%
save agecalib36secQuant.mat
quit
