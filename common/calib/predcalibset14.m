%
% This script computes ages for the Calibration samples using
% standard production rate. 
%
% Reset everything first.
%
clear
randn('state',0);
rand('state',0);
%
% load in the data.
%
load calibset14.mat
%
% Get basic info about the sample ages.
%
INDAGES=indages14(:,1)/1000;
SIGMAAGES=indages14(:,2)/1000;
CONC=nominal14(:,9);
[nsamples,ignore]=size(nominal14);
%
% Now, compute predicted N14 for each sample, and compute ratios.
%
measuredN14=nominal14(:,9);
predictedN14=zeros(size(measuredN14));
ratios=zeros(size(measuredN14));
uncerts=ratios;
for k=1:nsamples;
  if (ageindex14(k)==0)
    thisage=50;              % 50K years should saturate it.
  else
    thisage=indages14(ageindex14(k),1)/1000;
  end
  [pp,sp,sf,cp]=getpars14(nominal14(k,:));
  predictedN14(k)=predN14(pp,sp,sf,cp,thisage);
  ratios(k)=measuredN14(k)/predictedN14(k);
  temp=c14uncert(nominal14(k,9));
  if (temp > uncerts14(k,9))
    uncerts(k)=temp;
  else
    uncerts(k)=uncerts14(k,9);
  end
  uncerts(k)=uncerts(k)/predictedN14(k);
  fprintf(1,'Sample %d, N14pred=%f, N14meas=%f, ratio=%f +- %f \n',full(...
      [k; predictedN14(k); measuredN14(k); ratios(k); uncerts(k)]));
end
%
% Compute the errors.
%
percenterrors=100*(ratios-ones(size(ratios)));
RMSE=sqrt(mean(percenterrors.^2))
%
% Save the results.
%
save predcalibset14.mat
quit
