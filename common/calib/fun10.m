function fvec=fun10(p)
%
% This function computes the differences between forward predicted
% 10-Be concentrations and the measured 10-Be concentrations,  
% normalized by the standard deviations of the individual measured
% concentrations.
%
% global variables to hold stuff that must be visible to fun and jac.
%
global INDAGES;
global SIGMAAGES;
global SAMPLES;
global STOREDSF;           % stored precomputed scale factors.
global STOREDSP;
global STOREDCP;
global CONC;
global SIGMACONC;
global AGEINDEX;
%
% Some tunable parameters.
%
npars=1;             %  We're fitting one parameter.
maxdepth=100;        %  We're only using surface samples <100g/cm^2 thick.
%
% Figure out how many samples there are and how many ages
% there are.
%
[nsamples,n2]=size(SAMPLES);
[nages,n2]=size(INDAGES);
%
%  If any of the parameters are negative, then the parameters are
%  meaningless.  Return a very bad fvec to tell lm to make a
%  shorter step.
%
if (min(p(1:npars))<=0)
  warning('negative parameter.');
  fvec=1.0e30*ones(nsamples+nages,1);
  return;
else
%
% Otherwise, allocate space for fvec.
%
  fvec=zeros(nsamples,1);
end

%
% setup pp.
%
pp=physpars();
pp.PsBe=p(1);
%
% Run through the samples.
%
for k=1:nsamples
  sp=STOREDSP{k};
  sf=STOREDSF{k};
  cp=STOREDCP{k};
  age=INDAGES(AGEINDEX(k));
  [output,unused26]=predN1026(pp,sp,sf,cp,age);
  fvec(k)=(output-CONC(k))/SIGMACONC(k);
end
