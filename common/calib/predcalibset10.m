%
% This script computes ages for the Calibration samples using
% standard production rate. 
%
% Reset everything first.
%
clear
randn('state',0);
rand('state',0);
%
% load in the data.
%
load calibset10.mat
%
% Get basic info about the sample ages.
%
INDAGES=indages10(ageindex10,1)/1000;
SIGMAAGES=indages10(ageindex10,2)/1000;
nsamples=length(INDAGES);
%
% Update the uncertainties on the 10-Be concentration.
%
for k=1:nsamples
  temp=be10uncert(nominal10(k,9));
  if (temp > uncerts10(k,9))
    uncerts10(k,9)=temp;
  end
end
%
% Now, compute predicted N10 for each sample, and compute ratios.
%
measuredN10=nominal10(:,9);
predN10=zeros(size(measuredN10));
ratios=zeros(size(measuredN10));
for k=1:nsamples;
  [pp,sp,sf,cp]=getpars1026(nominal10(k,:));
  [predN10(k),unused26]=predN1026(pp,sp,sf,cp,INDAGES(k));
  ratios(k)=measuredN10(k)/predN10(k);
  uncerts(k)=uncerts10(k,9)/predN10(k);
  fprintf(1,'Sample %d, N10pred=%f, N10meas=%f, ratio=%f +- %f\n',full(...
      [k; predN10(k); measuredN10(k); ratios(k); uncerts(k)]));
end
%
% Compute the errors.
%
percenterrors=100*(ratios-ones(size(ratios)));
RMSE=sqrt(mean(percenterrors.^2))
%
% Save the results.
%
save predcalibset10.mat
quit
