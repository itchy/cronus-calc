%
% First, load in the predicted concentrations.
%
load agecalib3second.mat
%
% Plot the ratios
%
figure(1);
clf;
plot(ageindex3,ratios,'ko');
hold on
ylabel('Computed Age/Independent Age');
xlabel('site index');
plot([0 max(ageindex3)+1],[1.0 1.0],'k--');
axis([0 max(ageindex3)+1 0 2.0]);
print -depsc agecalibset3second.eps
print -dpdf agecalibset3second.pdf
print -dpng agecalibset3second.png
