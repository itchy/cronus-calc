function J=odrjac26(p)
%
% Computes the Jacobian for our calibration problem.
%

%
% global variables to hold stuff that must be visible to fun and jac.
%
global INDAGES;
global SIGMAAGES;
global SAMPLES;
global STOREDSF;           % stored precomputed scale factors.
global STOREDSP;
global N36R;
global CONC;
global SIGMACONC;
%
% Some tunable parameters.
%
npars=1;             %  We're fitting one parameters.
maxdepth=100;        %  We're only using surface samples <100g/cm^2 thick.
%
% Compute the Jacobian.
%
[nsamples,n2]=size(SAMPLES);
[nages,n2]=size(INDAGES);
v0=odrfun26(p);
myI=eye(npars+nages);
J=zeros(nsamples+nages,npars+nages);
for k=1:(npars+nages)
  h=abs(p(k))*1.0e-3;
  if (abs(h)<1.0e-3)
    h=1.0e-3;
  end;
  v=odrfun26(p+h*myI(:,k));
  J(:,k)=(v-v0)/h;
end

