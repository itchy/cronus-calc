%
% First, load in the predicted concentrations.
%
load agecalib36secQuant.mat
%
% Plot the ratios
%
figure(1);
clf;
plot(ageindex36,ratios,'ko');
hold on
ylabel('Computed Age/Independent Age');
xlabel('site index');
plot([0 max(ageindex36)+1],[1.0 1.0],'k--');
axis([0 max(ageindex36)+1 0 2.0]);
print -depsc agecalibset36secQuant.eps
print -dpdf agecalibset36secQuant.pdf
print -dpng agecalibset36secQuant.png
