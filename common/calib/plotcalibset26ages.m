%
% This script plots the computed ages for calibset10.
%
load agecalibset26.mat
%
% First, setup ranges that define which samples are from which
% sites in the 10-Be data set.
%
%  1. PPT
%  2. SCOT
%  3. HU
%  4. NZ
%
rangeppt=1:19;
rangescot=20:37;
rangehu=38:47;
%
% Set up a mapping from sample number to site number.
%
sites=zeros(47,1);
sites(rangeppt)=1;
sites(rangescot)=2;
sites(rangehu)=3;
%
% Now, get independent ages for the sites.
%
siteages=[INDAGES(1); INDAGES(20); INDAGES(38)];
siteuncerts=[SIGMAAGES(1); SIGMAAGES(20); SIGMAAGES(38)];
%
% Plot the results.
%
figure(3);
clf;
errorbar(sites-0.03*mod((1:47)',10),computedages,computeduncerts,'ko');
hold on
errorbar(1.1:3.1,siteages,siteuncerts,'ro');
axis([0 4 0 25]);
xlabel('Site (1=PPT, 2=SCOT, 3=HU)');
ylabel('Age (kyr before 2010)');
print -dpng plotcalibset26ages.png
print -depsc plotcalibset26ages.eps
%
% Compute % errors for all samples and the RMSE.
%
percenterrors=zeros(47,1);
for k=1:47
  percenterrors(k)=100*(computedages(k)/INDAGES(k)-1);
end
rmse=sqrt(mean(percenterrors.^2));
fprintf(1,'RMSE=%f%% \n',rmse);
%
% Print out some results.
%
fprintf(1,['1, PPT, ind=%.2f +- %.2f, mean comp=' ...
	   '%.2f, std comp age=%.2f, err=%.2f%% \n'], ...
	[siteages(1); siteuncerts(1); mean(computedages(rangeppt)); ...
	 std(computedages(rangeppt)); ... 
	 100*(mean(computedages(rangeppt))/siteages(1)-1)]);
fprintf(1,['2, SCOT, ind=%.2f +- %.2f, mean comp=' ...
	   '%.2f, std comp age=%.2f, err=%.2f%% \n'], ...
	[siteages(2); siteuncerts(2); mean(computedages(rangescot)); ...
	 std(computedages(rangescot)); ... 
	 100*(mean(computedages(rangescot))/siteages(2)-1)]);
fprintf(1,['3, HU, ind=%.2f +- %.2f, mean comp=' ...
	   '%.2f, std comp age=%.2f, err=%.2f%% \n'], ...
	[siteages(3); siteuncerts(3); mean(computedages(rangehu)); ...
	 std(computedages(rangehu)); ... 
	 100*(mean(computedages(rangehu))/siteages(3)-1)]);
