%
% age=computeage(pp,sp,sf,cp,pd,scaling_model)
%
function age=computeage36(pp,sp,sf,cp,scaling_model)
%
% Set the maximum possible age.  This routine can fail if you give
% it an older sample, but keeping this reasonably small helps
% performance.  
%
maxage=2000;                      % We've assumed a maximum age of
                                  % 2 Ma.
%
% The target N36 is cp.N36c, since we're trying to match the
% cosmogenic N36 to the predicted cosmogenic N36.  cp.N36c has
% already taken out any blank correction, inheritance, and 
% radiogenic 36-Cl.
%
targetN36=cp.N36c;
%
% The age will always be between left and right.  A binary search
% will be used to reduce the width of the interval.
%
left=0.0;
right=maxage;
%
% Main loop, reduce the width of the interval.  We'll settle for an 
% absolute accurate of 1.0e-6.  This is much more precise than we need
% for aging, but it is helpful in doing the calibration of production
% rates.
%
while ((right-left)/right >1.0e-8)
%
% Take the midpoint of the interval and compute the production there.
%
  midpoint=(right+left)/2;
  testN36=predN36(pp,sp,sf,cp,midpoint,scaling_model);
  if (testN36 < targetN36)
    %
    % it's older then midpoint.
    %
    left=midpoint;
  else
    %
    % it's younger then midpoint.
    %
    right=midpoint;
  end
end
age=midpoint;
