%
%  [output,times,plotprodca,plotprodk,plotprodcl,derivs]...
%    =cl36age(sampledata,sampleuncertainties,covar,scaling_model)
%
%  Given the data for a sample and associated one standard
%  deviation uncertainties, computes the age of the sample and uncertainty
%  using the  Lal/Stone 2000 scaling scheme.
%
%  The sampledata vector contains the following information.
%
%1.     Sample 36-Cl concentration (atoms of 36-Cl/g of target)
%2.     Inheritance (atoms 36-Cl/g of target)  
%3.     erosion-rate epsilon (g/(cm^2*kyr))
%4.     fractional volumetric water-content (unitless) 
%5.     bulk density (g/cm^3)
%6.     sample thickness (cm)
%7.     Latitude (decimal degrees, -90(S) to +90(N))
%8.     Longitude (decimal degrees, 0-360 degrees east)
%9.     Elevation (meters)
%10.    Pressure (hPa)                Both 9 and 10 must be present!
%11.    Shielding factor for terrain, snow, etc. (unitless)
%12.    Effective attenuation length -Lambdafe (g/cm^2)
%13.    % CO2                        Rock
%14.    % Na2O                       Rock
%15.    % MgO                        Rock
%16.    % Al2O3                      Rock
%17.    % SiO2                       Rock
%18.    % P2O5                       Rock
%19.    % K2O                        Rock
%20.    % CaO                        Rock
%21.    % TiO2                       Rock
%22.    % MnO                        Rock
%23.    % Fe2O3                      Rock
%24.    Cl (ppm)                     Rock
%25.    B (ppm)                      Rock
%26.    Sm (ppm)                     Rock
%27.    Gd (ppm)                     Rock
%28.    U (ppm)                      Rock
%29.    Th (ppm)                     Rock
%30.    Cr (ppm)                     Rock
%31.    Li (ppm)                     Rock
%32.	Target element %K2O          Target
%33.    Target element %CaO          Target
%34.    Target element %TiO2         Target
%35.    Target element %Fe2O3        Target
%36.    Target element Cl (ppm)      Target
%37.    Depth to top of sample (g/cm^2)
%38.    Year sampled (e.g. 2010)
%
% A second input vector, sampleuncertainties, contains 1-sigma
% uncertainties for all 36 inputs.  In general, we assume that
% these 36 inputs are uncorrelated.  The one exception is that 
% we allow for correlation between input parameter 1 (36-Cl
% concentration in the target rock in atoms/g) and parameter 36
% (Concentration of Cl in the target rock in ppm.)  The covariance
% between these paramters is given as the third input parameter, covar.
%
% scaling_model is one of 'DE','DU','LI','LM','SA','SF','ST' and 
% informs which scaling model is being used
%
% Returns an output vector:
%
% 1. Age (kyr)
% 2. Age uncertainty (kyr) 
% 3. Elevation/latitude scaling factor for fast neutrons for Ca(unitless)
% 4. Elevation/latitude scaling factor for fast neutrons for K(unitless)
% 5. Elevation/latitude scaling factor for fast neutrons for Ti(unitless)
% 6. Elevation/latitude scaling factor for fast neutrons for Fe(unitless)
% 7. Elevation/latitude scaling factor for epithermal neutrons (unitless)
% 8. Elevation/latitude scaling factor for thermal neutrons (unitless)
% 9. Avg elevation/latitude scaling factor for fast muons (unitless)
% 10. Elevation/latitude scaling factor for slow muons (unitless)
% 11. Contemporary depth avg prod rate, Ca, spallation (atoms/g/yr)
% 12. Contemporary depth avg prod rate, K, spallation (atoms/g/yr)
% 13. Contemporary depth avg prod rate, Fe, spallation (atoms/g/yr)
% 14. Contemporary depth avg prod rate, Ti, spallation (atoms/g/yr)
% 15. Contemporary depth avg prod rate, Ca, muons (atoms/g/yr)
% 16. Contemporary depth avg prod rate, K, muons (atoms/g/yr)
% 17. Contemporary depth avg prod rate, Cl, low energy (atoms/g/yr)
% 18. Sigma_th.ss (cm^2/g)
% 19. Sigma_eth.ss (cm^2/g)
% 20. Sigma_sc.ss (cm^2/g)
% 21. Qs (unitless)
% 22. Qth (unitless)
% 23. Qeth (unitless)
% 24. Qmu (unitless)
% 25. Cosmogenic 36-Cl (atoms/g of target)
% 26. Radiogenic 36-Cl (atoms/g of target)
% 27. Measured 36-Cl (atoms/g of target)
% 28. Analytical (internal) uncertainty (kyr)
%
% An optional output that can be useful for debugging is the vector
% derivs, which contains the derivatives of the age with respect to
% the 36 input parameters.  Note that the derivatives are only
% computed if the corresponding uncertainty is nonzero.  
%
%
function [output,times,plotprodca,plotprodk,plotprodcl,derivs]...
    =cl36age(sampledata,sampleuncertainties,covar,scaling_model)
  expectedOutputs = 28;
  %
  % Make sampledata and uncertainties column vectors if they aren't already.
  %
  if (size(sampledata,1)==1)
    sampledata=sampledata';
  end
  if (size(sampleuncertainties,1)==1)
    sampleuncertainties=sampleuncertainties';
  end
  
  %
  % First, check that the input data is reasonable.
  %
  if (length(sampledata) ~= 38)
    error('sampledata has wrong size!');
  end
  if (length(sampleuncertainties) ~= 38)
    error('sampleuncertainties has wrong size!');
  end
  %if (~exist('scaling_model','var')), scaling_model = 'sa'; end
  %
  % Give a warning about the composition not adding up to 100%.
  %
  if (abs(sum(sampledata(13:23))-100) > 2.0)
    warning('major element composition does not add up to one hundred percent!');
  end
  %
  % Setup the physical parameters.
  %
  pp=physpars();
  %
  % Extract the sample parameters from the sampledatavector.
  %
  sp=samppars36(sampledata);
  %
  % Get the scale factors.
  %
  sf=scalefacs36(sp,scaling_model);
  %
  % We need an absolute maximum age for several purposes, including
  % detecting saturated samples and setting the maximum depth for comppars.
  %
  maxage=2000;               % 2Ma > 6 half lives              
  %
  % Figure out the maximum possible depth at which we'll ever need a
  % production rate.  This is depthtotop + maxage * erosion +
  % thickness * density + a safety factor.
  %
  maxdepth=sp.depthtotop+maxage*sp.epsilon+sp.ls*sp.rb+1000; 
  %
  % Computed parameters.
  %
  cp=comppars36(pp,sp,sf,maxdepth);
  %
  % Get contemporary surface production rates in atoms/g 
  %
  sf.currentsf=getcurrentsf(sf,0,scaling_model,'cl');
  [Prodtotal,Prods,ProdsCa,ProdsK,ProdsTi,ProdsFe,Prodth,Prodeth]=prodz36(0,pp,sf,cp);
  %
  % Before we go any further, check to make sure that this sample
  % isn't saturated.  If the measured concentration is greater than
  % 90% of the saturated concentration then return NaN's.
  %
  satN36=predN36(pp,sp,sf,cp,maxage,scaling_model);
  if (sp.concentration36 > 0.95*satN36)
    fprintf(1,'Saturated N36=%f, Measured N36=%f \n',[satN36; sp.concentration36]);
    warning('This sample is saturated!');
    output=NaN*ones(expectedOutputs,1);
    [ times, plotprodca, plotprodk, plotprodcl ] = getPlotData(maxage, sf, Prods, ...
      ProdsCa, ProdsK, ProdsTi, ProdsFe, Prodth, Prodeth, 'cl', scaling_model);
    return;
  end
  %
  % Compute the nominal results.
  %
  output=cl36ageraw(sampledata,pp,sf,scaling_model);
  age=output(1);
  
  [ times, plotprodca, plotprodk, plotprodcl ] = getPlotData(maxage, sf, Prods, ...
      ProdsCa, ProdsK, ProdsTi, ProdsFe, Prodth, Prodeth, 'cl', scaling_model);
  
  % Start off with a sum of 0.
  %
  uncertainty=0.0;
  %
  % Work through all 36 sample parameters.  For each nonzero
  % uncertainty, add in the uncertainty term.
  %
  derivs=zeros(38,1);
  for i=1:38
    if ((sampleuncertainties(i) ~= 0.0) | (nargout > 5))
      if (sampledata(i) ~= 0.0)
        thisdelta=0.01*abs(sampledata(i));
      else
        thisdelta=0.01;
      end
      deltasampledata=sampledata;
      deltasampledata(i)=deltasampledata(i)+thisdelta;
      deltasp=samppars36(deltasampledata);
      deltasf=scalefacs36(deltasp,scaling_model);
      deltaoutput=cl36ageraw(deltasampledata,pp,deltasf,scaling_model);
      deltaage=deltaoutput(1);
      dagedi=(deltaage-age)/(thisdelta);
      derivs(i)=dagedi;
      if (~isnan(sampleuncertainties(i)))
        uncertainty=uncertainty+(dagedi^2*sampleuncertainties(i)^2);
      end
    end
  end
  %
  % Add in a term for the covariance of parameters 1 and 36.
  %
  uncertainty=uncertainty+2*covar*derivs(1)*derivs(36);
  %
  % Add in terms for the uncertainty in production rates.  
  %
  % First, for Pf0.
  %
  deltapp=pp;
  deltapp.Pf0=pp.Pf0+0.01*abs(pp.Pf0);
  deltaoutput=cl36ageraw(sampledata,deltapp,sf,scaling_model);
  deltaage=deltaoutput(1);
  dagedpf0=(deltaage-age)/(0.01*abs(pp.Pf0));
  uncertaintyext=uncertainty+(dagedpf0^2*pp.sigmapf0^2);
  %
  % Next, for PsCa0.
  %
  deltapp=pp;
  deltapp.PsCa0=pp.PsCa0+0.01*abs(pp.PsCa0);
  deltaoutput=cl36ageraw(sampledata,deltapp,sf,scaling_model);
  deltaage=deltaoutput(1);
  dagedpsCa0=(deltaage-age)/(0.01*abs(pp.PsCa0));
  uncertaintyext=uncertaintyext+(dagedpsCa0^2*pp.sigmaPsCa0^2);
  %
  % Finally, for PsK0.
  %
  deltapp=pp;
  deltapp.PsK0=pp.PsK0+0.01*abs(pp.PsK0);
  deltaoutput=cl36ageraw(sampledata,deltapp,sf,scaling_model);
  deltaage=deltaoutput(1);
  dagedpsK0=(deltaage-age)/(0.01*abs(pp.PsK0));
  uncertaintyext=uncertaintyext+(dagedpsK0^2*pp.sigmaPsK0^2);
  %
  % Finally, take the square root of uncertainty to get a standard deviation.
  %
  %This is the internal uncertainty (analytical only)
  uncertainty=sqrt(uncertainty);
  %This is external/total uncertainty
  uncertaintyext=sqrt(uncertaintyext);
  %
  % Put the uncertainty in the output.
  %
  output(2)=uncertaintyext;
  output(28)=uncertainty;

  if size(output,1) ~= expectedOutputs
    error('Cl outputs not expected size');
  end
end

% Overriding the function in getPlotData.m
function [times, plotprodca, plotprodk, plotprod] = ...
  getPlotData(age, sf, prodRate, prodRateCa, prodRateK, prodRateTi, prodRateFe, prodRateTh, prodRateEth, nuclide, scaling_model)
  
  %calculating the production rates/scaling factors through time
  %create a time vector
  numberpoints=300;
  times=linspace(0,-age,numberpoints); %find 300 points for the plot
  sf.currentsf=getcurrentsf(sf,0,scaling_model,nuclide);

  plotscalefactors=getcurrentsf(sf,times(1),scaling_model,nuclide);
  sf.currentsf=getcurrentsf(sf,0,scaling_model,nuclide);
  caprod=prodRateCa/plotscalefactors.Sel36Ca(1);
  kprod=prodRateK/plotscalefactors.Sel36K(1);
  %tiprod=prodRateTi/plotscalefactors.Sel36Ti(1);
  %feprod=prodRateFe/plotscalefactors.Sel36Fe(1);
  clprod=(prodRateTh/plotscalefactors.SFth(1))+(prodRateEth/plotscalefactors.SFeth(1));

  for k=2:numberpoints;
      plotscalefactors=getcurrentsf(sf,times(k),scaling_model,nuclide);
      plotprodca(k)=caprod*plotscalefactors.Sel36Ca;
      plotprodk(k)=kprod*plotscalefactors.Sel36K;
      plotprod(k)=clprod*plotscalefactors.SFth;
  end
end