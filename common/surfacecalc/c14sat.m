%
%  satN14=c14sat(sampledata)
%
%  Given the data for a sample, computes the corresponding
%  saturated 14-C concentration.
%
% The sampledata vector contains the following information:
%
%1. Latitude (decimal degrees, -90(S) to +90(N))
%2. Longitude (decimal degrees, 0-360 degrees east)
%3. Elevation (meters)
%4. Pressure (hPa)      
%5. sample thickness (cm)
%6. bulk density (g/cm^3)
%7. Shielding factor for terrain, snow, etc. (unitless)
%8. Erosion-rate epsilon (g/(cm^2*kyr))
%9. Sample 14-C concentration (atoms of 14-C/g of target)
%10. Inheritance for 14-C (atoms 14-C/g of target)
%11. Effective attenuation length -Lambdafe (g/cm^2)
%12. Depth to top of sample (g/cm^2)
%13. Year sampled (e.g. 2010)
%
% Returns an output vector:
%
% 1. Saturate 14-C concentration atoms/g
%
function satN14=c14sat(sampledata)
%
% Make sampledata a column vector if it isn't already.
%
if (size(sampledata,1)==1)
  sampledata=sampledata';
end
%
% First, check that the input data is reasonable.
%
if (length(sampledata) ~= 13)
  error('sampledata has wrong size!');
end
%
% Setup the physical parameters.
%
pp=physpars();
%
% Extract the sample parameters from the sampledatavector.
%
sp=samppars14(sampledata);
%
% Get the scale factors.
%
sf=scalefacs14(sp);
%
% We need an absolute maximum age for several purposes, including
% detecting saturated samples and setting the maximum depth for comppars.
%
maxage=50;                             % It'll be saturated after 50kyr
%
% Figure out the maximum possible depth at which we'll ever need a
% production rate.  This is depthtotop + maxage * erosion (g/cm2/kyr) +
% thickness * density + a safety factor.
%
maxdepth=sp.depthtotop+maxage*sp.epsilon+sp.ls*sp.rb+1000; 
%
% Computed parameters.
%
cp=comppars14(pp,sp,sf,maxdepth);
%
% Before we go any further, check to make sure that this sample
% isn't saturated.  If the measured concentration is greater than
% 90% of the saturated concentration then return NaN's.
%
satN14=predN14(pp,sp,sf,cp,maxage);
