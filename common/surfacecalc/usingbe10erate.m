
function [ total, erates, uncerts, eratesmm, uncertsmm ] = ...
  usingbe10erate( scaling_model, inputfilename, outputfilename )
  
load( inputfilename, '-mat' );

%
% Get basic info about the sample ages.
%
nsamples=size(nominal10,1);
%
% Update the uncertainties on the concentration.
%
%
erates=zeros(nsamples,1);
uncerts=zeros(nsamples,1);
eratesmm=zeros(nsamples,1);
uncertsmm=zeros(nsamples,1);
for k=1:nsamples;
  tic;
  [erates(k),uncerts(k),eratesmm(k),uncertsmm(k)] = ...
    be10erate(nominal10(k,:),uncerts10(k,:),scaling_model);
 
  fprintf(1,'Be Sample %d, computed erate=%f +- %f in %f minutes\n',full(...
      [k; eratesmm(k); uncertsmm(k); toc/60]));
end
%

total(:,1)=erates;
total(:,2)=uncerts;
total(:,3)=eratesmm;
total(:,4)=uncertsmm;

if nargin > 2
  % Save the results.
  %
  save( outputfilename );
end