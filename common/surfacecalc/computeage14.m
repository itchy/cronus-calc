%
% age=computeage14(pp,sp,sf,cp,scaling_model)
%
% Uses 14-C concentration to estimate the age of a sample.  
%
function age=computeage14(pp,sp,sf,cp,scaling_model)
%
% Set the maximum possible age.  This routine can fail if you give
% it an older sample, but keeping this reasonably small helps
% performance.  
%
maxage=50;                      % We've assumed a maximum age of 50Ka.
%
% The target N14 is sp.N14.  For 14-C, there's no radiogenic
% concentration, and we assume that any blank has already been
% taken out.
%
targetN14=sp.concentration14-sp.inheritance14;
%
% The age will always be between left and right.  A binary search
% will be used to reduce the width of the interval.
%
left=0.0;
right=maxage;
%
% Main loop, reduce the width of the interval.  We'll settle for an 
% absolute accurate of 1.0e-6.  This is much more precise than we need
% for aging, but it is helpful in doing the calibration of production
% rates.
%
while ((right-left)/right >1.0e-8)
%
% Take the midpoint of the interval and compute the production there.
%
  midpoint=(right+left)/2;
  testN14=predN14(pp,sp,sf,cp,midpoint,scaling_model);
  if (testN14 < targetN14)
    %
    % it's older then midpoint.
    %
    left=midpoint;
  else
    %
    % it's younger then midpoint.
    %
    right=midpoint;
  end
end
age=midpoint;
