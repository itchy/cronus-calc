%
%  [output,times,plotprodc,derivs]=c14age(sampledata,sampleuncertainties,scaling_model)
%
%  Given the data for a sample and associated one standard
%  deviation uncertainties, computes the age of the sample and uncertainty.
%
% The sampledata vector contains the following information:
%
%1. Latitude (decimal degrees, -90(S) to +90(N))
%2. Longitude (decimal degrees, 0-360 degrees east)
%3. Elevation (meters)
%4. Pressure (hPa)      
%5. sample thickness (cm)
%6. bulk density (g/cm^3)
%7. Shielding factor for terrain, snow, etc. (unitless)
%8. Erosion-rate epsilon (g/(cm^2*kyr))
%9. Sample 14-C concentration (atoms of 14-C/g of target)
%10. Inheritance for 14-C (atoms 14-C/g of target)
%11. Effective attenuation length -Lambdafe (g/cm^2)
%12. Depth to top of sample (g/cm^2)
%13. Year sampled (e.g. 2010)
%
% A second input vector, sampleuncertainties, contains 1-sigma
% uncertainties for all 13 inputs.  In general, we assume that
% these 13 inputs are uncorrelated. 
%
% scaling_model is one of 'DE','DU','LI','LM','SA','SF','ST' and 
% informs which scaling model is being used
%
% Returns an output vector:
%
% 1. Age (kyr)
% 2. Total (external) age uncertainty (kyr) 
% 3. Contemporary Elevation/latitude scaling factor for neutrons for C (unitless)
% 4. Contemporary Elevation/latitude scaling factor for fast muons (unitless)
% 5. Contemporary Elevation/lat scaling factor for slow muons (unitless)
% 6. Contemporary depth avg prod rate, neutron spallation (atoms/g/yr)
% 7. Contemporary depth avg prod rate, muons (atoms/g/yr)
% 8. Qs (unitless)
% 9. Qmu (unitless)
% 10. Inherited 14-C (atoms/g of target)
% 11. Measured 14-C (atoms/g of target)
% 12. Analytical only (internal) uncertainty (kyr)
%
% An optional output that can be useful for debugging is the vector
% derivs, which contains the derivatives of the age with respect to
% the 13 input parameters.  Note that the derivatives are only
% computed if the corresponding uncertainty is nonzero.  
%
function [output,times,plotprodc,derivs]=c14age(sampledata,sampleuncertainties,scaling_model)
  expectedOutputs = 12;
  %
  % Make sampledata and uncertainties column vectors if they aren't already.
  %
  if (size(sampledata,1)==1)
    sampledata=sampledata';
  end
  if (size(sampleuncertainties,1)==1)
    sampleuncertainties=sampleuncertainties';
  end
  
  %
  % First, check that the input data is reasonable.
  %
  if (length(sampledata) ~= 13)
    error('sampledata has wrong size!');
  end
  if (length(sampleuncertainties) ~= 13)
    error('sampleuncertainties has wrong size!');
  end
  %
  % Setup the physical parameters.
  %
  pp=physpars();
  %
  % Extract the sample parameters from the sampledatavector.
  %
  sp=samppars14(sampledata);
  %
  % Get the scale factors.
  %
  sf=scalefacs14(sp,scaling_model);
  %
  % We need an absolute maximum age for several purposes, including
  % detecting saturated samples and setting the maximum depth for comppars.
  %
  maxage=50;            % Saturated after 50ka (>8 half lives)
  %
  % Figure out the maximum possible depth at which we'll ever need a
  % production rate.  This is depthtotop + maxage * erosion (g/cm2/kyr) +
  % thickness * density + a safety factor.
  %
  maxdepth=sp.depthtotop+maxage*sp.epsilon+sp.ls*sp.rb+1000; 
  %
  % Computed parameters.
  %
  cp=comppars14(pp,sp,sf,maxdepth);
  %
  % Get contemporary surface production rates in atoms/g 
  %
  sf.currentsf=getcurrentsf(sf,0,scaling_model,'c');
  [ProdtotalC,ProdsC,ProdmuC]=prodz14(0,pp,sf,cp);
  %
  % Before we go any further, check to make sure that this sample
  % isn't saturated.  If the measured concentration is greater than
  % 90% of the saturated concentration then return NaN's.
  %
  satN14=predN14(pp,sp,sf,cp,maxage,scaling_model);
  if (sp.concentration14 > 0.95*satN14)
    fprintf(1,'Saturated N14=%f, Measured N14=%f \n',[satN14; sp.concentration14]);
    warning('This sample is saturated!');
    output=NaN*ones(expectedOutputs,1);
    [ times, plotprodc ] = getPlotData(maxage, sf, ProdsC, 'c', scaling_model);
    return;
  end
  %
  % Compute the nominal results.
  %
  output=c14ageraw(sampledata,pp,sf,scaling_model);
  age=output(1);
  
  [ times, plotprodc ] = getPlotData(age, sf, ProdsC, 'c', scaling_model);
  
  %
  % Start off with a sum of 0.
  %
  uncertainty=0.0;
  %
  % Work through all 13 sample parameters.  For each nonzero
  % uncertainty, add in the uncertainty term.
  %
  derivs=zeros(13,1);
  for i=1:13
    if ((sampleuncertainties(i) ~= 0.0) | (nargout > 3))
      if (sampledata(i) ~= 0.0)
        thisdelta=0.01*abs(sampledata(i));
      else
        thisdelta=0.01;
      end
      deltasampledata=sampledata;
      deltasampledata(i)=deltasampledata(i)+thisdelta;
      deltasp=samppars14(deltasampledata);
      deltasf=scalefacs14(deltasp,scaling_model);
      deltaoutput=c14ageraw(deltasampledata,pp,deltasf,scaling_model);
      deltaage=deltaoutput(1);
      dagedi=(deltaage-age)/(thisdelta);
      derivs(i)=dagedi;
      if (~isnan(sampleuncertainties(i)))
        uncertainty=uncertainty+(dagedi^2*sampleuncertainties(i)^2);
      end
    end
  end
  %
  % Add in terms for the uncertainty in production rates.  
  %
  %
  % Uncertainty in PsC.
  %
  deltapp=pp;
  deltapp.PsC=pp.PsC+0.01*abs(pp.PsC);
  deltaoutput=c14ageraw(sampledata,deltapp,sf,scaling_model);
  deltaage=deltaoutput(1);
  dagedpsC=(deltaage-age)/(0.01*abs(pp.PsC));
  uncertaintyext=uncertainty+(dagedpsC^2*pp.sigmaPsC^2);
  %
  % Finally, take the square root of uncertainty to get a standard deviation.
  %
  %This is the internal uncertainty (analytical only)
  uncertainty=sqrt(uncertainty);
  %This is external/total uncertainty
  uncertaintyext=sqrt(uncertaintyext);
  %
  % Put the uncertainty in the output.
  %
  output(2)=uncertaintyext;
  output(12)=uncertainty;
  
  if size(output,1) ~= expectedOutputs
    error('C outputs not expected size');
  end
end