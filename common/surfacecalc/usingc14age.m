
function [ total, computedages, computeduncerts, scalefactorsn, scalefactorsmu, percentmuons, computeduncertsint ] = ...
  usingc14age( scaling_model, inputfilename, outputfilename )
  
load( inputfilename, '-mat' );

%
% Get basic info about the sample ages.
%
%INDAGES=indages14(:,1)/1000;
%SIGMAAGES=indages14(:,2)/1000;
nsamples=size(nominal14,1);
%
% Update the uncertainties on the concentration.
%
% for k=1:nsamples
%   temp=c14uncert(nominal14(k,9));
%   if (temp > uncerts14(k,9))
%     uncerts14(k,9)=temp;
%   end
% end
%
% Now, compute ages for each sample, with uncertainties.
%
computedages=zeros(nsamples,1);
for k=1:nsamples;
  tic;
  output=c14age(nominal14(k,:),uncerts14(k,:),scaling_model);
  
  computedages(k)=output(1);
  computeduncerts(k)=output(2);
  scalefactorsn(k)=output(3);
  scalefactorsmu(k)=output(4);
  computeduncertsint(k)=output(12);
  percentmuons(k)=output(7)/(output(6)+output(7))*100;
  
  fprintf(1,'C Sample %d, computed age=%f +- %f in %f minutes\n',full(...
      [k; output(1); output(2); toc/60]));
end
%
% Compute the errors.
%
%percenterrors=100*(computedages-INDAGES)./INDAGES;
%RMSE=sqrt(mean(percenterrors.^2))
%
%To output everything in an "easy to paste into excel" format:
  total(:,1)=computedages';
  total(:,2)=computeduncerts';
  total(:,3)=scalefactorsn';
  total(:,4)=scalefactorsmu';
  total(:,5)=percentmuons';
  total(:,6)=computeduncertsint';

if nargin > 2
  % Save the results.
  %
  save( outputfilename );
end
