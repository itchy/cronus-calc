%
%  [output,times,plotprodne,derivs]=ne21age(sampledata,sampleuncertainties,scaling_model)
%
%  Given the data for a sample and associated one standard
%  deviation uncertainties, computes the age of the sample and uncertainty.
%
% The sampledata vector contains the following information:
%
%1. Latitude (decimal degrees, -90(S) to +90(N))
%2. Longitude (decimal degrees, 0-360 degrees east)
%3. Elevation (meters)
%4. Pressure (hPa)      
%5. sample thickness (cm)
%6. bulk density (g/cm^3)
%7. Shielding factor for terrain, snow, etc. (unitless)
%8. Erosion-rate epsilon (g/(cm^2*kyr))
%9. Sample 21-ne concentration (atoms of 21-ne/g of target)
%10. Inheritance for 21-ne (atoms 21-ne/g of target)
%11. Effective attenuation length -Lambdafe (g/cm^2)
%12. Depth to top of sample (g/cm^2)
%13. Year sampled (e.g. 2010)
%
% A second input vector, sampleuncertainties, contains 1-sigma
% uncertainties for all 13 inputs.  In general, we assume that
% these 13 inputs are uncorrelated. 
%
% scaling_model is one of 'DE','DU','LI','LM','SA','SF','ST' and 
% informs which scaling model is being used
%
% Returns an output vector:
%
% 1. Age (kyr)
% 2. Total age uncertainty (kyr)
% 3. Contemporary Elevation/latitude scaling factor for neutrons for ne (unitless)
% 4. Contemporary depth avg prod rate, neutron spallation (atoms/g/yr)
% 5. Qs (unitless)
% 6. Inherited 21-ne (atoms/g of target)
% 7. Measured 21-ne (atoms/g of target)
% 8. Analytical (internal) uncertainty (kyr)
%
% An optional output that can be useful for debugging is the vector
% derivs, which contains the derivatives of the age with respect to
% the 13 input parameters.  Note that the derivatives are only
% computed if the corresponding uncertainty is nonzero.  
%
function [output,times,plotprodne,derivs]=ne21age(sampledata,sampleuncertainties,scaling_model)
  expectedOutputs = 8;
  %
  % Make sampledata and uncertainties column vectors if they aren't already.
  %
  if (size(sampledata,1)==1)
    sampledata=sampledata';
  end
  if (size(sampleuncertainties,1)==1)
    sampleuncertainties=sampleuncertainties';
  end
  
  %
  % First, check that the input data is reasonable.
  %
  if (length(sampledata) ~= 13)
    error('sampledata has wrong size!');
  end
  if (length(sampleuncertainties) ~= 13)
    error('sampleuncertainties has wrong size!');
  end
  %if (~exist('scaling_model','var')), scaling_model = 'all'; end
  %
  % Setup the physical parameters.
  %
  pp=physpars();
  %
  % Extract the sample parameters from the sampledatavector.
  %
  sp=samppars21(sampledata);
  %
  % Get the scale factors.
  %
  sf=scalefacs21(sp,scaling_model);
  %
  % We need an absolute maximum age for several purposes, including
  % detecting saturated samples and setting the maximum depth for comppars.
  %
  maxage=2500;                             
  %
  % Figure out the maximum possible depth at which we'll ever need a
  % production rate.  This is depthtotop + maxage * erosion +
  % thickness * density + a safety factor.
  %
  % maxdepth=sp.depthtotop+maxage*sp.epsilon+sp.ls*sp.rb+1000; 
  % There is no maxdepth based on maxage due to the fact that ne can't become
  % saturated
  maxdepth=sp.depthtotop+maxage*sp.epsilon+sp.ls*sp.rb+1000;
  
  %
  % Computed parameters.
  %
  cp=comppars21(pp,sp,sf,maxdepth);
  %
  % Get contemporary surface production rates in atoms/g 
  %
  sf.currentsf=getcurrentsf(sf,0,scaling_model,'ne');
  [ProdtotalNe,ProdsNe]=prodz21(0,pp,sf,cp);
  %
  % Compute the raw age.
  %
  output=ne21ageraw(sampledata,pp,sf,scaling_model);
  age=output(1);
  
  [ times, plotprodne ] = getPlotData(age, sf, ProdsNe, 'ne', scaling_model);
  
  %
  % Start off with a sum of 0.
  %
  uncertainty=0.0;
  %
  % Work through all 13 sample parameters.  For each nonzero
  % uncertainty, add in the uncertainty term.
  %
  derivs=zeros(13,1);
  for i=1:13
    if ((sampleuncertainties(i) ~= 0.0) | (nargout > 3))
      if (sampledata(i) ~= 0.0)
        thisdelta=0.01*abs(sampledata(i));
      else
        thisdelta=0.01;
      end
      deltasampledata=sampledata;
      deltasampledata(i)=deltasampledata(i)+thisdelta;
      deltasp=samppars21(deltasampledata);
      deltasf=scalefacs21(deltasp,scaling_model);
      deltaoutput=ne21ageraw(deltasampledata,pp,deltasf,scaling_model);
      deltaage=deltaoutput(1);
      dagedi=(deltaage-age)/(thisdelta);
      derivs(i)=dagedi;
      if (~isnan(sampleuncertainties(i)))
        uncertainty=uncertainty+(dagedi^2*sampleuncertainties(i)^2);
      end
    end
  end
  %
  % Add in terms for the uncertainty in production rates.  
  %
  %
  % Uncertainty in PsNe.
  %
  deltapp=pp;
  deltapp.PsNe=pp.PsNe+0.01*abs(pp.PsNe);
  deltaoutput=ne21ageraw(sampledata,deltapp,sf,scaling_model);
  deltaage=deltaoutput(1);
  dagedpsne=(deltaage-age)/(0.01*abs(pp.PsNe));
  uncertaintyext=uncertainty+(dagedpsne^2*pp.sigmaPsNe^2);
  %
  % Finally, take the square root of uncertainty to get a standard deviation.
  %
  %This is the internal uncertainty (analytical only)
  uncertainty=sqrt(uncertainty);
  %This is external/total uncertainty
  uncertaintyext=sqrt(uncertaintyext);
  %
  % Put the uncertainty in the output.
  %
  output(2)=uncertaintyext;
  output(8)=uncertainty;
  
  if size(output,1) ~= expectedOutputs
    error('Ne outputs not expected size');
  end
end