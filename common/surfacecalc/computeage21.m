%
% age=computeage21(pp,sp,sf,cp,scaling_model)
%
% Uses 21-ne concentration to estimate the age of a sample.  
%
function age=computeage21(pp,sp,sf,cp,scaling_model)
%
% Set the maximum possible age.  This routine can fail if you give
% it an older sample, but keeping this reasonably small helps
% performance.  
%
maxage=2500;                      % We've assumed a maximum age of 2500Ka.
%
% The target N21 is sp.N21.  For 21-ne, there's no radiogenic
% concentration, and we assume that any blank has already been
% taken out.  We take out any inheritance here.
%
targetN21=sp.concentration21-sp.inheritance21;
%
% The age will always be between left and right.  A binary search
% will be used to reduce the width of the interval.
%
left=0.0;
right=maxage;
%
% Main loop, reduce the width of the interval.  We'll settle for an 
% absolute accurate of 1.0e-6.  This is much more precise than we need
% for aging, but it is helpful in doing the calibration of production
% rates.
%
while ((right-left)/right >1.0e-8)
%
% Take the midpoint of the interval and compute the production there.
%
  midpoint=(right+left)/2;
  testN21=predN21(pp,sp,sf,cp,midpoint,scaling_model);
  if (testN21 < targetN21)
    %
    % it's older then midpoint.
    %
    left=midpoint;
  else
    %
    % it's younger then midpoint.
    %
    right=midpoint;
  end
end
age=midpoint;
