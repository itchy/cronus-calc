%
%  age=cl36ageraw(sampledata,pp,sf,scaling_model)
%
%  
%  Given the data for a sample computes the age of the sample using
%  the Lal/Stone 2000 scaling scheme.  This inner routine does not
%  handle the uncertainty calculations, which are done by
%  cl36age.m.  Instead, this inner routine simply does the basic
%  computation of the age handling a variety of possible input types.
%
%  The user can provide atmospheric pressure or elevation.  If
%  the elevation is to be used, then elevation should be given in
%  meters in parameter 9 and pressure in parameter 10 should be
%  NaN.  If pressure is to be used then the pressure should be
%  given in parameter 10 and parameter 9 should be Nan.
%
%  The sampledata vector contains the following information.
%
%1.     Sample 36-Cl concentration (atoms of 36-Cl/g of target)
%2.     Inheritance (atoms 36-Cl/g of target)  
%3.     erosion-rate epsilon (g/(cm^2*kyr))
%4.     fractional volumetric water-content (unitless) 
%5.     bulk density (g/cm^3)
%6.     sample thickness (cm)
%7.     Latitude (decimal degrees, -90(S) to +90(N))
%8.     Longitude (decimal degrees, 0-360 degrees east)
%9.     Elevation (meters)
%10.    Pressure (hPa)                Both 9 and 10 must be present!
%11.    Shielding factor for terrain, snow, etc. (unitless)
%12.    Effective attenuation length -Lambdafe (g/cm^2)
%13.    % CO2                        Rock
%14.    % Na2O                       Rock
%15.    % MgO                        Rock
%16.    % Al2O3                      Rock
%17.    % SiO2                       Rock
%18.    % P2O5                       Rock
%19.    % K2O                        Rock
%20.    % CaO                        Rock
%21.    % TiO2                       Rock
%22.    % MnO                        Rock
%23.    % Fe2O3                      Rock
%24.    Cl (ppm)                     Rock
%25.    B (ppm)                      Rock
%26.    Sm (ppm)                     Rock
%27.    Gd (ppm)                     Rock
%28.    U (ppm)                      Rock
%29.    Th (ppm)                     Rock
%30.    Cr (ppm)                     Rock
%31.    Li (ppm)                     Rock
%32.	Target element %K2O          Target
%33.    Target element %CaO          Target
%34.    Target element %TiO2         Target
%35.    Target element %Fe2O3        Target
%36.    Target element Cl (ppm)      Target
%37.    Depth to top of sample (g/cm^2)
%38.    Year sampled (e.g. 2010)
%
% A second input variable pp contains physical parameters used in
% the calculations.  This is an input so that the production rates 
% can be varied.  
%
% scaling_model is one of 'DE','DU','LI','LM','SA','SF','ST' and 
% informs which scaling model is being used
%
% Returns an output vector:
%
% 1. Age (kyr)
% 2. Age uncertainty (kyr) (always 0 in this routine)
% 3. Elevation/latitude scaling factor for fast neutrons for Ca(unitless)
% 4. Elevation/latitude scaling factor for fast neutrons for K(unitless)
% 5. Elevation/latitude scaling factor for fast neutrons for Ti(unitless)
% 6. Elevation/latitude scaling factor for fast neutrons for Fe(unitless)
% 7. Elevation/latitude scaling factor for epithermal neutrons (unitless)
% 8. Elevation/latitude scaling factor for thermal neutrons (unitless)
% 9. Avg elevation/latitude scaling factor for fast muons (unitless)
% 10. Elevation/latitude scaling factor for slow muons (unitless)
% 11. Contemporary depth avg prod rate, Ca, spallation (atoms/g/yr)
% 12. Contemporary depth avg prod rate, K, spallation (atoms/g/yr)
% 13. Contemporary depth avg prod rate, Fe, spallation (atoms/g/yr)
% 14. Contemporary depth avg prod rate, Ti, spallation (atoms/g/yr)
% 15. Contemporary depth avg prod rate, Ca, muons (atoms/g/yr)
% 16. Contemporary depth avg prod rate, K, muons (atoms/g/yr)
% 17. Contemporary depth avg prod rate, Cl, low energy (atoms/g/yr)
% 18. Sigma_th.ss (cm^2/g)
% 19. Sigma_eth.ss (cm^2/g)
% 20. Sigma_sc.ss (cm^2/g)
% 21. Qs (unitless)
% 22. Qth (unitless)
% 23. Qeth (unitless)
% 24. Qmu (unitless)
% 25. Cosmogenic 36-Cl (atoms/g of target)
% 26. Radiogenic 36-Cl (atoms/g of target)
% 27. Measured 36-Cl (atoms/g of target)
% 28. Analytical (internal) uncertainty (kyr)
%
function output=cl36ageraw(sampledata,pp,sf,scaling_model)
%
% Make sampledata a column vectors if it isn't already.
%
if (size(sampledata,1)==1)
  sampledata=sampledata';
end
%
% First, check that the input data is reasonable.
%
if (length(sampledata) ~= 38)
  error('sampledata has wrong size!');
end
%
% Setup the values of sample parameters.
%
sp=samppars36(sampledata);
%
% Figure out the maximum possible depth at which we'll ever need a
% production rate.  This is depthtotop + maxage * erosion +
% thickness * density + a safety factor.
%
maxage=2000;               % 2Ma > 6 half lives 
maxdepth=sp.depthtotop+maxage*sp.epsilon+sp.ls*sp.rb+1000; 
%
% Computed parameters.  
%
cp=comppars36(pp,sp,sf,maxdepth);
%
% Compute the age. 
%
age=computeage36(pp,sp,sf,cp,scaling_model);
%
% Next, compute production rates for various pathways.
% Here, we call prodz once more at time 0 (present) and then
% compute the depth average from the results.
%
%
% In doing this computation, use the contemporary production rate.
%
sf.currentsf=getcurrentsf(sf,0,scaling_model,'cl');
%
% Have to recompute time dependent parameters.
%
% cp=comppars2(cp,pp,sp,sf);
%
% Compute production rates at 100 depths through the thickness of
% the sample.
%
nlayers=100;
thickness=sp.ls*sp.rb;
thick=(thickness/nlayers)*ones(nlayers,1);
totalthickness=sum(thick);
midpt=sp.depthtotop+(thickness/nlayers)*((1:nlayers)'-0.5);
[Prodtotal,Prods,ProdsCa,ProdsK,ProdsTi,ProdsFe,Prodth,Prodeth,Prodmu,...
 ProdmuCa,ProdmuK]=prodz36(midpt,pp,sf,cp);
Casrate=(thick'*ProdsCa)/totalthickness;
Ksrate=(thick'*ProdsK)/totalthickness;
Fesrate=(thick'*ProdsFe)/totalthickness;
Tisrate=(thick'*ProdsTi)/totalthickness;
Camurate=(thick'*ProdmuCa)/totalthickness;
Kmurate=(thick'*ProdmuK)/totalthickness;
Cllowenergy=(thick'*(Prodth+Prodeth))/totalthickness;
cp.Qs=(Casrate+Ksrate+Fesrate+Camurate)/(ProdsCa(1)+ProdsK(1)+ProdsFe(1)+ProdsTi(1));
cp.Qth=((thick'*Prodth)/totalthickness)/Prodth(1);
cp.Qeth=((thick'*Prodeth)/totalthickness)/Prodeth(1);
cp.Qmu=(Camurate+Kmurate)/(ProdmuCa(1)+ProdmuK(1));
%
% Setup the output vector.
%
output=[age; 0; sf.currentsf.Sel36Ca; sf.currentsf.Sel36K; sf.currentsf.Sel36Ti;...
    sf.currentsf.Sel36Fe; sf.currentsf.SFeth; sf.currentsf.SFth; ...
    cp.SFmufast; cp.SFmuslow; ...
    Casrate; Ksrate; Fesrate; Tisrate; ...
	Camurate; Kmurate; Cllowenergy; ...
	cp.Sigmathss; cp.Sigmaethss; cp.Sigmascss; cp.Qs; ...
	cp.Qth; cp.Qeth; cp.Qmu; cp.N36c; ...
        cp.N36r; cp.N36m; 0];

