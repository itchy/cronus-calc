%
%  age=ne21ageraw(sampledata,pp,sf,scaling_model)
%
%  Given the data for a sample computes the age of the sample.
%
%
% This inner routine does not handle the uncertainty calculations, 
% which are done by ne21age.m.  Instead, this inner routine simply 
% does the basic computation of the age.
%
% The sampledata vector contains the following information:
%
%1. Latitude (decimal degrees, -90(S) to +90(N))
%2. Longitude (decimal degrees, 0-360 degrees east)
%3. Elevation (meters)
%4. Pressure (hPa)      
%5. sample thickness (cm)
%6. bulk density (g/cm^3)
%7. Shielding factor for terrain, snow, etc. (unitless)
%8. Erosion-rate epsilon (g/(cm^2*kyr))
%9. Sample 21-ne concentration (atoms of 21-ne/g of target)
%10. Inheritance for 21-ne (atoms 21-ne/g of target)
%11. Effective attenuation length -Lambdafe (g/cm^2)
%12. Depth to top of sample (g/cm^2)
%13. Year sampled (e.g. 2010)
%
% Also requires physical parameters (pp) and scaling factors (sf).
%
% Returns an output vector:
%
% 1. Age (kyr)
% 2. Total (external) age uncertainty (always 0 in this routine)
% 3. Contemporary Elevation/latitude scaling factor for neutrons for ne (unitless)
% 4. Contemporary depth avg prod rate, neutron spallation (atoms/g/yr)
% 5. Qs (unitless)
% 6. Inherited 21-ne (atoms/g of target)
% 7. Measured 21-ne (atoms/g of target)
% 8. Analytical (internal) uncertainty (kyr)(always 0 in this routine)
%
function output=ne21ageraw(sampledata,pp,sf,scaling_model)
%
% Make sampledata a column vectors if it isn't already.
%
if (size(sampledata,1)==1)
  sampledata=sampledata';
end
%
% First, check that the input data is reasonable.
%
if (length(sampledata) ~= 13)
  error('sampledata has wrong size!');
end
%
% Setup the values of sample parameters.
%
sp=samppars21(sampledata);
%
% Figure out the maximum possible depth at which we'll ever need a
% production rate.  This is depthtotop + maxage * erosion +
% thickness * density + a safety factor.
%
maxage=2500;                       % 2.5Ma maximum
maxdepth=sp.depthtotop+maxage*sp.epsilon+sp.ls*sp.rb+1000; 
%
% Computed parameters.  
%
cp=comppars21(pp,sp,sf,maxdepth);
%
% Compute the age. 
%
age=computeage21(pp,sp,sf,cp,scaling_model);
%
% Next, compute production rates for various pathways.
% Here, we call prodz once more at time 0 (present) and then
% compute the depth average from the results.
%
%
% In doing this computation, use the contemporary production rate.
%
sf.currentsf=getcurrentsf(sf,0,scaling_model,'ne');
%
%
%
nlayers=100;
thickness=sp.ls*sp.rb;
thick=(thickness/nlayers)*ones(nlayers,1);
totalthickness=sum(thick);
midpt=sp.depthtotop+(thickness/nlayers)*((1:nlayers)'-0.5);
Psne=prodz21(midpt,pp,sf,cp);
srate=(thick'*Psne)/totalthickness;
Qs=srate/Psne(1);
%
% Setup the output vector.
%
output=[age; 0; sf.currentsf.Sel21; srate; Qs; ...
	sp.inheritance21; sp.concentration21; 0];



