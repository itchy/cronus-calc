%
% N3=predN3depth(pp,sp,sf,cp,age,depths,scaling_model)
%
% Predicts the 3-He concentration for a depth sample.
%
%  pp,sp,sf,cp            physics, site, sample, and
%                         computed parameters.
%  age                    Exposure age in kyr.
%  depths                 Depths in g/cm^2.
%
%
function N3=predN3depth(pp,sp,sf,cp,age,depths,scaling_model)
%
% Figure out how many depths we're doing this for.
%
ndepths=length(depths);
%
% Get the erosion rate in gm/(cm^2*yr) (sp.epsilon is in gm/cm^2/kyr)  
%
erosionrate=sp.epsilon/1000;
%
% Note that from now on in this function, the erosion rate is in g/cm^2/yr.
%
%
% Adjust contemporary depth to depth at start time.
%
currentdepths=depths+erosionrate*age*1000;
%
% We use a time step of 100 years for the integration in time.  
%
deltat=0.1;
%
% We integrate until the point in time where the sample was collected.
%
tfinal=sp.tfinal;
%
% Figure out the depth spacing.
%
deltadepth=deltat*erosionrate*1000;
%
% Now, the main integration loop.
%
N3=zeros(ndepths,1);
t=tfinal-age;
while (t+deltat < tfinal)
%
% Update the elevation/latitude scaling factor to the current
% time.  Note that our t is in kyr, with negative values in the
% past, while the TDSF stuff is done in terms of years before 2010.
%
  interptime=t+deltat/2;
  sf.currentsf=getcurrentsf(sf,interptime,scaling_model,'he');
%
% Compute the production rate.  We use the mid point of the range
% of depths corresponding to the current time interval.  
%
  if (min(currentdepths-deltadepth/2)<0)
    fprintf(1,'currentdepth-deltadepth/2 is %f\n',currentdepth- ...
	    deltadepth/2);
    warning('Negative depth!');
  end
  pz3=prodz3(currentdepths-deltadepth/2,pp,sf,cp);
%
% Update N3
%
% Because 3-He is a stable isotope, this calculation is much
% simpler than for other nuclides.
% 
  N3=N3+pz3*deltat*1000;
%
% Update t.
%
  t=t+deltat;
%
% Update depth
%
  currentdepths=currentdepths-deltat*1000*erosionrate;
end
%
% One final fractional time step.  deltatf is the length of this
% fractional time step.
%
deltatf=tfinal-t;
deltadepth=deltatf*erosionrate*1000;
%
% Update the elevation/latitude scaling factor to the current
% time.  Note that our t is in kyr, with negative values in the
% past, while the TDSF stuff is done in terms of years before present. 
%
interptime=t+deltatf/2;
sf.currentsf=getcurrentsf(sf,interptime,scaling_model,'he');
%
% Compute the production rates.
%
pz3=prodz3(currentdepths-deltadepth/2,pp,sf,cp);
%
% Update N3
%
N3=N3+pz3*deltatf*1000;
