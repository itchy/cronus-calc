
function [ total, conc36 ] = usingpredN36( scaling_model, inputfilename, outputfilename )
  
load( inputfilename, '-mat' );
%
% Get basic info about the sample ages.
%
nsamples=size(nominal36,1);
pp=physpars();
maxage=10000;

for i=1:nsamples;
    
  sp=samppars36(nominal36(i,:));
  sf=scalefacs36(sp36,scaling_model);
  
  % Figure out the maximum possible depth at which we'll ever need a
  % production rate.  This is depthtotop + maxage * erosion (g/cm^2/kyr)+
  % thickness * density + a safety factor.
  %
  maxdepth=sp.depthtotop+maxage*sp.epsilon+sp.ls*sp.rb+1000; 
  
  cp=comppars36(pp,sp,sf,maxdepth);
  age=ages(i);
  conc36(i)=predN36(pp,sp,sf,cp,age,scaling_model);
end

total=zeros(numbersamples,1);
total(:,1)=conc36;

if nargin > 2
  % Save the results.
  %
  save( outputfilename );
end
