%
% cp=comppars3(physpars,samppars,scalefactors,maxdepth)
%
% Generates a complete set of parameters for helium-3 dating
% from the basic physics parameters, sample specific parameters, 
% and scale factors.
%
% maxdepth is an optional parameter.  The default value is 2500
% g/cm^2, or about 10 meters.  Computing muon production profiles
% down this far is extremely expensive.  For surface samples, a max
% depth of 25 g/cm^2 (or about 10cm)  is more appropriate.  
%
function cp=comppars3(pp,sp3,sf3,maxdepth)
%
% First, set maxdepth to a default value if not supplied.
%
if (nargin < 4)
  maxdepth=2500;
end
%
% Record the maximum depth so that we know how deep it is safe to go.
%
cp.maxdepth=maxdepth;
%
% Setup Lambdafe.  
%
cp.Lambdafe=sp3.Lambdafe;
%

cp.Zs=sp3.ls*sp3.rb;
%
%---------------------------------------------------------------------
%
% % The RcEst above was originally used and has been replaced by Nat Lifton's
% % new model.  He  fit the function below to trajectory-traced cutoffs for the
% % modern dipolar field strength, so it should be more accurate overall.
% 
% dd = [6.89901,-103.241,522.061,-1152.15,1189.18,-448.004;];
% %   Trajectory-traced dipolar estimate for these purposes
%    RcEst = (dd(1)*cos(d2r(sp3.latitude)) + ...
%        dd(2)*(cos(d2r(sp3.latitude))).^2 + ...
%        dd(3)*(cos(d2r(sp3.latitude))).^3 + ...
%        dd(4)*(cos(d2r(sp3.latitude))).^4 + ...
%        dd(5)*(cos(d2r(sp3.latitude))).^5 + ...
%        dd(6)*(cos(d2r(sp3.latitude))).^6);
%    
% %     % %Use mean Solar Modulation Parameter (SPhiInf)
% nnn=ceil(maxdepth/5);
% %
% deltadepth=maxdepth/nnn;    % this gives the step for each
% 
% cp.depthvector=[0:deltadepth:maxdepth];
% %
% %store the output fluxes that we need
% 
% %Also store the muons production rates from the code
% %
% % Preallocate space for cp.muon3.  
% %
% cp.muon3=zeros(3,length(cp.depthvector));
% %
% % Store the depth vector.
% %
% cp.muon3(1,:)=cp.depthvector;  
% %
% 
% %
% % In the following, we'll
% % Generate He-3 production rates.
% %
% if ~isnan(sp3.concentration3)
% %set the constants to He-3
%   Natoms = pp.Natoms3;
%   sigma190 = pp.sigma190_3;
% %    pp.delsigma190 = pp.delsigma190_3; % not used
%   sigma0=pp.sigma03;
%   k_negpartial = pp.k_negpartial3;
%   aalpha=pp.aalpha3;
%   fstar=pp.fstar3;
% %    pp.delk_neg = pp.delk_neg3; % not used
%   muon3=muonfluxsato(cp.depthvector,sf3.P,RcEst,pp.SPhiInf,pp,'yes');
%   % Now calculate the production rates. 
% z=cp.depthvector;
% 
% %store muon scaling factor
% cp.SFmufast=muon3.SFmufast;
% cp.SFmuslow=muon3.SFmuslow;
% 
% % Depth-dependent parts of the fast muon reaction cross-section
% Beta = 0.846 - 0.015 .* log((z./100)+1) + 0.003139 .* (log((z./100)+1).^2);
% Ebar = 7.6 + 321.7.*(1 - exp(-8.059e-6.*z)) + 50.7.*(1-exp(-5.05e-7.*z));
% 
% %sigma0 = consts.sigma190./(190.^aalpha);
% 
% % fast muon production
% 
% P_fast = muon3.phi.*Beta.*(Ebar.^aalpha).*sigma0.*Natoms;
% 
% % negative muon capture
% P_neg = muon3.R.*k_negpartial.*fstar;
% 
% cp.P_total3=P_fast+P_neg;
%   
%   cp.muon3(2,:)=cp.P_total3;
%   cp.negflux=muon3.R;
%   cp.totalflux=muon3.phi;
% end


