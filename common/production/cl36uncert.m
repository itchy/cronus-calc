%
% uncert=cl36uncert(c)
%
% Given a 36-Cl concentration in atoms/gram, computes the
% uncertainty based on interpolation of the standard deviations of
% the intercomparison sample A and N uncertainties.
%
% Inputs:
%       c            concentration (atoms/gram)
%
% Output
%       uncert       uncertainty (1-sigma) atoms/gram)
%
function uncert=cl36uncert(c)
%
% For now, we're simply using 10%, since we don't have anything else.
%
uncert=0.05*c;
%
% We'd like to do something like this, but simply lack
% intercomparison data with which to do it.
%
%
% The concentrations and uncertainties for the A and N samples.
%
%c0=2.09e5;           % N-sample concentration atoms/gram
%u0=7.97;             % 1-sigma as a percentage of c0.
%c1=3.426e7;          % A-sample concentration atoms/gram
%u1=2.29;             % 1-sigma uncertainty as a percentage.
%
% Interpolate on the log of the concentration.  
% 
%u=u0+((log(c)-log(c0))/(log(c1)-log(c0)))*(u1-u0);
%uncert=u*c/100;
