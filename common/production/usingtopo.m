function out=usingtopo(input)

%define necessary data format for input: I will describe each "block".  The
%input is one block per sample with each one being added in the row
%following the previous sample

%cell (1,1)     a value of -1. This indicates a new sample
%cell (1,2)     Direction of dip (360 degree format)
%cell (1,3)     dip angle (value must be between 0-90 degrees)
%cell (2,1)     Azimuth of the horizon angle reading
%cell (2,2)     Horizon angle corresponding to the azimuth
%continue adding rows with azimuth, theta pairs until all measurements have
%been added
%fill in empty cells in columns 3 &4 with NaNs to avoid Matlab errors

numberrows=size(input,1);

% initialize the variables for horizon information,etc.
horizon=zeros(1,2);
dipangle=0;
dipdir=0;
Lf_a=0;
k=1; %one for each sample

for i=1:numberrows;
    % Deal with boundary condition (-1 at front of code)
    %if there is a -1, check to see if the entire horizon variable is filled 
    % or not; if
    %so, skip it; if not, then run the next steps
    
    if input(i,1)==-1;
        
        if size(horizon,1)~=1;
            horizon(end+1,1:2)=horizon(1,1:2);
            topo=topooriginal(horizon,dipdir,dipangle,'Lf_a',Lf_a);
            % save final results
            shield(k)=topo.ST_calc;
            Lambdaeff(k)=topo.Lf_calc;
            Stopog(k)=topo.Stopog;
            %increment the sample counter
            k=k+1;
            %zero out the horizon information
            horizon=zeros(1,2);
        end
        
        %save the information for the next sample
        j=0; %counter for number of rows within a particular sample
        dipangle=input(i,3);
        dipdir=input(i,2);
        Lf_a=input(i,4);
    else
        j=j+1;
        horizon(j,1:2)=input(i,1:2);
    end

end

%call topo stuff again to get the last sample
horizon(end+1,1:2)=horizon(1,1:2);
topo=topooriginal(horizon,dipdir,dipangle,'Lf_a',Lf_a);
% save final results
shield(k)=topo.ST_calc;
Lambdaeff(k)=topo.Lf_calc;
Stopog(k)=topo.Stopog;
out.shield=shield';
out.Lambdaeff=Lambdaeff';
out.Stopog=Stopog';