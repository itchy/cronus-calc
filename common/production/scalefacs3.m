%
% sf=scalefacs3(sample)
%
function sf=scalefacs3(sp3,scaling_model)
%
% Check if scaling_model was specified, otherwise set it to default
%
if (~exist('scaling_model','var')), scaling_model = 'all'; end

%
% Setup the scale factors.
%
sf.ST=sp3.ST;
sf.SLth=1;
sf.SLeth=1;
sf.P=sp3.P;
sf.elevation=sp3.elevation;

%
% Use Greg/Nat's code to compute the time dependent scale factors.
% The reason that we have a separate tdsfsample here is that the
% get_tdsf() function wasn't written by us.
%

load pmag_consts
tdsfsample.lat=sp3.latitude;
tdsfsample.long=sp3.longitude;
tdsfsample.pressure=sp3.P;
tdsfsample.elevation=sp3.elevation;
tdsfsample.scaling=scaling_model;
sf.tdsf=get_tdsf(tdsfsample,pmag_consts);
