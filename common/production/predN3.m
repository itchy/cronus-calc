%
% N3=predN3(pp,sp,sf,cp,age,scaling_model)
%
% Predicts N3 (in atoms/gm) given the parameters and the age in ka.
% This version has been updated to deal with samples at depth (see
% the sp.depthtotop parameter.)  It make use of predN3depth().
%
function N3=predN3(pp,sp,sf,cp,age,scaling_model)
%
% Convert the sample thickness to g/cm^2.
%
thickness=sp.ls*sp.rb;
%
% Set the number of depths to interpolate production across the
% thickness of the sample.
%
ndepths=10;
%
% Work out the specific depths at which to compute cumulative
% production.  
%
depths=zeros(ndepths,1);
deltadepth=thickness/ndepths;
for i=1:ndepths
  depths(i)=sp.depthtotop+deltadepth/2+deltadepth*(i-1);
end
%
% Compute N3 at each depth.
%
N3depths=zeros(ndepths,1);
N3depths=predN3depth(pp,sp,sf,cp,age,depths,scaling_model);
%
% average the results.
%
N3=mean(N3depths);
