%
% sf=scalefacs21(sample)
%
function sf=scalefacs21(sp21,scaling_model)
%
% Check if scaling_model was specified, otherwise set it to default
%
if (~exist('scaling_model','var')), scaling_model = 'all'; end

%
% Setup the scale factors.
%
sf.ST=sp21.ST;
sf.SLth=1;
sf.SLeth=1;
sf.P=sp21.P;
sf.elevation=sp21.elevation;

%
% Use Greg/Nat's code to compute the time dependent scale factors.
% The reason that we have a separate tdsfsample here is that the
% get_tdsf() function wasn't written by us.
%

load pmag_consts
tdsfsample.lat=sp21.latitude;
tdsfsample.long=sp21.longitude;
tdsfsample.pressure=sp21.P;
tdsfsample.elevation=sp21.elevation;
tdsfsample.scaling=scaling_model;
sf.tdsf=get_tdsf(tdsfsample,pmag_consts);
