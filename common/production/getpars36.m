%
%  [pp,sp36,sf36,cp36]=getpars36(sampledata36,maxdepth);  
%  
%
%  The sampledata vector contains the following information.
%
%1.     Sample 36-Cl concentration (atoms of 36-Cl/g of target)
%2.     Inheritance (atoms 36-Cl/g of target)  
%3.     erosion-rate epsilon (mm/kyr)
%4.     fractional volumetric water-content (unitless) 
%5.     bulk density (g/cm^3)
%6.     sample thickness (cm)
%7.     Latitude (decimal degrees)
%8.     Longitude (decimal degrees)
%9.     Elevation (meters)
%10.    Pressure (hPa)                Either 9 or 10 must be NaN. 
%11.    Shielding factor for terrain, snow, etc. (unitless)
%12.    Lambdafe Effective neutron attenuation length (g/cm^2)
%13.    % CO2                        Rock
%14.    % Na2O                       Rock
%15.    % MgO                        Rock
%16.    % Al2O3                      Rock
%17.    % SiO2                       Rock
%18.    % P2O5                       Rock
%19.    % K2O                        Rock
%20.    % CaO                        Rock
%21.    % TiO2                       Rock
%22.    % MnO                        Rock
%23.    % Fe2O3                      Rock
%24.    Cl (ppm)                     Rock
%25.    B (ppm)                      Rock
%26.    Sm (ppm)                     Rock
%27.    Gd (ppm)                     Rock
%28.    U (ppm)                      Rock
%29.    Th (ppm)                     Rock
%30.    Cr (ppm)                     Rock
%31.    Li (ppm)                     Rock
%32.	Target element %K2O          Target
%33.    Target element %CaO          Target
%34.    Target element %TiO2         Target
%35.    Target element %Fe2O3        Target
%36.    Target element Cl (ppm)      Target
%37.    Depth to top of sample (g/cm^2)
%
% maxdepth              Optional, maximum depth in g/cm^2.
%
% Outputs:
%
%     pp                Physical parameters that are sample independent
%     sp36              Parameters of this particular sample
%     sf36              Site dependent scale factors
%     cp36              Further parameters computed from pp, sp36, and sf36.
%
function [pp,sp36,sf36,cp36]=getpars36(sampledata36,maxdepth)
%
% If maxdepth is not specified, use a default.
%
if (nargin < 2)
  maxdepth=2500;
end
%
% Get the physical parameters.
%
pp=physpars();
%
% Extract the sample parameters from the sampledatavector.
%
sp36=samppars36(sampledata36);
%
% Get the scale factors.
%
sf36=scalefacs36(sp36);
%
% Computed parameters.
%
cp36=comppars36(pp,sp36,sf36,maxdepth);
%
% Go ahead and produce contemporary scaling factors.
%
sf36.currentsf=getcurrentsf(sf36,0);

