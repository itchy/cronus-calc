function out=usingattenuationlength(atteninput);

%sample is group of samples (one sample per row) that contain lat, 
% long, elev, pressure, and
%independent age. This code averages the mean rigidity cutoff over the
%exposure age of the sample instead of the entire geomagnetic history. The
%outputs are: 
% 
% out.time - this is the time steps in the geomagnetic history (double check
% that the code used the correct time steps)
% out.RcEstAge - This is the Rc averaged over the exposure age of the
% sample
% out.RcEst - This is the Rc averaged over the entire available history
% (for comparison, not used)
% out.endage - This is the last time used by the code for the geomagnetic history 
% out.Lambdanotopo - This is the attenuation length for each sample. 

numbersamples=size(atteninput,1);
RcEstAge=zeros(numbersamples,1);

for k=1:numbersamples
    %loop over each sample
sample=zeros(15,1);
sample(1)=atteninput(k,1);
sample(2)=atteninput(k,2);
sample(3)=atteninput(k,3);
sample(4)=atteninput(k,4);
sample(5)=3;
sample(6)=2.66;
sample(7)=1.0;
sample(8)=0.0;
sample(9)=0.0;
sample(10)=0.0;
sample(11)=0.0;
sample(12)=0.0;
sample(13)=150;
sample(14)=0;
sample(15)=2010;
%
% Run get pars to get geomag information for this site.
%
[pp,sp,sf,cp]=getpars1026(sample);
out.tdsf=sf.tdsf;
    i=1;
    while sf.tdsf.tv(i) <= atteninput(k,5);
        time(k,i)=sf.tdsf.tv(i);
        Rc(i)= sf.tdsf.Rc_Sa(i);
        i=i+1; 
    end
endage(k)=time(k,end);

RcEstAge(k)=mean(Rc);
%compare this with the old value of RcEst
RcEst(k)=mean(sf.tdsf.Rc_Sa);

%calculate the attenuation length for the sample
Lambdanotopo(k)=rawattenuationlength(atteninput(k,4),RcEstAge(k));
end

out.time=time;
out.RcEstAge=RcEstAge;
out.RcEst=RcEst;
out.endage=endage;
out.Lambdanotopo=Lambdanotopo;