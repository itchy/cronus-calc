%
% [ProdtotalNe,ProdsNe]=prodz21(z,pp,sf,cp)
%
%
% The prodz function calculates spallation production of 21Ne
% at a particular given depth. 

%  pp,sf,cp               physics, site, sample, and
%                         computed parameters.
%  z                      Depths in g/cm^2.
% 
% Get current scaling factors. 
% Then call this function to compute total production for a vector of
% depths.  
% Note: This function has been updated to work with nuclide-dependent
% scaling factors (only applicable to the Sato/Lifton scaling scheme).  
%
function [ProdtotalNe,ProdsNe]=prodz21(z,pp,sf,cp)
%
% Make sure that we don't have any depths that are too big.
%
if (max(z) > cp.maxdepth)
  error('Prodz called for depth greater than maxdepth.');
end
%
% Make sure that sf.SelX is a number.
%
if (isnan(sf.currentsf.Sel21))
    error('sf.currentsf.Sel21 is a NaN');
end
%
% We'll get some slightly negative depths due to roundoff errors.
% Fix them.
%
for i=1:length(z)
  if (z(i)<-1.0e-4)
    error('z(i) is negative!');
  end
  if (z(i)<0.0)
    z(i)=0.0;
  end
end

%
%find the number of depths given in the input vector z
numberdepths=length(z);
%
% Production from spallation is all that is currently included for Ne.
%
ProdsNe=sf.currentsf.Sel21*sf.ST*pp.PsNe*exp(-z/cp.Lambdafe);
%
% Now, compute the total production.
%
ProdtotalNe=ProdsNe;
%
% Look for NaN's in the results.
%
if ((sum(isnan(ProdtotalNe)) > 0) )
  ProdtotalNe
  error('Prodz21 produced NaN!');
end


