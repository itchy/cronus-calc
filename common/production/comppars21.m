%
% cp=comppars21(physpars,samppars,scalefactors,maxdepth)
%
% Generates a complete set of parameters for neon-21 dating
% from the basic physics parameters, sample specific parameters, 
% and scale factors.
%
% maxdepth is an optional parameter.  The default value is 2500
% g/cm^2, or about 10 meters.  Computing muon production profiles
% down this far is extremely expensive.  For surface samples, a max
% depth of 25 g/cm^2 (or about 10cm)  is more appropriate.  
%
function cp=comppars21(pp,sp21,sf21,maxdepth)
%
% First, set maxdepth to a default value if not supplied.
%
if (nargin < 4)
  maxdepth=2500;
end
%
% Record the maximum depth so that we know how deep it is safe to go.
%
cp.maxdepth=maxdepth;
%
% Setup Lambdafe.  
%
cp.Lambdafe=sp21.Lambdafe;
%

cp.Zs=sp21.ls*sp21.rb;
%
%---------------------------------------------------------------------
%
% % The RcEst above was originally used and has been replaced by Nat Lifton's
% % new model.  Ne  fit the function below to trajectory-traced cutoffs for the
% % modern dipolar field strength, so it should be more accurate overall.
% 
% dd = [6.89901,-103.241,522.061,-1152.15,1189.18,-448.004;];
% %   Trajectory-traced dipolar estimate for these purposes
%    RcEst = (dd(1)*cos(d2r(sp21.latitude)) + ...
%        dd(2)*(cos(d2r(sp21.latitude))).^2 + ...
%        dd(3)*(cos(d2r(sp21.latitude))).^3 + ...
%        dd(4)*(cos(d2r(sp21.latitude))).^4 + ...
%        dd(5)*(cos(d2r(sp21.latitude))).^5 + ...
%        dd(6)*(cos(d2r(sp21.latitude))).^6);
%    
% %     % %Use mean Solar Modulation Parameter (SPhiInf)
% nnn=ceil(maxdepth/5);
% %
% deltadepth=maxdepth/nnn;    % this gives the step for each
% 
% cp.depthvector=[0:deltadepth:maxdepth];
% %
% %store the output fluxes that we need
% 
% %Also store the muons production rates from the code
% %
% % Preallocate space for cp.muon21.  
% %
% cp.muon21=zeros(3,length(cp.depthvector));
% %
% % Store the depth vector.
% %
% cp.muon21(1,:)=cp.depthvector;  
% %
% 
% %
% % In the following, we'll
% % Generate Ne-21 production rates.
% %
% if ~isnan(sp21.concentration21)
% %set the constants to Ne-21
%   Natoms = pp.Natoms21;
%   sigma190 = pp.sigma190_21;
% %    pp.delsigma190 = pp.delsigma190_21; % not used
%   sigma0=pp.sigma021;
%   k_negpartial = pp.k_negpartial21;
%   aalpha=pp.aalpha21;
%   fstar=pp.fstar21;
% %    pp.delk_neg = pp.delk_neg21; % not used
%   muon21=muonfluxsato(cp.depthvector,sf21.P,RcEst,pp.SPhiInf,pp,'yes');
%   % Now calculate the production rates. 
% z=cp.depthvector;
% 
% %store muon scaling factor
% cp.SFmufast=muon21.SFmufast;
% cp.SFmuslow=muon21.SFmuslow;
% 
% % Depth-dependent parts of the fast muon reaction cross-section
% Beta = 0.846 - 0.015 .* log((z./100)+1) + 0.003139 .* (log((z./100)+1).^2);
% Ebar = 7.6 + 321.7.*(1 - exp(-8.059e-6.*z)) + 50.7.*(1-exp(-5.05e-7.*z));
% 
% %sigma0 = consts.sigma190./(190.^aalpha);
% 
% % fast muon production
% 
% P_fast = muon21.phi.*Beta.*(Ebar.^aalpha).*sigma0.*Natoms;
% 
% % negative muon capture
% P_neg = muon21.R.*k_negpartial.*fstar;
% 
% cp.P_total21=P_fast+P_neg;
%   
%   cp.muon21(2,:)=cp.P_total21;
%   cp.negflux=muon21.R;
%   cp.totalflux=muon21.phi;
% end


