This directory contains the MATLAB code and data sets for the CRONUS project
production rate models, production rate calibration, surface sample calculator
and depth profile calculator. 

The directories are organized as:
 
production:               Basic functions for computing production rates
                          and accumulated production for depth and surface
                          samples
 
surfacecalc               Functions for computing the exposure age of surface 
			  samples

profilecalc		  Functions for the computing the exposure age of
			  depth profiles.

calib                     Code and data for calibrating spallation production
                          parameters.

muoncalib		  Code and data for calibrating muon production 
			  parameters.

pf0calib		  Code and data for calibrating 36-Cl thermal and 
			  epithermal neutron production parameter Pf(0).  
                          Currenty empty.  

excelformatting           Scripts for formatting an Excell spreadsheet as 
                          a .mat file.

Note that you must add the "production" directory to search directory so that 
these functions will be visible to MATLAB when working in the other 
directories.  This can be done with the "addpath" command.  e.g., if the
production directory is in /home/borchers/cronuscode/production, then use
 
>> addpath /home/borchers/cronuscode/production
 
to add the production directory to the search path.  

Finally, note that this software is licensed under the Gnu Public
License (GPL) version 2.  See gpl.txt
