function out=createcalib10(sample)

% Uses the standard input for a Be-10/Al-26 sample and creates the individual
% variables needed for aging or calibration in the rest of the code.  

%sample input must be the inputs
%with no spaces between: inputs,uncerts,independent ages. 
% One row per sample with the following columns:

%1. Latitude (decimal degrees, -90(S) to +90(N))
%2. Longitude (decimal degrees, 0-360 degrees east)
%3. Elevation (meters)
%4. Pressure (hPa)       
%5. sample thickness (cm)
%6. bulk density (g/cm^3)
%7. Shielding factor for terrain, snow, etc. (unitless)
%8. Erosion-rate epsilon (mm/kyr)
%9. Sample 10-Be concentration (atoms of 10-Be/g of target)
%10. Sample 26-Al concentration (atoms of 26-Al/g of target)
%11. Inheritance for Be (atoms 10-Be/g of target)
%12. Inheritance for Al (atoms 26-Al/g of target)
%13. Lambdafe (g/cm^2)
%14. Depth to top of sample (g/cm^2)
%15. Year sampled (e.g. 2010)
%16-30. One-sigma uncertainties for each of the above inputs (1-15). 
%31. Independent age of the sample (years before AD 2010)
%32. Uncertainty on independent age of the sample (years)

numbersamps=size(sample,1); %this gives the number of samples
%initialize the results
nominal10=zeros(numbersamps,15);
indages10=zeros(numbersamps,2);
uncerts10=zeros(numbersamps,15);

nominal10(:,1:15)=sample(:,1:15);
uncerts10(:,1:15)=sample(:,16:30);
indages10(:,1:2)=sample(:,31:32);

%convert erosion rate from mm/kyr to g/cm^2/kyr
nominal10(:,8)=nominal10(:,8).*nominal10(:,6)./10;
uncerts10(:,8)=uncerts10(:,8).*nominal10(:,6)./10;

save CCMMB_3 nominal10 uncerts10 indages10