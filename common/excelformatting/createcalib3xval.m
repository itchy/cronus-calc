function out=createcalib3xval(sample)

%Uses standard input + 4 additional columns necessary for calibrations. 
% Creates the individual variables needed for calibration as well as the
% datasets necessary for cross-validation.  

%sample input must be the inputs
%with no spaces between: inputs,uncerts,independent ages. 
% One row per sample with the following columns:
%
%1. Latitude (decimal degrees, -90(S) to +90(N))
%2. Longitude (decimal degrees, 0-360 degrees east)
%3. Elevation (meters)
%4. Pressure (hPa)      
%5. sample thickness (cm)
%6. bulk density (g/cm^3)
%7. Shielding factor for terrain, snow, etc. (unitless)
%8. Erosion-rate epsilon (mm/kyr)
%9. Sample 3-He concentration (atoms of 3-He/g of target)
%10. Inheritance for 3-He (atoms 3-He/g of target)
%11. Lambdafe (g/cm^2)
%12. Depth to top of sample (g/cm^2)
%13. Year sampled (e.g. 2010)
%14-26. Uncertainty for each of the above variables
%27. Independent age of the sample (years before AD 2010)
%28. Uncertainty on independent age of the sample (years)
%29. Age Index indicator (should increment for each new age, even within
%sites (ex. if boulder ages are allowed to vary within the site)
%34. Site indicator: should match the sites given for the calibration list
%here:
% 1 - Promontory Point & Tabernacle Hill
% 2 - New Zealand
% 3 - Peru
% 4 - Scotland
% 5 - New England
% 6 - Argentina (Ackert 2003) (ARG)
% 7 - Oregon (Cerling & Craig, 1994; Licciardi 1999) (OR)
% 8 - Idaho (Cerling & Craig, 1994) (ID)
% 9 - Canary Islands (Dunai & Wijbrans 2000) (CI)
%10 - Iceland (Licciardi 2006) (ICE)
%11 - Hawaii (Kurz 1990) (HAW)
%12 - Sicily (Blard 2006) (ETNA)

numbersamps=size(sample,1); %this gives the number of samples
%initialize the results
nominal3=zeros(numbersamps,13);
indages3=zeros(numbersamps,2);
uncerts3=zeros(numbersamps,13);
ageindexsample3=zeros(numbersamps,1);
siteindex3=zeros(numbersamps,1);

nominal3(:,1:13)=sample(:,1:13);
uncerts3(:,1:13)=sample(:,14:26);
indages3(:,1:2)=sample(:,27:28);
ageindexsample3(:,1)=sample(:,29);
siteindex3(:,1)=sample(:,30);

%convert erosion rate from mm/kyr to g/cm^2/kyr
nominal3(:,8)=nominal3(:,8).*nominal3(:,6)./10;
uncerts3(:,8)=uncerts3(:,8).*nominal3(:,6)./10;


%split up samples based on aging index/site index
%initialize the matrix with the first sample
ageindex3(1)=1;
previousindex=ageindexsample3(1);
%indagenew is the variable for the new aging method. Indages is the
%variable that has the independent age for each sample.
indagenew3(1,1:2)=indages3(1,1:2);
countindage=1;

for i=2:size(nominal3,1);
    currentindex=ageindexsample3(i);
    if currentindex==previousindex;
        %same age as previous sample, so only use one age
        ageindex3(i)=ageindex3(i-1);
    else
        countindage=countindage+1;
        ageindex3(i)=ageindex3(i-1)+1;
        indagenew3(countindage,1:2)=indages3(i,1:2);
        
    end
    previousindex=currentindex;
end
save calibset3test nominal3 uncerts3 indages3 ageindex3 indagenew3

%Below this point - cross-validation datasets are produced

fullnominal3=nominal3;
fulluncerts3=uncerts3;
fullindages3=indages3;
fullageindex3=ageindex3';

for i=1:12
    switch i
        case 1;
            smallname='PPTonly3';
            bigname='calib3minusPPT';
            countsiteindex=1;
            
        case 2;
            countsiteindex=2;
            smallname='NZonly3';
            bigname='calib3minusNZ';
            
        case 3;
            countsiteindex=3;
            smallname='HUonly3';
            bigname='calib3minusHU';
            
        case 4;
            countsiteindex=4;
            smallname='SCOTonly3';
            bigname='calib3minusSCOT';
        case 5;
            countsiteindex=5;
            smallname='NEonly3';
            bigname='calib3minusNE';
        case 6;
            countsiteindex=6;
            smallname='ARGonly3';
            bigname='calib3minusARG';
        case 7;
            countsiteindex=7;
            smallname='ORonly3';
            bigname='calib3minusOR';
        case 8;
            countsiteindex=8;
            smallname='IDonly3';
            bigname='calib3minusID';
        case 9;
            countsiteindex=9;
            smallname='CIonly3';
            bigname='calib3minusCI'; 
        case 10;
            countsiteindex=10;
            smallname='ICEonly3';
            bigname='calib3minusICE';
        case 11;
            countsiteindex=11;
            smallname='HAWonly3';
            bigname='calib3minusHAW';
        case 12;
            countsiteindex=12;
            smallname='ETNAonly3';
            bigname='calib3minusETNA';
    end
    countsmall=1;
    countbig=1;
    datafound=false;
    numbersamps=size(fullnominal3,1);
    for k=1:numbersamps;
        if siteindex3(k,1)==countsiteindex;
            %copy sample(k) to small matrix
            nominalsmall(countsmall,:)=fullnominal3(k,:);
            uncertssmall(countsmall,:)=fulluncerts3(k,:);
            indagessmall(countsmall,:)=fullindages3(k,:);
            ageindexsmalltemp(countsmall,:)=fullageindex3(k,:);
            countsmall=countsmall+1;
            datafound=true;
            %create the age index and indpt ages matrices
        else
            % copy sample(k) to big matrix
            nominalbig(countbig,:)=fullnominal3(k,:);
            uncertsbig(countbig,:)=fulluncerts3(k,:);
            indagesbig(countbig,:)=fullindages3(k,:);
            ageindexbigtemp(countbig,:)=fullageindex3(k,:);
            countbig=countbig+1;
        end
    end
    % save variables to the correct name (renaming them)
    if datafound==false
        continue
    end 
    nominal3=nominalsmall;
    uncerts3=uncertssmall;
    indages3=indagessmall;
    
%Creating the age index and indages for the x val datasets
%initialize the matrix with the first sample
ageindexsmall(1)=1;
previousindex=ageindexsmalltemp(1);
%indagenew is the variable for the new aging method. Indages is the
%variable that has the independent age for each sample.
indagenewsmall(1,1:2)=indages3(1,1:2);
countindage=1;

for i=2:size(nominal3,1);
    currentindex=ageindexsmalltemp(i);
    if currentindex==previousindex;
        %same age as previous sample, so only use one age
        ageindexsmall(i)=ageindexsmall(i-1);
    else
        countindage=countindage+1;
        ageindexsmall(i)=ageindexsmall(i-1)+1;
        indagenewsmall(countindage,1:2)=indages3(i,1:2);
        
    end
    previousindex=currentindex;
end
%rename variables
ageindex3=ageindexsmall;
indagenew3=indagenewsmall;
    save(smallname, 'nominal3', 'uncerts3', 'indages3','indagenew3','ageindex3')

% For the "big" matrix of values
    nominal3=nominalbig;
    uncerts3=uncertsbig;
    indages3=indagesbig;
    
    %initialize the matrix with the first sample
ageindexbig(1)=1;
previousindex=ageindexbigtemp(1);
%indagenew is the variable for the new aging method. Indages is the
%variable that has the independent age for each sample.
indagenewbig(1,1:2)=indages3(1,1:2);
countindage=1;

for i=2:size(nominal3,1);
    currentindex=ageindexbigtemp(i);
    if currentindex==previousindex;
        %same age as previous sample, so only use one age
        ageindexbig(i)=ageindexbig(i-1);
    else
        countindage=countindage+1;
        ageindexbig(i)=ageindexbig(i-1)+1;
        indagenewbig(countindage,1:2)=indages3(i,1:2);
        
    end
    previousindex=currentindex;
end
%rename variables
    
    ageindex3=ageindexbig;
    indagenew3=indagenewbig;
    save(bigname, 'nominal3','uncerts3', 'indages3','indagenew3','ageindex3')
    
    %clear variables so we can rewrite in the next site
    
    clear nominalsmall
    clear uncertssmall
    clear indagessmall
    clear nominalbig
    clear uncertsbig
    clear indagesbig
    clear ageindexbig
    clear ageindexsmall
    clear ageindexsmalltemp
    clear ageindexbigtemp
    clear indagenewsmall
    clear indagenewbig
end

end
