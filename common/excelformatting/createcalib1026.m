function out=createcalib1026(sample)

%Uses standard input + 3 additional columns necessary for calibrations. 
% Creates the individual variables needed for calibration.  

%sample input must be the inputs
%with no spaces between: inputs,uncerts,independent ages. Note that all the
%samples from a single site must be together in order for the calibration
%dataset to be produced correctly. 

% One row per sample with the following columns:

%1. Latitude (decimal degrees, -90(S) to +90(N))
%2. Longitude (decimal degrees, 0-360 degrees east)
%3. Elevation (meters)
%4. Pressure (hPa)       
%5. sample thickness (cm)
%6. bulk density (g/cm^3)
%7. Shielding factor for terrain, snow, etc. (unitless)
%8. Erosion-rate epsilon (mm/kyr)
%9. Sample 10-Be concentration (atoms of 10-Be/g of target)
%10. Sample 26-Al concentration (atoms of 26-Al/g of target)
%11. Inheritance for Be (atoms 10-Be/g of target)
%12. Inheritance for Al (atoms 26-Al/g of target)
%13. Lambdafe (g/cm^2)
%14. Depth to top of sample (g/cm^2)
%15. Year sampled (e.g. 2010)
%16-30. One-sigma uncertainties for each of the above inputs (1-15). 
%31. Independent age of the sample (years before AD 2010)
%32. Uncertainty on independent age of the sample (years)
%33. Age Index indicator (should increment for each new age, even within
%sites (ex. if boulder ages are allowed to vary within the site)

numbersamps=size(sample,1); %this gives the number of samples
%initialize the results
%go through each sample and see if it has Be and/or Al nd put it into the
%appropriate vectors (sample10 and sample26)
count10=1;
count26=1;

for i=1:numbersamps;

    if isnan(sample(i,9)) || sample(i,9)==0;
    else
        sample10(count10,:)=sample(i,:);
        count10=count10+1;
    end
    if isnan(sample(i,10)) || sample(i,10)==0; 
    else
        sample26(count26,:)=sample(i,:);
        count26=count26+1;
    end
end

if count10~=1
nominal10(:,1:15)=sample10(:,1:15);
uncerts10(:,1:15)=sample10(:,16:30);
indages10(:,1:2)=sample10(:,31:32);
ageindexsample10(:,1)=sample10(:,33);

%convert erosion rate from mm/kyr to g/cm^2/kyr
nominal10(:,8)=nominal10(:,8).*nominal10(:,6)./10;
uncerts10(:,8)=uncerts10(:,8).*nominal10(:,6)./10;

%split up samples based on aging index/site index
% For Be
%initialize the matrix with the first sample
ageindex10(1)=1;
previousindex=ageindexsample10(1);
%indagenew is the variable for the new aging method. Indages is the
%variable that has the independent age for each sample.
indagenew10(1,1:2)=indages10(1,1:2);
countindage=1;

for i=2:size(nominal10,1);
    currentindex=ageindexsample10(i);
    if currentindex==previousindex;
        %same age as previous sample, so only use one age
        ageindex10(i)=ageindex10(i-1);
    else
        countindage=countindage+1;
        ageindex10(i)=ageindex10(i-1)+1;
        indagenew10(countindage,1:2)=indages10(i,1:2);
        
    end
    previousindex=currentindex;
end
save calibset10test nominal10 uncerts10 indages10 ageindex10 indagenew10
end

if count26~=1
nominal26(:,1:15)=sample26(:,1:15);
uncerts26(:,1:15)=sample26(:,16:30);
indages26(:,1:2)=sample26(:,31:32);
ageindexsample26(:,1)=sample26(:,33);

%convert erosion rate from mm/kyr to g/cm^2/kyr
nominal26(:,8)=nominal26(:,8).*nominal26(:,6)./10;
uncerts26(:,8)=uncerts26(:,8).*nominal26(:,6)./10;

 % For Al
%initialize the matrix with the first sample
ageindex26(1)=1;
previousindex=ageindexsample26(1);
%indagenew is the variable for the new aging method. Indages is the
%variable that has the independent age for each sample.
indagenew26(1,1:2)=indages26(1,1:2);
countindage=1;

for i=2:size(nominal26,1);
    currentindex=ageindexsample26(i);
    if currentindex==previousindex;
        %same age as previous sample, so only use one age
        ageindex26(i)=ageindex26(i-1);
    else
        countindage=countindage+1;
        ageindex26(i)=ageindex26(i-1)+1;
        indagenew26(countindage,1:2)=indages26(i,1:2);
    end
    previousindex=currentindex;
end   
 save calibset26test nominal26 uncerts26 indages26 ageindex26 indagenew26   
end




