function [nominal36,uncerts36,cov36]=createage36(sample)

% Uses the standard input for a chlorine-36 sample and creates the individual
% variables needed for aging in the rest of the code.  

%sample input must be the inputs
%with no spaces between: inputs,uncerts,independent ages. 
% One row per sample with the following columns:

%1. 7.     Latitude (decimal degrees)
%2. 8.     Longitude (decimal degrees)
%3. 9.     Elevation (meters)
%4. 10.    Pressure (hPa)                 
%5. 6.     sample thickness (cm)
%6. 5.     bulk density (g/cm^3)
%7. 11.    Shielding factor for terrain, snow, etc. (unitless)
%8. 3.     erosion-rate epsilon (mm/kyr)
%9. 1.     Sample 36-Cl concentration (atoms of 36-Cl/g of target)
%10. 2.     Inheritance (atoms 36-Cl/g of target)  
%11. 12.    Lambdafe Effective neutron attenuation length (g/cm^2)
%12. 38.    Depth to top of sample (g/cm^2)
%13. 39.    Year Collected (AD)
%14. 4.     fractional volumetric water-content (unitless) 
%15. 13.    wt % SiO2                    Rock
%16. 14.    wt % TiO2                    Rock
%17. 15.    wt % Al2O3                   Rock
%18. 16.    wt % Fe2O3                   Rock
%19. 17.    wt % MnO                     Rock
%20. 18.    wt % MgO                     Rock
%21. 19.    wt % CaO                     Rock
%22. 20.    wt % Na2O                    Rock
%23. 21.    wt % K2O                     Rock
%24. 22.    wt % P2O5                    Rock
%25. 23.    wt % Analytical Water        Rock
%26. 24.    wt % CO2                     Rock
%27. 25.    Cl (ppm)                     Rock
%28. 26.    B (ppm)                      Rock
%29. 27.    Sm (ppm)                     Rock
%30. 28.    Gd (ppm)                     Rock
%31. 29.    U (ppm)                      Rock
%32. 30.    Th (ppm)                     Rock
%33. 31.    Cr (ppm)                     Rock
%34. 32.    Li (ppm)                     Rock
%35. 33.	Target element %K2O          Target
%36. 34.    Target element %CaO          Target
%37. 35.    Target element %TiO2         Target
%38. 36.    Target element %Fe2O3        Target
%39. 37.    Target element Cl (ppm)      Target

%40-78  Uncerts (one for each input above)
%79. 81.    Covariance between cl and at36Cl/g
%80. 79.    Calibration age (years, NOT ka) (not used)
%81. 80.    Calibration age uncertainty (years) (not used)


% Outputs of the code (nominal36):
%1.     Sample 36-Cl concentration (atoms of 36-Cl/g of target)
%2.     Inheritance (atoms 36-Cl/g of target)  
%3.     erosion-rate epsilon ((g/cm^2)/kyr)
%4.     fractional volumetric water-content (unitless) 
%5.     bulk density (g/cm^3)
%6.     sample thickness (cm)
%7.     Latitude (decimal degrees)
%8.     Longitude (decimal degrees)
%9.     Elevation (meters)
%10.    Pressure (hPa)                
%11.    Shielding factor for terrain, snow, etc. (unitless)
%12.    Lambdafe Effective neutron attenuation length (g/cm^2)
%13.    % CO2                        Rock
%14.    % Na2O                       Rock
%15.    % MgO                        Rock
%16.    % Al2O3                      Rock
%17.    % SiO2                       Rock
%18.    % P2O5                       Rock
%19.    % K2O                        Rock
%20.    % CaO                        Rock
%21.    % TiO2                       Rock
%22.    % MnO                        Rock
%23.    % Fe2O3                      Rock
%24.    Cl (ppm)                     Rock
%25.    B (ppm)                      Rock
%26.    Sm (ppm)                     Rock
%27.    Gd (ppm)                     Rock
%28.    U (ppm)                      Rock
%29.    Th (ppm)                     Rock
%30.    Cr (ppm)                     Rock
%31.    Li (ppm)                     Rock
%32.	Target element %K2O          Target
%33.    Target element %CaO          Target
%34.    Target element %TiO2         Target
%35.    Target element %Fe2O3        Target
%36.    Target element Cl (ppm)      Target
%37.    Depth to top of sample (g/cm^2)
%38.    Year Collected (AD)
% The new format is 3 files:
%nominal36, uncerts36, covar36.

numbersamps=size(sample,1); %this gives the number of samples
%initialize the results
nominal36=zeros(numbersamps,38);
uncerts36=zeros(numbersamps,38);
cov36=zeros(numbersamps,1);

%copy in the values directly from the inputs

nominal36(:,1)=sample(:,9);
nominal36(:,2)=sample(:,10);
nominal36(:,3)=sample(:,8);
nominal36(:,4)=sample(:,14);
nominal36(:,5)=sample(:,6);
nominal36(:,6)=sample(:,5);
nominal36(:,7)=sample(:,1);
nominal36(:,8)=sample(:,2);
nominal36(:,9)=sample(:,3);
nominal36(:,10)=sample(:,4);
nominal36(:,11)=sample(:,7);
nominal36(:,12)=sample(:,11);

nominal36(:,13)=sample(:,26);
nominal36(:,14)=sample(:,22);
nominal36(:,15)=sample(:,20);
nominal36(:,16)=sample(:,17);
nominal36(:,17)=sample(:,15);
nominal36(:,18)=sample(:,24);
nominal36(:,19)=sample(:,23);
nominal36(:,20)=sample(:,21);
nominal36(:,21)=sample(:,16);
nominal36(:,22)=sample(:,19);
nominal36(:,23)=sample(:,18);

nominal36(:,24:36)=sample(:,27:39);
nominal36(:,37)=sample(:,12);
nominal36(:,38)=sample(:,13);


uncerts36(:,1)=sample(:,48);
uncerts36(:,2)=sample(:,49);
uncerts36(:,3)=sample(:,47);
uncerts36(:,4)=sample(:,53);
uncerts36(:,5)=sample(:,45);
uncerts36(:,6)=sample(:,44);
uncerts36(:,7)=sample(:,40);
uncerts36(:,8)=sample(:,41);
uncerts36(:,9)=sample(:,42);
uncerts36(:,10)=sample(:,43);
uncerts36(:,11)=sample(:,46);
uncerts36(:,12)=sample(:,50);

uncerts36(:,13)=sample(:,65);
uncerts36(:,14)=sample(:,61);
uncerts36(:,15)=sample(:,59);
uncerts36(:,16)=sample(:,56);
uncerts36(:,17)=sample(:,54);
uncerts36(:,18)=sample(:,63);
uncerts36(:,19)=sample(:,62);
uncerts36(:,20)=sample(:,60);
uncerts36(:,21)=sample(:,55);
uncerts36(:,22)=sample(:,58);
uncerts36(:,23)=sample(:,57);

uncerts36(:,24:36)=sample(:,66:78);
uncerts36(:,37)=sample(:,51);
uncerts36(:,38)=sample(:,52);

cov36(:)=sample(:,79);


%deal with water content
qavgtemp=nominal36(:,4);

%determine the normalization factor based on water content
for i=1:numbersamps;
    normalization(i)=1-(qavgtemp(i)/nominal36(i,5));

    % multiply the weight % oxide by the normalization factor to get the actual
    % wt % water
    analyticalwater(i)=sample(i,25)*normalization(i);

    %convert the wt % water to vol water using density then add them together
    analyticalwatervol(i)=analyticalwater(i)*nominal36(i,5)/100;
    watervolpercent(i)=qavgtemp(i)+analyticalwatervol(i);

    nominal36(i,4)=watervolpercent(i);

    %combine uncerts on water content
    uncertwater1(i)=sample(i,65)*normalization(i)*nominal36(i,5)/100;
    uncertwater(i)=sqrt(uncertwater1(i)^2+sample(i,53)^2);
    uncerts36(i,4)=uncertwater(i);
    
    %normalize the values of the major elements
    nominal36(i,13)=nominal36(i,13)*normalization(i);
    nominal36(i,14)=nominal36(i,14)*normalization(i);
    nominal36(i,15)=nominal36(i,15)*normalization(i);
    nominal36(i,16)=nominal36(i,16)*normalization(i);
    nominal36(i,17)=nominal36(i,17)*normalization(i);
    nominal36(i,18)=nominal36(i,18)*normalization(i);
    nominal36(i,19)=nominal36(i,19)*normalization(i);
    nominal36(i,20)=nominal36(i,20)*normalization(i);
    nominal36(i,21)=nominal36(i,21)*normalization(i);
    nominal36(i,22)=nominal36(i,22)*normalization(i);
    nominal36(i,23)=nominal36(i,23)*normalization(i);

    %convert erosion rate and replace in nominal & uncerts
    nominal36(i,3)=nominal36(i,3)*nominal36(i,5)/10;
    uncerts36(i,3)=uncerts36(i,3)*nominal36(i,5)/10;
end

%save testsamples36 nominal36 uncerts36 cov36
