function [nominal21 uncerts21]=createage21(sample)

% Uses the standard input for a neon-21 sample and creates the individual
% variables needed for aging in the rest of the code.  

%sample input must be the inputs
%with no spaces between: inputs,uncerts,independent ages. 
% One row per sample with the following columns:
%
%1. Latitude (decimal degrees, -90(S) to +90(N))
%2. Longitude (decimal degrees, 0-360 degrees east)
%3. Elevation (meters)
%4. Pressure (hPa)      
%5. sample thickness (cm)
%6. bulk density (g/cm^3)
%7. Shielding factor for terrain, snow, etc. (unitless)
%8. Erosion-rate epsilon (mm/kyr)
%9. Sample 21-ne concentration (atoms of 21-ne/g of target)
%10. Inheritance for 21-ne (atoms 21-ne/g of target)
%11. Lambdafe (g/cm^2)
%12. Depth to top of sample (g/cm^2)
%13. Year sampled (e.g. 2010)
%14-26. Uncertainty for each of the above variables

numbersamps=size(sample,1); %this gives the number of samples
%initialize the results
nominal21=zeros(numbersamps,13);
uncerts21=zeros(numbersamps,13);

nominal21(:,1:13)=sample(:,1:13);
uncerts21(:,1:13)=sample(:,14:26);

%convert erosion rate from mm/kyr to g/cm^2/kyr
nominal21(:,8)=nominal21(:,8).*nominal21(:,6)./10;
uncerts21(:,8)=uncerts21(:,8).*nominal21(:,6)./10;

%save testsamples21 nominal21 uncerts21