function out=createcalib14(sample)

% Uses the standard input + 3 additional inputs for a carbon-14 sample 
% and creates the individual variables needed for calibration in the 
% rest of the code.  

%sample input must be the inputs
%with no spaces between: inputs,uncerts,independent ages. 
% One row per sample with the following columns:
%
%1. Latitude (decimal degrees, -90(S) to +90(N))
%2. Longitude (decimal degrees, 0-360 degrees east)
%3. Elevation (meters)
%4. Pressure (hPa)      
%5. sample thickness (cm)
%6. bulk density (g/cm^3)
%7. Shielding factor for terrain, snow, etc. (unitless)
%8. Erosion-rate epsilon (mm/kyr)
%9. Sample 14-C concentration (atoms of 14-C/g of target)
%10. Inheritance for 14-C (atoms 14-C/g of target)
%11. Lambdafe (g/cm^2)
%12. Depth to top of sample (g/cm^2)
%13. Year sampled (e.g. 2010)
%14-26. Uncertainty for each of the above variables
%27. Independent age of the sample (years before AD 2010)
%28. Uncertainty on independent age of the sample (years)
%29. Age Index indicator (should increment for each new age, even within
%sites (ex. if boulder ages are allowed to vary within the site)

numbersamps=size(sample,1); %this gives the number of samples
%initialize the results
nominal14=zeros(numbersamps,13);
indages14=zeros(numbersamps,2);
uncerts14=zeros(numbersamps,13);
ageindexsample14=zeros(numbersamps,1);

nominal14(:,1:13)=sample(:,1:13);
uncerts14(:,1:13)=sample(:,14:26);
indages14(:,1:2)=sample(:,27:28);
ageindexsample14(:,1)=sample(:,29);

%convert erosion rate from mm/kyr to g/cm^2/kyr
nominal14(:,8)=nominal14(:,8).*nominal14(:,6)./10;
uncerts14(:,8)=uncerts14(:,8).*nominal14(:,6)./10;

%split up samples based on aging index/site index
%initialize the matrix with the first sample
ageindex14(1)=1;
previousindex=ageindexsample14(1);
%indagenew is the variable for the new aging method. Indages is the
%variable that has the independent age for each sample.
indagenew14(1,1:2)=indages14(1,1:2);
countindage=1;

for i=2:size(nominal14,1);
    currentindex=ageindexsample14(i);
    if currentindex==previousindex;
        %same age as previous sample, so only use one age
        ageindex14(i)=ageindex14(i-1);
    else
        countindage=countindage+1;
        ageindex14(i)=ageindex14(i-1)+1;
        indagenew14(countindage,1:2)=indages14(i,1:2);
        
    end
    previousindex=currentindex;
end
save calibset14test nominal14 uncerts14 indages14 ageindex14 indagenew14
