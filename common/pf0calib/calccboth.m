%
% This script uses the LM method to calibrate the erosion rate and
% Pf(0) from the Copper Canyon 36-Cl and 10-Be data.  
%
%
% First, load in the data.  These file names will have to change if
% the data is updated!
%
load CCMM10B_5.mat
load CCMM36B_9.mat
%
% setup global variables.
%
global besamples
global besigmas;
global bedepths;
global clsamples;
global clsigmas;
global cldepths;
%
% Fill in with the raw data from the .mat files.
%
%
% First, 10-Be.
%
[nbesamples,fifteen]=size(nominal10);
besamples=nominal10;
besamples(:,13)=150.873107;
besigmas=zeros(nbesamples,1);
for i=1:nbesamples
  besigmas(i)=be10uncert(nominal10(i,9));
end
bedepths=zeros(nbesamples,1);
for i=1:nbesamples
  bedepths(i)=besamples(i,14)+0.5*besamples(i,5)*besamples(i,6);
end
%
% Next, the 36-Cl data.
%
[nclsamples,thirtyeight]=size(nominal36);
clsamples=nominal36;
clsamples(:,12)=150.873107;
clsigmas=zeros(nclsamples,1);
for i=1:nclsamples
  clsigmas(i)=uncerts36(i);
end
cldepths=zeros(nclsamples,1);
for i=1:nclsamples
  cldepths(i)=clsamples(i,37)+0.5*clsamples(i,5)*clsamples(i,6);
end
%
% Now that we have the data setup, use LM to fit the parameters.
%
%
% Here's a starting guess based on the grid search.
%
npars=2;
pinit=[6.5; 800]
%
% Use LM to find the optimal parameters.
%
[pstar,iter]=lm('ccfun','ccjac',pinit,1.0e-4,10);
%
% Compute the residual and J at the optimal parameters.
%
rstar=ccfun(pstar);
Jstar=ccjac(pstar);
%
% Compute Chi2 and pvalue.
%
chi2=norm(rstar,2)^2;
pvalue=1-chi2cdf(chi2,nbesamples+nclsamples-npars);
%
% Compute the covariance matrix for the fitted parameters.
%
covp=inv(Jstar'*Jstar);
sigmapstar=sqrt(diag(covp));
fprintf(1,'Chi^2=%f, p-value=%f \n',[chi2; pvalue]);
fprintf(1,'Erosion Rate=%f +- %f (mm/kyr) \n',[pstar(1); ...
		    sigmapstar(1)]);
fprintf(1,'Pf(0)=%f +- %f \n',[pstar(2); sigmapstar(2)]);
%
% Save the results.
%
save calccboth.mat
