function r=ccfun(p)

%
% setup global variables.
%
global besamples
global besigmas;
global bedepths;
global clsamples;
global clsigmas;
global cldepths;
%
% Get the number of 10-Be and 36-Cl samples.
%
[nbesamples,fifteen]=size(besamples);
[nclsamples,thirtyeight]=size(clsamples);
%
% Deal with the case that either parameter is negative.
%
if (p(1)<0)
  r=1.0e30*ones(nbesamples+nclsamples,1);
  return
end
if (p(2)<0)
  r=1.0e30*ones(nbesamples+nclsamples,1);
  return
end
%
% Set up a vector to hold the standardized residuals.
%
r=zeros(nbesamples+nclsamples,1);
%
% First, 10-Be.
%
sample=besamples(1,:);
%
% Get the erosion rate from p(1).  p(2)=Pf(0) is irrelevant to 10-Be.
%
sample(8)=p(1);
%
% Set a maximum depth.
%
maxdepth=20000;
%
% Get the rest of the parameters.
%
pp=physpars();
sp=samppars1026(sample);
sf=scalefacs1026(sp);
cp=comppars1026(pp,sp,sf,maxdepth);
%
% We'll use an age of 10 million years.
%
age=10000;
%
% Predict the accumulated N10
%
[N10,N26]=predN1026depth(pp,sp,sf,cp,age,bedepths);
%
% Compute the residuals.
%
for i=1:nbesamples
  r(i)=(N10(i)-besamples(i,9))/besigmas(i);
end

%
% Now, move on to 36-Cl.
%

for i=1:nclsamples
%
% Get this sample.
%
  sample=clsamples(i,:);
%
% Get the erosion rate from p(1).  p(2)=Pf(0) is irrelevant to 10-Be.
%
  sample(3)=p(1);
%
% Set a maximum depth.
%
  maxdepth=20000;
%
% Get physpars and set Pf(0).
%
  pp=physpars();
  pp.Pf0=p(2);
%
% Get the rest of the parameters.
%
  sp=samppars36(sample);
  sf=scalefacs36(sp);
  cp=comppars36(pp,sp,sf,maxdepth);
%
% Use an age of 10M years.
%
  age=10000;
%
% Compute the predicted concentration.
%
  N36=predN36depth(pp,sp,sf,cp,age,cldepths(i));
%
% Compute the residual for this sample.
%
  r(i+nbesamples)=(N36+cp.N36r-clsamples(i,1))/clsigmas(i);
end
