%
% This script uses the Copper Canyon 10-Be profile to calibrate the
% erosion rate.
%
%
% Load in the data.
%
load CCMM10B_5.mat
%
% Global variables.
%
global E;
global samples;
global depths;
samples=nominal10;
depths=nominal10(:,14);
%
% Get the number of samples.
%
[nsamples,fifteen]=size(samples);
%
% Compute uncertainties on the concentrations.
%
for k=1:nsamples
  E(k)=be10uncert(samples(k,9));
end
%
% An initial guess at the erosion rate and attenuation length.
%
pinit=[4.0; 130.0];
%
% Use LM to find optimal values of erosion rate.
%
[pstar,iter]=lm('corefun10','corejac10',pinit,1.0e-4,10);
%
% Compute the residual and J at the optimal parameters.
%
rstar=corefun10(pstar);
Jstar=corejac10(pstar);
%
% Compute Chi2 and pvalue.
%
chi2=norm(rstar,2)^2;
pvalue=1-chi2cdf(chi2,nsamples-2);
%
% Compute the covariance matrix for the fitted parameters.
%
covp=inv(Jstar'*Jstar);
sigmapstar=sqrt(diag(covp));
fprintf(1,'Chi^2=%f, p-value=%f \n',[chi2; pvalue]);
fprintf(1,'Erosion Rate=%f +- %f (mm/kyr) \n',[pstar(1); ...
		    sigmapstar(1)]);
fprintf(1,'Lambdafe=%f +- %f (g/cm^2) \n',[pstar(2); sigmapstar(2)]);
%
% Save the results.
%
save calccbecore.mat
