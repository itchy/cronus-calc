function J=ccjac(p)
%
% Compute ccfun(p) first.
%
r0=ccfun(p);
%
% We have two parameters.
%
npars=2;
%
% Initialize J.
%
J=zeros(length(r0),2);
%
% Compute the derivative with respect to p(1).
%
p1=p;
p1(1)=p(1)*1.001;
r1=ccfun(p1);
J(:,1)=(r1-r0)/(0.001*p(1));
%
% Compute the derivative with respect to p(2).
%
p1=p;
p1(2)=p(2)*1.001;
r1=ccfun(p1);
J(:,2)=(r1-r0)/(0.001*p(2));

