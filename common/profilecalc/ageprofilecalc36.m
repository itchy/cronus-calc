function [posterior_er,posterior_age,posterior_inher,MAP,mu_bayes,chi2grid,...
    lhsurf,jposterior]=ageprofilecalc36(comp,concsig,depths,erates,ages,...
    inhers,prior,scaling_model,maxerosion)
%
%   Age Profile Calculator for Cl36
%
%   [posterior_er,posterior_age,posterior_inher,MAP,mu_bayes,chi2grid]=...
%       ageprofilecalc36(comp,concsig,depths,erates,ages,inhers,scaling_model,maxerosion)
%
%   Inputs:
%
%       comp                profile composistion
%       concsig             vector of concentration uncertanties
%       depths              vector of depths
%       erates              vector of erosion rates
%       age                 vector of ages
%       inhers              vector of inhers
%       prior               prior distribution
%  		scaling_model		The scaling model being used
%       maxerosion          maximum erosion allowed
%
%   Outputs:
%   
%       posterior_er        posterior erosion rate distribution
%       posterior_age       posterior age distribution
%       posterior_inher     posterior inheritance distribution
%       MAP                 maximum a posterior solution   
%       mu_bayes                 bayesian mean solution
%

if nargin==8
    maxerosion=+inf;
end


% Set global variables
global PP;
global STOREDSP;
global STOREDSF;
global STOREDCP;
global maxdepth;

STOREDSP={};
STOREDSF={};
STOREDCP={};
%
% Figure out the number of samples.
%

[nsamples,thirtyeight]=size(comp);

conc=comp(:,1);
ndepths=length(depths);
m=length(erates);
n=length(ages);
p=length(inhers);


% % Select the prior distributions
% er_prior=prior(erates,'uniform');
% age_prior=prior(ages,'uniform');
% inher_prior=prior(inhers,'uniform');


% Define maxdepth
maxdepth=max(erates)*max(ages)*max(comp(:,6))+3000;


% Run getpars on the first sample(choice abitrary) to get and store
% physical parameters(pp) and scale factors(sf)

for k=1:nsamples
  [pp,sp,sf,cp]=getpars36(comp(k,:),maxdepth);
  PP=pp;
  STOREDSP{k}=sp;
  STOREDSF{k}=sf;
  STOREDCP{k}=cp;
end


% Compute the chi2 grid
chi2grid=computechi236(erates,ages,inhers,comp,...
    depths,conc,concsig,scaling_model);
disp('The Chi2 grid is done...')


% Calculate the likelihood surface
disp('Calculating likelihood  surface...');
lhsurf=ndepths/sqrt(2*pi*mean(concsig(:,1)))*exp(-1/2*chi2grid);


% Check for exceeding maxerosion
for i=1:m
    for j=1:n
        for k=1:p
            if (erates(i)*ages(j)>maxerosion)
                lhsurf(i,j,k)=0;
            end
        end
    end
end


% Calculate the joint posterior distribution
disp('Calculating joint posterior distribution...');
jposterior=lhsurf.*prior/trapz(erates,trapz(ages,...
    trapz(inhers,lhsurf.*prior,3),2),1);


% Find the MAP solution
disp('Finding MAP solution...');
[indexofmax,themax]=wheremax(jposterior);
MAP(1)=erates(indexofmax(1));
MAP(2)=ages(indexofmax(2));
MAP(3)=inhers(indexofmax(3));


% Marginalize joint posterior for erosion rate
disp('Marginalizing joint posterior for erosion rate...');
posterior_er=trapz(ages,trapz(inhers,jposterior,3),2);


% Marginalize joint posterior for age
disp('Marginalizing joint posterior for age...');
posterior_age=trapz(erates,trapz(inhers,jposterior,3),1)';


% Marginalize joint posterior for inher
disp('Marginalizing joint posterior for inheritance...');
posterior_inher=trapz(erates,jposterior,1);
posterior_inher=reshape(posterior_inher,n,p);
posterior_inher=trapz(ages,posterior_inher,1)';


% Calulate Bayesian mean model
disp('Calculating Bayesian mean model...');
mu_bayes=zeros(3,1);
mu_bayes(1)=trapz(erates,posterior_er.*erates);
mu_bayes(2)=trapz(ages,posterior_age.*ages);
mu_bayes(3)=trapz(inhers,posterior_inher.*inhers);