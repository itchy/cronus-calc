function pr=prior(erates,ages,inhers,er_type,age_type,inher_type)
%
%   Calculates the 3-d prior distribution
%
%   pr=prior(erates,ages,inhers,er_type,age_type,inher_type)
%
%   Inputs:
%
%       erates          range of erosion rates
%       ages            range of ages
%       inhers          range of inheritances
%       **_type         prior can be either 'uniform' or 'triangle'
%       
%   Output:
%
%       pr              3-d prior distribution
%

m=length(erates);
n=length(ages);
p=length(inhers);
pr1=zeros(m,n,p);
pr2=zeros(m,n,p);
pr3=zeros(m,n,p);


for j=1:n
    for k=1:p
        pr1(:,j,k)=proirtype(erates,er_type);
    end
end

for i=1:m
    for k=1:p
        pr2(i,:,k)=proirtype(ages,age_type)';
    end
end

for i=1:m
    for j=1:n
        temp=proirtype(inhers,inher_type);
        for k=1:p
            pr3(i,j,k)=temp(k);
        end
    end
end

pr=pr1.*pr2.*pr3;
pr=pr/sum(sum(sum(pr)));



function y=proirtype(x,type)

switch type
    
    case 'uniform'
        y=ones(length(x),1);
        y=y*1/length(x);
        
    case 'triangle'
        a=min(x);
        b=max(x);
        t=[a (b+a)/2 b]; 
        s=[0 2/(b-a) 0];
        y=interp1(t,s,x);
end
