function plotprof1026(comp,uncerts,depths,er,age,inher,scaling_model)
%
%   plotprof1026(comp,uncerts,depths,er,age,inher,scaling_model)
%
%   Plots our model at given parameters vs. data


mydepths=(0.0:10:500)';
conc=comp(:,9);
maxdepth1026 = 1000+max(mydepths)+er/10*age*max(comp(:,6));
[pp,sp1026,sf1026,cp1026]=getpars1026(comp(1,:),maxdepth1026);
sp36.epsilon=er;

% Calculate theoretical profile
[predictedN10,predictedN26]=predN1026depth(pp,sp1026,sf1026,cp1026,age, ...
					   mydepths,scaling_model);
Prodtotal=prodz1026(0,pp,sf1026,cp1026);
predictedN10=predictedN10+inher*Prodtotal;


%  Plot model with data
set(axes,'XDir','default', 'YDir', 'reverse')
% set(gca,'XAxisLocation','top')
hold on
hPred = plot(predictedN10,mydepths,'k');
herrorbar(conc,depths,uncerts(:,1),'ko')
hConc = plot(conc,depths,'ko');
% set(hConc,'MarkerEdgeColor',[0 0 0],'MarkerFaceColor',[.7 .7 .7])
% hTitle=title('Fitted Depth Profile');
hLegend=legend([hPred,hConc],'Predicted ^{10}Be','Measured ^{10}Be','Location','SouthEast');
hXLabel=xlabel('Concentration of ^{10}Be');
hYLabel=ylabel('Depth (g/cm^2)');
plotstyle(hXLabel,hYLabel,hLegend)
hold off
