% Profile calculator script for Greenland Be-10 #3 profile

clear
tic


% Define the paramter space
erates = linspace(-2,5,50)';
er_prior = 'uniform';
ages = linspace(7,13,50)';
age_prior = 'uniform';
inhers = linspace(0e3,3.5e3,50)';
inher_prior = 'uniform';

% Define alpha level
alpha = 1-0.68; % equivalent to 1-sigma confidence level 


% Run ageprofilecalc1026
[jposterior,posterior_er,posterior_age,posterior_inher,MAP,...
    mu_bayes,BCI,chi2grid,lhsurf]=profilecalc1026('greenland_IC06-3.mat',...
    erates,ages,inhers,er_prior,age_prior,inher_prior,alpha);


% Print the results
fprintf('MAP_erate = %.2f g/cm^2/kyr \n',MAP(1))
fprintf('MAP_age   = %.2f k years \n',MAP(2))
fprintf('MAP_inher = %.2f years of exposure \n \n',MAP(3))
fprintf('P(%.2f < erate < %.2f) = %.2f \n',[BCI(1,:) 1-alpha])
fprintf('P(%.2f < age < %.2f) = %.2f \n',[BCI(2,:) 1-alpha])
fprintf('P(%.2f < inher < %.2f) = %.2f \n \n',[BCI(3,:) 1-alpha])

toc