function [BCI mid] = bayesianCI2(x,y,p_x,alpha)
% Computed bayesian credible intervals for both x and y, at the (1-alpha)
% confiedence level. The optional output 'mid', is the height of the 
% countor at which the (1-alpha) BCI should be drawn.
%
% NOTE: You may have to adjust the biscetion search tol relative to
% the resolution and shape p_x. 
%
%       e.g. for p_x = mvnpdf(0,eye(2)), a 100x100 grid, tol <= 1e-3 works well.
%

mid = bisection(x,y,p_x,1-alpha);
dummy  = max(max(p_x)) + 10;
C = contourc(x,y,p_x,[mid dummy]);
C = C(:,2:end);

LB_x = min(C(1,:));
UB_x = max(C(1,:));
LB_y = min(C(2,:));
UB_y = max(C(2,:));
BCI = [LB_x UB_x;LB_y UB_y];



function mid = bisection(x,y,p_x,CL)
left = 0;
right = 4;
iters = 0;
tol = 1e-4;
while (abs(right - left) > tol) && iters < 100
    mid = (left+right)/2;
    fl = f(x,y,p_x,left,CL);
    fr = f(x,y,p_x,right,CL);
    fm = f(x,y,p_x,mid,CL);
    if fl*fm < 0
        right  = mid;
    elseif fr*fm < 0
        left = mid;
    else
        break
    end
    iters = iters + 1;
end

function V = f(x,y,p_x,c,CL)
X = p_x;
for i = 1:length(x)
    for j = 1:length(y)
        if p_x(i,j) < c
            X(i,j) = 0;
        end
    end
end
V = trapz(y,trapz(x,X,1),2)-CL;

