function [jposterior,posterior_er,posterior_age,posterior_inher,MAP,...
    mu_bayes,BCI,chi2grid,lhsurf]=profilecalc1026(filename,erates,ages,...
    inhers,er_prior,age_prior,inher_prior,alpha,maxerosion)
%
%   Profile Calculator for Be10/Al26
%
%   [jposterior,posterior_er,posterior_age,posterior_inher,MAP,...
%   mu_bayes,BCI,chi2grid,lhsurf]=profilecalc1026(filename,erates,ages,...
%   inhers,er_prior,age_prior,inher_prior,alpha,maxerosion)
%
%   Inputs:
%
%       filename            'filename.mat'
%       erates              range of erosion rates
%       age                 range of ages
%       inhers              range of inheritances
%       er_prior            prior distribution type for erosion rate
%       age_prior           prior distribution type for age
%       inher_prior         prior distribution type for inheritance
%       alpha               confidence level
%       maxerosion          maximum erosion allowed (optional)
%
%   Outputs:
%   
%       j_posterior         joint posterior distribution
%       posterior_er        marginal erosion rate distribution
%       posterior_age       marginal age distribution
%       posterior_inher     marginal inheritance distribution
%       MAP                 maximum a posterior solution   
%       mu_bayes            bayesian mean solution
%       BCI                 bayesian crebible intervals [erate;age;inher]
%       chi2grid            chisquared hyper-surface
%       lhsurf              joint likelihood surface


if nargin==8
    maxerosion=+inf;
end


% Load the data file
feval(@load, filename);


% Read in the concentrations, uncertanties and composition for  both Be-10 
% and Al-26
comp = nominal10;
uncerts = uncerts10;
conc = comp(:,9:10);
concsig = uncerts(:,9:10);



% Calculate depth-to-middle of the samples
depths = comp(:,14) + 0.5*comp(:,5).*comp(:,6);
ndepths=length(depths);


% Get problem dimesions
m=length(erates);
n=length(ages);
p=length(inhers);


% Define the prior distribution
pr=prior(erates,ages,inhers,er_prior,age_prior,inher_prior);


% Set global variables
global PP;
global STOREDSF;
global maxdepth;


% Define maxdepth
maxdepth=max(erates)*max(ages)*max(comp(:,6))+3000;


% Run getpars on the first sample(choice abitrary) to get and store
% physical parameters(pp) and scale factors(sf) 
[pp,sp,sf]=getpars1026(comp(1,:),maxdepth);
PP=pp;
STOREDSF=sf;


% Compute the chi2 grid
chi2grid=computechi21026(erates,ages,inhers,comp,...
    depths,conc,concsig);
disp('The Chi2 grid is done...')


% Calculate the likelihood surface
lhsurf=ndepths/sqrt(2*pi*mean(concsig(:,1)))*exp(-1/2*chi2grid);


% Check for exceeding maxerosion
for i=1:m
    for j=1:n
        for k=1:p
            if (erates(i)*ages(j)>maxerosion)
                lhsurf(i,j,k)=0;
            end
        end
    end
end


% Calculate the joint posterior distribution
jposterior=lhsurf.*pr/trapz(erates,trapz(ages,...
    trapz(inhers,lhsurf.*pr,3),2),1);


% Find the MAP solution
[indexofmax]=wheremax(jposterior);
MAP(1)=erates(indexofmax(1));
MAP(2)=ages(indexofmax(2));
MAP(3)=inhers(indexofmax(3));


% Marginalize joint posterior for erosion rate
posterior_er=trapz(ages,trapz(inhers,jposterior,3),2);


% Marginalize joint posterior for age
posterior_age=trapz(erates,trapz(inhers,jposterior,3),1)';


% Marginalize joint posterior for inher
posterior_inher=trapz(erates,jposterior,1);
posterior_inher=reshape(posterior_inher,n,p);
posterior_inher=trapz(ages,posterior_inher,1)';


% Calulate Bayesian mean model
mu_bayes=zeros(3,1);
mu_bayes(1)=trapz(erates,posterior_er.*erates);
mu_bayes(2)=trapz(ages,posterior_age.*ages);
mu_bayes(3)=trapz(inhers,posterior_inher.*inhers);


% Create plots
[erate_vs_age,age_vs_inher,erate_vs_inher]=makeplots1026(...
    erates,ages,inhers,posterior_er,posterior_age,...
    posterior_inher,comp,uncerts10,depths,MAP,jposterior);


% Calculate Bayesian credible intervals frome the 2-D joint posterior pairs
BCI_er_age = bayesianCI2(erates,ages,erate_vs_age',alpha);
BCI_age_inher  = bayesianCI2(ages,inhers,age_vs_inher',alpha);
BCI_er_inher  = bayesianCI2(erates,inhers,erate_vs_inher',alpha);


% Calculate the most conservative BCIs for each parameter
BCI(1,1) = min([BCI_er_age(1,:) BCI_er_inher(1,:)]);
BCI(2,1) = min([BCI_er_age(2,:) BCI_age_inher(1,:)]);
BCI(3,1) = min([BCI_er_inher(2,:) BCI_age_inher(2,:)]);
BCI(1,2) = max([BCI_er_age(1,:) BCI_er_inher(1,:)]);
BCI(2,2) = max([BCI_er_age(2,:) BCI_age_inher(1,:)]);
BCI(3,2) = max([BCI_er_inher(2,:) BCI_age_inher(2,:)]);
