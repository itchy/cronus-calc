%
%  chi2grid=computechi21026(erates,ages,inhers,comp,depths,measuredconc,...
%                       sigma,scaling_model)
%
%
%  Computes a 3-d array of Chi^2 values corresponding to the ages,
%  erosion rates, inheritances, given for the profile defined by
%  comp, depths, measuredconc, sigma, and scaling_model.
%
%
function chi2grid=computechi21026(erates,ages,inhers,comp,depths,measuredconc,...
    sigma,scaling_model)
%
% Global Variables.
%
global PP;
global STOREDSF;
global maxdepth;
%
% First, figure out the dimensions of the problem.
%
nerosion=length(erates);
nages=length(ages);
ninhers=length(inhers);
nsamples=size(comp,1);

measuredconc10=measuredconc(:,1);
measuredconc26=measuredconc(:,2);
sigma10=sigma(:,1);
sigma26=sigma(:,2);
%
% Initialize chi2grid.
%
chi2grid=zeros(nerosion,nages,ninhers);
%
% Loop over the samples.
%

%
% Compute the parameters for this sample.
%
pp=PP;
sp=samppars1026(comp(1,:));
sf=STOREDSF;
cp=comppars1026(pp,sp,sf,maxdepth);
%
% Go ahead and produce contemporary scaling factors.
%
sf.Sel=getcurrentsf(sf,0,scaling_model,'albe');

%
% Set the depth to the depth of this sample.
%
%     thisdepth=depths(s);
%
% Compute the contemporary surface production rate and use it to get the
% saturation concentration=production rate/decay rate.
%
[ProdtotalBe,ProdtotalAl]=prodz1026(0,pp,sf,cp);
%
% Now, loop over all of the erosion rates, ages, and inheritances.
%
for i=1:nerosion
    %
    % Adjust the erosion rate.
    %
    sp.epsilon=erates(i);
    for j=1:nages
        %
        % Check for the impossible case that with negative erosion, there
        % couldn't be a point at this current depth (thisdepth) that was
        % original deposited back then.
        %
        if (min(depths) < (-erates(i)*ages(j)))
            N10predicted=-Inf;
            N26predicted=-Inf;
        else
            [N10predicted,N26predicted]=predN1026depth(pp,sp,sf,cp,ages(j),depths,scaling_model);
        end
        for k=1:ninhers
            %
            % The predicted 10-Be consists of cosmogenic 10-Be plus 
            % inherited 10-Be.  We take the difference between this
            % and the measured concentration of 10-Be in the sample.
            %
            rBe=((N10predicted+inhers(k)*ProdtotalBe-measuredconc10)./ ...
                sigma10);

            rAl=((N26predicted+inhers(k)*ProdtotalAl-measuredconc26)./ ...
                sigma26);

            if min(sigma26) > 0 % assumes we dont use Al26 w/o Be10
                chi2grid(i,j,k)=chi2grid(i,j,k)+(rBe'*rBe)+(rAl'*rAl);
            else
                chi2grid(i,j,k)=chi2grid(i,j,k)+(rBe'*rBe);
            end
        end
    end

    fprintf(1,'Progress: %.1f%% \n',100*i/nerosion);
end
