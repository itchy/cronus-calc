function J=corejac26(p)
%
% Global parameters.
%
global samples;
global depths;
global E;
%
% Get the number of samples.
%
[nsamples,foo]=size(samples);
%
% Make space for the J matrix.
%
J=zeros(nsamples,2);
%
% Compute the derivative with respect to p(1).
%
r0=corefun26(p);
p1=p;
p1(1)=p(1)*1.001;
r1=corefun26(p1);
J(:,1)=(r1-r0)/(0.001*p(1));
%
% Compute the derivative with respect to p(2).
%
p1=p;
p1(2)=p(2)*1.001;
r1=corefun26(p1);
J(:,2)=(r1-r0)/(0.001*p(2));
