function J=corejac36Ca(p)
%
% Global parameters.
%
global samples;
global depths;
global E;
%
% Get the number of samples.
%
[nsamples,foo]=size(samples);
%
% We have four parameters.
%
npars=5;
%
% Make space for the J matrix.
%
J=zeros(nsamples,npars);
%
% Figure out r for the current parameter values.
%
r0=corefun36Ca(p);
%
% Compute the derivative with respect to p(1).
%
p1=p;
p1(1)=p(1)*1.001;
r1=corefun36Ca(p1);
J(:,1)=(r1-r0)/(0.001*p(1));
%
% Compute the derivative with respect to p(2).
%
p1=p;
p1(2)=p(2)*1.001;
r1=corefun36Ca(p1);
J(:,2)=(r1-r0)/(0.001*p(2));
%
% Compute the derivative with respect to p(3).
%
p1=p;
p1(3)=p(3)*1.001;
r1=corefun36Ca(p1);
J(:,3)=(r1-r0)/(0.001*p(3));
%
% Compute the derivative with respect to p(4).
%
p1=p;
p1(4)=p(4)*1.001;
r1=corefun36Ca(p1);
J(:,4)=(r1-r0)/(0.001*p(4));
%
% Compute the derivative with respect to p(4).
%
p1=p;
p1(5)=p(5)*1.001;
r1=corefun36Ca(p1);
J(:,5)=(r1-r0)/(0.001*p(5));
