%
% This script plots the results from calbhcoreboth.m
%
%
% Load in the .mat file containing the results.
%
load calbhcoreboth.mat
%
% Put the fitted parameters in for erosion rate and attenuation length.
%
samples(:,8)=pstar(1);
samples(:,13)=pstar(2);
%
% Compute predicted 10-Be concentrations using the fitted parameters.
%
maxdepth=20000;
pp=physpars();
sp=samppars1026(samples(1,:));
sf=scalefacs1026(sp);
cp=comppars1026(pp,sp,sf,maxdepth);
%
% We need a range of depths from 0 to 250 g/cm^2.  
%
plotteddepths=(0:10:250)';
%
% We're using an age of 10 million years (long enough to reach saturation.)
%
age=10000;
%
% Compute the predicted concentrations.
%
[predictedN10,foo]=predN1026depth(pp,sp,sf,cp,age,plotteddepths);
%
% Now, repeat for 26-Al.  Note that the attenuation length changes!
%
%
% Put the fitted parameters in for erosion rate and attenuation length.
%
samples(:,8)=pstar(1);
samples(:,13)=pstar(3);
%
% Compute predicted 10-Be concentrations using the fitted parameters.
%
maxdepth=20000;
pp=physpars();
sp=samppars1026(samples(1,:));
sf=scalefacs1026(sp);
cp=comppars1026(pp,sp,sf,maxdepth);
%
% We need a range of depths from 0 to 250 g/cm^2.  
%
plotteddepths=(0:10:250)';
%
% We're using an age of 10 million years (long enough to reach saturation.)
%
age=10000;
%
% Compute the predicted concentrations.
%
[foo,predictedN26]=predN1026depth(pp,sp,sf,cp,age,plotteddepths);
%
% Note that in the following you'll need a copy of herrorbar from
% the MATLAB file-exchange.  
%
%
% Plot the 10-Be profile.
%
figure(1);
clf;
herrorbar(samples(:,9),depths,E(1:nsamples),'ko');
set(gca,'YDir','reverse');
axis([0 7e7 0 250]); 
hold on
plot(predictedN10,plotteddepths,'k');
xlabel('10-Be Concentration (atoms/gram)');
ylabel('Depth (grams/cm^2)');
print -dpng bhbeprofile.png
print -depsc bhbeprofile.eps
%
% Plot the 26-Al profile.
%
figure(2);
clf;
herrorbar(samples(:,10),depths,E(nsamples+1:end),'ko');
set(gca,'YDir','reverse');
axis([0 2.5e8 0 250]); 
hold on
plot(predictedN26,plotteddepths,'k');
xlabel('26-Al Concentration (atoms/gram)');
ylabel('Depth (grams/cm^2)');
print -dpng bhalprofile.png
print -deps bhalprofile.eps