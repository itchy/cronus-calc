function r=corefun36K(p)
%hard-code scaling scheme
scaling_model='sa';
%
% Global parameters.
%
global samples;
global depths;
global E;
%
% Get the number of samples.
%
[nsamples,foo]=size(samples);
%
% Deal with the case the erosion rate is negative.
%
if any(p<0)
  r=1.0e30*ones(nsamples,1);
  return
end
%
%Copy samples into "sample" so we don't overwrite original data
%
sample=samples;
%
% Put parameter 1 into the erosion rate.
%
sample(:,3)=p(1);
%
% Put parameter 2 into the attenuation length.
%
sample(:,12)=p(2);
%
% Set a maximum depth.
%
maxdepth=70000;
%
% Get the rest of the parameters for each sample.
%
pp=physpars();
pp.fstar36K=p(3)*1.0e-2;
pp.sigma036K=p(4)*1.0e-30;
%
% Now, work out the parameters for the rest of the samples.
%
for m=1:nsamples
    sampletemp=sample(m,:);
    sp(m)=samppars36(sampletemp);
    sf(m)=scalefacs36(sp(m));
    cp(m)=comppars36(pp,sp(m),sf(m),maxdepth);
end
%
% We'll use an age of 10 million years.
%
age=10000;
%
% Loop through the samples and compute predicted concentrations and residuals. 
%
r=zeros(nsamples,1);
for i=1:nsamples
  [N36(i)]=predN36depth(pp,sp(i),sf(i),cp(i),age,depths(i),scaling_model);
  cpnow=cp(i);
  r(i)=(N36(i)+cpnow.N36r-samples(i,1))/E(i);
end