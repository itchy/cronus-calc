function J=muonjac26(p)
%
% Global parameters.
%
global samples;
global depths;
global E;
%
% We have two parameters.
%
npars=2;
%
% Get the number of samples.
%
[nsamples,foo]=size(samples);
%
% Make space for the J matrix.
%
J=zeros(nsamples,npars);
%
% Compute the residuals at the nominal parameter values.
%
r0=muonfun26(p);
%
% Compute the derivative with respect to p(1).
%
p1=p;
p1(1)=p(1)*1.001;
r1=muonfun26(p1);
J(:,1)=(r1-r0)/(0.001*p(1));
%
% Compute the derivative with respect to p(2).
%
p2=p;
p2(2)=p(2)*1.001;
r2=muonfun26(p2);
J(:,2)=(r2-r0)/(0.001*p(2));
