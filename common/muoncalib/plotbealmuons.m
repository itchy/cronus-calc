%
% This script plots the results from calbhcore.m
%
%
% Load in the .mat file containing the results.
%
load calbhcore.mat
%
% Compute predicted 10-Be using the fitted parameters.
%
%
% First, setup the correct values of the erosion rate and
% attenuation length.
%
samples(:,8)=pstar(1);
samples(:,13)=pstar(2);
%
% Now, compute the parameters.
%
maxdepth=20000;
pp=physpars();
sp=samppars1026(samples(1,:));
sf=scalefacs1026(sp);
cp=comppars1026(pp,sp,sf,maxdepth);
%
% We need a range of depths from 0 to 6500 g/cm^2.  
%
plotteddepths=(0:125:6500)';
%
% We're using an age of 10 million years (long enough to reach saturation.)
%
age=10000;
%
% Compute the predicted concentrations.
%
[predictedN10,ignore]=predN1026depth(pp,sp,sf,cp,age,plotteddepths);
%
% Plot the 10-Be profile.
%
figure(3);
clf;
errorbar(depths,samples(:,9),E(1:nsamples10),'ko');
axis([0 6500 0 80000000]); 
set(gca,'YScale','log');
hold on
plot(plotteddepths,predictedN10,'k');
ylabel('10-Be Concentration (atoms/g)');
xlabel('Depth (g/cm^2)');
print -dpng plotbemuons.png
print -depsc plotbemuons.eps
print -dpdf plotbemuons.pdf
%
% Compute predicted 26-Al using the fitted parameters.
%
%
% Pick out only the samples that have 26-Al concentrations.
%
range=(samples(:,10) ~= 0);
samples=samples(range,:);
depths=depths(range,:);
%
% In the E vector, we want only the uncertainties corresponding to
% 26-Al measurements.  The first nsamples10 measurements correspond
% to 10-Be measurements.  
%
rangeAl=[zeros(nsamples10,1); range];
%
% Next, setup the correct values of the erosion rate and
% attenuation length.  Note that attenuation length is different
% for 26-Al!
%
samples(:,8)=pstar(1);
samples(:,13)=pstar(3);
%
% Now, compute the parameters.
%
maxdepth=20000;
pp=physpars();
sp=samppars1026(samples(1,:));
sf=scalefacs1026(sp);
cp=comppars1026(pp,sp,sf,maxdepth);
%
% We need a range of depths from 0 to 6500 g/cm^2.  
%
plotteddepths=(0:125:6500)';
%
% We're using an age of 10 million years (long enough to reach saturation.)
%
age=10000;
%
% Compute the predicted concentrations.
%
[ignore,predictedN26]=predN1026depth(pp,sp,sf,cp,age,plotteddepths);
%
% Plot the 26-Al profile.
%
figure(4);
clf;
errorbar(depths,samples(:,10),E(rangeAl ~= 0),'ko');
axis([0 6500 0 300000000]); 
set(gca,'YScale','log');
hold on
plot(plotteddepths,predictedN26,'k');
ylabel('26-Al Concentration (atoms/g)');
xlabel('Depth (g/cm^2)');
print -dpng plotalmuons.png
print -depsc plotalmuons.eps
print -dpdf plotalmuons.pdf
