%
% This script uses the Beacon Hill 10-Be profile to calibrate the
% attenuation length and erosion rate for 10-Be production at this site. 
%
% Load in the data.
%
load BHC10JS.mat
%
% Global variables.
%
global E;
global samples;
global depths;
global nsamples10;
global nsamples26;
global nresids;
global npars;
%
% Store the samples in global variables.
%
samples=nominal10;
uncerts=uncerts10;
%
% Get the number of samples.
%
[nsamples10,fifteen]=size(samples);
nsamples26=nnz(samples(:,10) ~= 0);
nresids=nsamples10+nsamples26;
%
% We have 7 parameters to fit.
%
npars=7;
%
% Compute uncertainties on the concentrations.
%
for k=1:nsamples10
  temp=be10uncert(samples(k,9));
  if (temp > uncerts(k,9))
     E(k)=temp;
  else
     E(k)=uncerts(k,9);
  end
end
%
% Note that some samples have no measured 26-Al, so we'll set those
% residuals to 0 in computing Chi^2.
%
for k=1:nsamples10
  temp=al26uncert(samples(k,10));
  if (samples(k,10) > 0)
    if (temp > uncerts(k,10))
      E(k+nsamples10)=temp;
    else
      E(k+nsamples10)=uncerts(k,10);
    end
  else
    E(k+nsamples10)=1;
  end
end
%
% Separate out the depths.
%
depths=samples(:,14)+0.5*samples(:,5).*samples(:,6);
%
% An initial guess at the parameters.
%
pinit=[0.02; 140.0; 140.0; 1.4; 0.25; 10; 4];
%
% Use LM to find optimal values of erosion rate and attenuation length.
%
[pstar,iter]=lm('corefun1026','corejac1026',pinit,1.0e-5,100);
%
% Compute the residual and J at the optimal parameters.
%
rstar=corefun1026(pstar);
Jstar=corejac1026(pstar);
%
% Compute Chi2 and pvalue.
%
chi2=norm(rstar,2)^2;
pvalue=1-chi2cdf(chi2,nresids-npars);
%
% Compute the covariance matrix for the fitted parameters.
%
covp=inv(Jstar'*Jstar);
sigmapstar=sqrt(diag(covp));
%
% Compute the correlation matrix for the fitted parameters.
%
for i=1:npars 
  for j=1:npars
    corp(i,j)=covp(i,j)/(sqrt(covp(i,i))*sqrt(covp(j,j)));
  end
end
disp('Correlations between fitted parameters');
corp
%
% Print out the fitted parameters.
%
fprintf(1,'Chi^2=%f, p-value=%f \n',[chi2; pvalue]);
fprintf(1,'Erosion Rate=%f +- %f ((g/cm^2)/kyr) \n',[pstar(1); ...
		    sigmapstar(1)]);
fprintf(1,'10-Be Attenuation length=%f +- %f (g/cm^2) \n',[pstar(2); ...
		    sigmapstar(2)]);
fprintf(1,'26-Al Attenuation length=%f +- %f (g/cm^2) \n',[pstar(3); ...
		    sigmapstar(3)]);
fprintf(1,'fstar10=%f +- %f \n',[pstar(4); ...
		    sigmapstar(4)]);
fprintf(1,'sigma010=%f +- %f \n',[pstar(5); ...
		    sigmapstar(5)]);
fprintf(1,'fstar26=%f +- %f \n',[pstar(6); ...
		    sigmapstar(6)]);
fprintf(1,'sigma026=%f +- %f \n',[pstar(7); ...
		    sigmapstar(7)]);
%
% Save the results.
%
save calbhcore.mat
