%
% This script uses a depth profile to calibrate the
% muon production parameters for production of 36-Cl from K.
%
% Load in the data.
%
load calib36coreCa.mat
%
% Global variables.
%
global E;
global samples;
global depths;
global nsurfacesamples;
%
% Note that the first three samples are true surface samples.
%
nsurfacesamples=1;
%
% Figure out depths to middle of samples.
%
samples=nominal36;
depths=nominal36(:,37)+0.5*nominal36(:,5).*nominal36(:,6);
%
% Get the number of samples.
%
[nsamples,thirtyeight]=size(samples);
%
% Compute uncertainties on the concentrations.
%
for k=1:nsamples
   E(k)=uncerts36(k,1);
   if (cl36uncert(nominal36(k,1))> E(k))
     E(k)=cl36uncert(nominal36(k,1));
   end
end
%
% An initial guess at the parameters.
%
npars=5;
pinit=[6.0; 160; 150; 1.0; 7.0];
%
% Use LM to find optimal values of parameters.
%
[pstar,iter]=lm('corefun36Ca','corejac36Ca',pinit,1.0e-5,100);
%
% Compute the residual and J at the optimal parameters.
%
rstar=corefun36Ca(pstar);
Jstar=corejac36Ca(pstar);
%
% Compute Chi2 and pvalue.
%
chi2=norm(rstar,2)^2;
pvalue=1-chi2cdf(chi2,nsamples-npars);
%
% Compute the covariance matrix for the fitted parameters.
%
covp=inv(Jstar'*Jstar);
sigmapstar=sqrt(diag(covp));
fprintf(1,'Chi^2=%f, p-value=%f \n',[chi2; pvalue]);
fprintf(1,'erosion rate=%f +- %f \n',[pstar(1); ...
		    sigmapstar(1)]);
fprintf(1,'attenuation length=%f +- %f  \n',[pstar(2); ...
		    sigmapstar(2)]);
fprintf(1,'additional overburden=%f +- %f  \n',[pstar(3); ...
		    sigmapstar(3)]);
fprintf(1,'fstar=%f +- %f \n',[pstar(4); ...
		    sigmapstar(4)]);
fprintf(1,'sigma0=%f +- %f  \n',[pstar(5); ...
		    sigmapstar(5)]);


for i=1:npars
  for j=1:npars
    corp(i,j)=covp(i,j)/(sqrt(covp(i,i))*sqrt(covp(j,j)));
  end
end
corp
%
% Save the results.
%
save calClCaprofile.mat
